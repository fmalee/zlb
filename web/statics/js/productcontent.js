﻿function IsNumeric(e) {
    var t = /^\d+$/;
    return t.test(e) ? !0 : !1
};

function selectSpec(e, t, n, r) {
    var i = "spec_group" + e,
        s = "spec_value" + e + "_" + t,
        o = jQuery("#" + i).val();
    o == n ? (disalbedNullSelect(e, t, n, !0), jQuery("#" + i).val(""), jQuery("#" + s).removeClass("s_a")) : (disalbedNullSelect(e, t, n, !1), jQuery("#em" + e).find("span").removeClass("s_a"), jQuery("#" + i).val(n), jQuery("#" + s).addClass("s_a"), isCanBuyTheSpec() || (jQuery("#" + i).val(""), jQuery("#" + s).removeClass("s_a"), alert("该商品没有此规格！"))), setBtnClass()
};

function isUnabled(e) {
    return jQuery(e).attr("unable") == "true"
};

function isCanBuyTheSpec() {
    if (!isSpecSelected()) return !0;
    var e = getSpecs();
    return isSpecExisted(e) > 0 ? !0 : !1
};

function isSpecExisted(e) {
    var t = jQuery("#SpecPrice").val();
    if (t == "") return 1;
    var n = t.split("|");
    for (var r = 0; r < n.length; r++) {
        var i = n[r].split(";
	");
        if (i[0] == e) return i[3] == "1" ? 1 : 0
    };
    return -1
};

function disalbedNullSelect(e, t, n, r) {
    var i = jQuery("#SpecifactionCount").val();
    for (var s = 0; s < parseInt(i); s++) {
        if (e == s) continue;
        var o = isIe() == 1 ? "hand" : "pointer",
            u = jQuery("#spec_group" + s + "_count").val();
        for (var a = 0; a < parseInt(u); a++) {
            var f = "#spec_value" + s + "_" + a,
                l = jQuery(f),
                c = jQuery(f + " a:first-child");
            if (r) l.show(), l.removeClass("goods_spec_off").addClass("goods_spec_on"), c.attr("unable", "false"), c.attr("title", "");
            else {
                var h = c.text(),
                    p = "";
                e > s ? p = h + "," + n : p = n + "," + h;
                var d = isSpecExisted(p);
                switch (d) {
                case -1:
                    l.hide();
                    break;
                case 0:
                    l.show(), l.removeClass("goods_spec_on").addClass("goods_spec_off"), c.attr("unable", "true"), c.attr("title", "已下架");
                    break;
                default:
                    l.show(), l.removeClass("goods_spec_off").addClass("goods_spec_on"), c.attr("unable", "false"), c.attr("title", "")
                }
            }
        }
    }
};

function setBtnClass() {
    jQuery("#outOffHint").html("");
    var e = isSpecSelected(),
        t = isIe() == 1 ? "hand" : "pointer",
        n = e == 1 ? t : "not-allowed",
        r = e == 1 ? "" : "请选择规格";
    document.getElementById("justBuy").style.cursor = n, document.getElementById("addCart").style.cursor = n, document.getElementById("justBuy").title = r, document.getElementById("addCart").title = r;
    if (e) {
        var i = document.getElementById("SpecPrice").value;
        if (i != "") {
            var s = getSpecs(),
                o = i.split("|");
            for (var u = 0; u < o.length; u++) {
                var a = o[u].split(";
	");
                if (a[0] == s) {
                    document.getElementById("Product_No").innerText = "货　　号：" + a[1];
                    var f = document.getElementById("Product_MarketPrice").innerText;
                    document.getElementById("Product_ShopPrice").innerText = a[2], document.getElementById("Product_SavePrice").innerText = (parseFloat(f) - parseFloat(a[2])).toFixed(2), jQuery.ajax({
                        type: "get",
                        dataType: "text",
                        async: !0,
                        url: jQuery("#myAppPath").val() + "/product/productcontent.aspx?Option=outOff&q_productid=" + jQuery("#Product_ID").val() + "&code=" + a[1] + "&t=" + new Date,
                        success: function (e) {
                            e && e != "" && jQuery("#outOffHint").html("目前缺货，" + e + " 到货")
                        }
                    });
                    break
                }
            }
        }
    }
};

function isSpecSelected() {
    var e = jQuery("#SpecifactionCount").val();
    for (var t = 0; t < parseInt(e); t++) {
        var n = "#spec_group" + t;
        if (jQuery(n).val() == "") return !1
    };
    return !0
};

function getSpecs() {
    var e = jQuery("#SpecifactionCount").val();
    if (parseInt(e) <= 0) return null;
    var t = new Array(e);
    for (var n = 0; n < parseInt(e); n++) {
        var r = "#spec_group" + n;
        t[n] = jQuery(r).val()
    };
    return t
};

function justToBuy() {
    if (!isSpecSelected()) return !1;
    var e = getSpecs(),
        t = e == null ? "" : e.join(","),
        n = jQuery("#Immediately_ShoppingUrl").val(),
        r = jQuery("#number").val(),
        i = n + "&q_proSpecification=" + encodeURIComponent(t) + "&q_proCount=" + r;
    window.location.href = i
};

function addToCart(e, t) {
    if (!isSpecSelected()) return !1;
    var n = getSpecs(),
        r = n == null ? "" : n.join(","),
        i = jQuery("#number").val();
    showShopCart("showShopCart", document.getElementById("imgAddCart"), "", 350, 80, t, e, encodeURIComponent(r), i)
};

function oneKeyUp(e, t) {
    alert("近期推出，请期待！")
};

function addToCollect() {
    var e = "q_productid=" + jQuery("#Product_ID").val();
    jQuery.ajax({
        type: "get",
        url: "../filehandle/collection.aspx",
        dataType: "text",
        data: e,
        success: function (e) {
            e == "needLogin" ? alert("请您登录后再收藏商品！") : e == "haveCollect" ? alert("您已经收藏该商品！") : e == "ok" && alert("收藏成功")
        }
    })
};

function mycarousel_initCallback(e) {
    jQuery("#mycarousel li").mouseover(function () {
        var e = jQuery("img", this),
            t = e.attr("name"),
            n = e.attr("id");
        jQuery("#_middleImage").attr("src", t).attr("jqimg", n), jQuery(this).siblings().each(function () {
            jQuery("img", this).removeClass().addClass("")
        }), e.addClass("gallery_selected")
    })
};

function showTab(e) {
    for (var t = 0; t < 4; t++) {
        t != e ? jQuery("#tab" + t).parent().removeClass("curr") : jQuery("#tab" + e).parent().addClass("curr");
        var n = "#tab" + t + "_detail";
        t == e || e == 0 ? jQuery(n).show() : jQuery(n).hide()
    }
};

function submitConsult() {
    var e = jQuery("#email").val(),
        t = /\w+@\w+\.\w+/;
    if (jQuery.trim(e) != "" && !t.test(e)) return alert("Email格式不正确!"), jQuery("#email").focus(), !1;
    if (jQuery.trim(jQuery("#content").val()) == "") return alert("请输入咨询内容！"), jQuery("#content").focus(), !1;
    if (jQuery.trim(jQuery("#check_code").val()) == "") return alert("请输入验证码！"), jQuery("#check_code").focus(), !1;
    var n = "isStoreLeaveWord=" + jQuery("#Product_ID").val() + "&txtLW_UserName=" + encodeURIComponent(jQuery("#UserName").val()) + "&txtLW_Email=" + encodeURIComponent(jQuery.trim(jQuery("#email").val())) + "&txtLW_Content=" + encodeURIComponent(jQuery.trim(jQuery("#content").val())) + "&token=" + jQuery("#token").val() + "&checkcode=" + jQuery("#check_code").val() + "&hfType=" + jQuery("input[name=consult_type]:checked").val();
    jQuery.ajax({
        type: "get",
        async: !1,
        url: "../filehandle/leavewordform.ashx",
        data: n,
        success: function (e) {
            e == "true" ? (jQuery("#email").val(""), jQuery("#content").val(""), jQuery("#check_code").val(""), jQuery(".commentsList").html('<span style="color:blue">您的留言已经提交成功，我们将尽快回复！<span>')) : alert("留言提交失败，请检查验证码是否正确。也可能是页面信息已经过期，请刷新后重新填写再提交！")
        }
    })
};
jQuery(document).ready(function () {
    jQuery("#mycarousel").jcarousel({
        initCallback: mycarousel_initCallback
    }), jQuery("#Fi_pro").jcarousel(), jQuery(".jqzoom").jqueryzoom({
        xzoom: 400,
        yzoom: 400,
        offset: 10,
        position: "right",
        preload: 1,
        lens: 1
    })
}), jQuery(document).ready(function () {
    var e = jQuery("#SpecifactionCount").val();
    for (var t = 0; t < parseInt(e); t++) {
        var n = "#spec_group" + t;
        jQuery(n).attr("value", "")
    };
    setBtnClass(), jQuery("#aCollect").click(function () {
        return addToCollect(), !1
    }), jQuery("#justBuy").click(function () {
        return justToBuy(), !1
    }), jQuery("#tab0").click(function () {
        return showTab(0), !1
    }), jQuery("#tab1").click(function () {
        return showTab(1), !1
    }), jQuery("#tab2").click(function () {
        return showTab(2), !1
    }), jQuery("#tab3").click(function () {
        return showTab(3), !1
    }), jQuery("input[name=consult_type]:eq(0)").attr("checked", "checked"), jQuery("#btnSubmit").click(function () {
        submitConsult()
    })
})