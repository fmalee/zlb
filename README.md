# 众来邦服装分销网(Symfony2)

原`Inmocha`项目。

原版基于`YII`框架创建，现使用`Symfony2`重写。

所以现在是基于`Symfony`框架。

重写项目主要是为学习`Symfony`框架。

本应用原本托管于SAE。
