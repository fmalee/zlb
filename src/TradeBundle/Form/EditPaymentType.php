<?php

namespace TradeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EditPaymentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array(
                'label'       => '代号',
                'attr' => array(
                    'placeholder' => '配送方式代号，如 sto',
                ),
            ))
            ->add('title', 'text', array(
                'label'       => '名称',
                'attr' => array(
                    'placeholder' => '配送方式名称, 如 申通快递',
                ),
            ))
            ->add('homePage', 'url', array(
                'label'       => '相关网页',
                'required' => false,
                'attr' => array(
                    'placeholder' => '相关网页',
                ),
            ))
            ->add('enabled', 'choice', array(
                'label'       => '是否启用',
                'choices' => array(0 => '禁用', 1 => '启用'),
                'expanded'    => true
            ))
            ->add('supportCod', 'choice', array(
                'label'       => '货到付款',
                'choices' => array(0 => '不支持', 1 => '支持'),
                'expanded'    => true
            ))
            ->add('insure', 'text', array(
                'label'       => '收费',
                'required' => false,
                'attr' => array(
                    'placeholder' => '除快递费用外的其他费用',
                ),
            ))
            ->add('sortOrder', 'text', array(
                'label'       => '排序',
            ))
            ->add('description', 'textarea', array(
                'label'    => '说明',
                'required' => false,
                'attr'     => array(
                    'rows'  => 4,
                    'cols'  => 55
                )
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'        => 'TradeBundle\Entity\Payment',
            'validation_groups' => array('Edit')
        ));
    }

    public function getName()
    {
        return 'payment';
    }
}