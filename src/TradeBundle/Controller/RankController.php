<?php

namespace TradeBundle\Controller;

use CoreBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use CoreBundle\Util\Curl;

/**
 * @Route("/tools/rank")
 */
class RankController extends Controller
{
    /**
     * @Route("/", name="rank_home")
     * @Template()
     */
    public function indexAction()
    {
        $this->getBreadcrumb()
            ->add('后台管理', $this->generateUrl('admin_home'))
            ->add('淘宝宝贝排名搜索');
        return array();
    }

    /**
     * @Route("/search", name="rank_search")
     */
    public function searchAction(Request $request)
    {
        $shop = trim($request->get('shop'));
        $keyword = trim($request->get('keyword'));
        $total = $request->get('total') ?: 20;
        if (!$shop) {
            return $this->ajaxReturn('请填写旺旺名称', false);
        }
        if (!$keyword) {
            return $this->ajaxReturn('请填写关键字', false);
        }

        for ($page = 1; $page <= $total; $page++) { 
            $result = $this->taobao($shop, $keyword, $page);
            if ($result) {
                $message = '在 <a href="' . $result['search_url'] .'" target="_blank">第' . $page . '页的' . $result['position'];
                $message .= '位</a> 找到商品：<a href="'. $result['link'] .'" target="_blank">' . $result['title'] .'</a>';

                return $this->ajaxReturn($message);
            }

        }

        $result = '在' . $total . '页内没有找到属于 ' . $shop . ' 的宝贝，请更换关键字';
        return $this->ajaxReturn($result, false);
    }

    private function taobao($shop, $keyword, $page = 1)
    {
        $searchUrl = 'http://s.taobao.com/search?tab=all&promote=0&bcoffset=1';
        $keyword = urlencode(iconv("UTF-8", "GBK", $keyword));
        $searchUrl .= '&q=' . $keyword;
        if ($page > 1) {
            $offset = ($page-1)*40;
            $searchUrl .= '&s=' . $offset;
        }

        /*获取搜索页面*/
        $curl = New Curl();
        $searchContent = $curl->get($searchUrl);
        $searchContent = iconv("GBK", "UTF-8",$searchContent);

        /*先匹配店铺*/
        $shopPreg = "/<a\strace=\"srpwwnick\"\starget=\"_blank\"\shref=\"(.+?)\">(.+?)<\/a>/";
        preg_match_all($shopPreg, $searchContent, $content);
        $shops = $content[2];

        if (in_array($shop, $shops)) {
            $position = array_keys($shops, $shop);
            $position = array_shift($position);

            /*匹配对应商品*/
            $itemPreg = "/<h3\sclass=\"summary\"><a\strace=\"auction\"\straceNum=\"2\"\shref=\"(.+?)\"\starget=\"_blank\"\stitle=\"(.+?)\">/";
            preg_match_all($itemPreg, $searchContent, $content);

            /*返回数组*/
            return array(
                'search_url' => $searchUrl,
                'position' => $position+1,
                'link' => $content[1][$position],
                'title' => $content[2][$position]
            );
        }

        return false;
    }
}
