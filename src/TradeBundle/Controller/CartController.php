<?php

namespace TradeBundle\Controller;

use CoreBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/cart")
 */
class CartController extends Controller
{
    /**
     * @Route("/view", name="cart_view")
     */
    public function viewAction(Request $request)
    {
/*        $content = $this->renderView('AdminBundle:Sku:add.html.twig', array(
            'supplies' => $supplies,
            'productId' => $productId,
            'supplyId' => $supplyId,
            'product' => $product
        ));*/

        $content = "<li><h1>暂时没有订单</h1></li>";
        $response = new Response();
        $response->setContent($content);
        return $response;
    }

    /**
     * @Route("/add", name="cart_add")
     */
    public function addAction(Request $request)
    {
        $productId = $request->get('product_id');
        $currentPrice = $request->get('current_price');
        $sku1 = $request->get('sku_1');
        $quantity = $request->get('quantity');
        $sessionId = $request->cookies->get('PHPSESSID');
        if (!$productId || !$currentPrice || !$sku1 || !$quantity) {
            if (!$productId || !$currentPrice) {
                $msg = '参数错误';
            }
            if (!$sku1) {
                $msg = '请选择颜色和尺码';
            }
            if (!$quantity) {
                $msg = '请选择数量';
            }
            return $this->ajaxReturn(array('error' => $msg), false);
        }

        $product = $this->getProductRepository()->find($productId);
        if (!$product) {
            return $this->ajaxReturn('商品不存在', false);
        }

        $sku2 = $request->get('sku_2');
        if ($product->getSku2() && !$sku2) {
            return $this->ajaxReturn(array('error' => '请选择' . $product->getSku2()), false);
        }

        $sku = $this->getSkuRepository()->findSkuBySkuId($productId, $sku1, $sku2);
        if (!$sku) {
            return $this->ajaxReturn(array('error' => $sku1 . ': ' . $sku2 . '已经下架,请重新选择'), false);
        }
        $skuId = $sku->getId();

        if (null != $this->getUser()) {
            $userId = $this->getUser()->getId();
            $cart = $this->getCartRepository()->findOneBy(array(
                'userId' => $userId,
                'productId' => $productId,
                'skuId' => $skuId
            ));
        } else {
            $userId = 0;
            $cart = $this->getCartRepository()->findOneBy(array(
                'userId' => 0,
                'sessionId' => $sessionId,
                'productId' => $productId,
                'skuId' => $skuId
            ));
        }

        if ($cart) {
            $cart->setQuantity($quantity + $cart->getQuantity());
        } else {
            $productAttr = $product->getSku1() . ':' . $sku1 .';';
            if ($sku2) {
                $productAttr .= $product->getSku2() . ':' . $sku2 .';';
            }

            $cart = $this->getCartRepository()->createNew();
            $cart->setUserId($userId);
            $cart->setSessionId($sessionId);
            $cart->setCurrentPrice($currentPrice);
            $cart->setQuantity($quantity);
            $cart->setProductAttr($productAttr);
            $cart->setProduct($product);
            $cart->setSku($sku);
        }
        $this->persist($cart, true);

        if ($userId) {
            $carts = $this->getCartRepository()->findByUserId($userId);
        } else {
            $carts = $this->getCartRepository()->findBySessionId($sessionId);
        }
        if ($carts) {
            $products = $price = 0;
            foreach ($carts as $cart) {
                $products += $cart->getQuantity();
                $price += $cart->getCurrentPrice() * $cart->getQuantity();
            }
        }

        $content = array(
            'info' => '成功添加商品到您的购物车',
            'total' => "$products 个商品 - $price"
        );
        return $this->ajaxReturn($content);
    }

    /**
     * @return CartRepository
     */
    private function getCartRepository()
    {
        return $this->get('trade.entity.cart_repository');
    }

    /**
     * @return ProductRepository
     */
    private function getProductRepository()
    {
        return $this->get('product.entity.product_repository');
    }

    /**
     * @return SkuRepository
     */
    private function getSkuRepository()
    {
        return $this->get('product.entity.sku_repository');
    }

    /**
     * @return PaymentRepository
     */
    private function getPaymentRepository()
    {
        return $this->get('trade.entity.payment_repository');
    }
}
