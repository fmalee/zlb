<?php

namespace AdminBundle\Controller;

use CoreBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/trade/shipping")
 */
class ShippingController extends Controller
{

    /**
     * @return ShippingRepository
     */
    private function getShippingRepository()
    {
        return $this->get('trade.entity.shipping_repository');
    }
}
