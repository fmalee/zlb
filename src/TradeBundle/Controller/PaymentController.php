<?php

namespace TradeBundle\Controller;

use CoreBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/trade/payment")
 */
class PaymentController extends Controller
{

    /**
     * @return PaymentRepository
     */
    private function getPaymentRepository()
    {
        return $this->get('trade.entity.payment_repository');
    }
}
