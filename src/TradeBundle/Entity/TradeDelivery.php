<?php

namespace TradeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Trade Delivery Entity
 *
 * @ORM\Entity(repositoryClass="TradeBundle\Entity\TradeDeliveryRepository")
 * @ORM\Table(name="mc_trade_delivery", indexes={@ORM\Index(name="status", columns={"status", "id"})})
 * @ORM\HasLifecycleCallbacks()
 */
class TradeDelivery
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="shipping_id", type="integer")
     */
    private $shippingId;

    /**
     * @var integer
     *
     * @ORM\Column(name="trade_id", type="integer")
     */
    private $tradeId;

    /**
     * @var smallint
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;

    /**
     * @var float
     *
     * @ORM\Column(name="payment", type="float", precision=10, scale=0)
     */
    private $payment;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_cod", type="boolean")
     */
    private $isCod;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_payer", type="boolean")
     */
    private $isPayer;

    /**
     * @var string
     *
     * @ORM\Column(name="shipping_name", type="string", length=20)
     */
    private $shippingName;

    /**
     * @var string
     *
     * @ORM\Column(name="delivery_number", type="string", length=15)
     */
    private $deliveryNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="delivery_start", type="integer", nullable=true)
     */
    private $deliveryStart;

    /**
     * @var integer
     *
     * @ORM\Column(name="delivery_end", type="integer", nullable=true)
     */
    private $deliveryEnd;

    /**
     * @var string
     *
     * @ORM\Column(name="invoice_no", type="string", nullable=true)
     */
    private $invoiceNo;

    /**
     * @var string
     *
     * @ORM\Column(name="memo", type="string", length=255, nullable=true)
     */
    private $memo;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="modified", type="datetime")
     */
    private $modified;

    /**
     * @var Trade[]
     * @ORM\ManyToOne(targetEntity="TradeBundle\Entity\Trade", inversedBy="refunds")
     * @ORM\JoinColumn(name="trade_id", referencedColumnName="id")
     */
    private $trade;

    /**
     * @ORM\ManyToOne(targetEntity="TradeBundle\Entity\Shipping", inversedBy="tradeDeliveries")
     * @ORM\JoinColumn(name="shipping_id", referencedColumnName="id")
     */
    private $shipping;

    public function __construct()
    {
        $this->status      = 0;
        $this->isCod       = false;
        $this->isPayer     = false;
        $this->created     = new \Datetime();
        $this->modified    = new \Datetime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function setPerUpdate()
    {
        $this->modified = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return TradeDelivery
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set shippingId
     *
     * @param integer $shippingId
     * @return TradeDelivery
     */
    public function setShippingId($shippingId)
    {
        $this->shippingId = $shippingId;

        return $this;
    }

    /**
     * Get shippingId
     *
     * @return integer 
     */
    public function getShippingId()
    {
        return $this->shippingId;
    }

    /**
     * Set tradeId
     *
     * @param integer $tradeId
     * @return TradeDelivery
     */
    public function setTradeId($tradeId)
    {
        $this->tradeId = $tradeId;

        return $this;
    }

    /**
     * Get tradeId
     *
     * @return integer 
     */
    public function getTradeId()
    {
        return $this->tradeId;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return TradeDelivery
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set payment
     *
     * @param float $payment
     * @return TradeDelivery
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * Get payment
     *
     * @return float 
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * Set isCod
     *
     * @param boolean $isCod
     * @return TradeDelivery
     */
    public function setIsCod($isCod)
    {
        $this->isCod = $isCod;

        return $this;
    }

    /**
     * Get isCod
     *
     * @return boolean 
     */
    public function getIsCod()
    {
        return $this->isCod;
    }

    /**
     * Set isPayer
     *
     * @param boolean $isPayer
     * @return TradeDelivery
     */
    public function setIsPayer($isPayer)
    {
        $this->isPayer = $isPayer;

        return $this;
    }

    /**
     * Get isPayer
     *
     * @return boolean 
     */
    public function getIsPayer()
    {
        return $this->isPayer;
    }

    /**
     * Set shippingName
     *
     * @param string $shippingName
     * @return TradeDelivery
     */
    public function setShippingName($shippingName)
    {
        $this->shippingName = $shippingName;

        return $this;
    }

    /**
     * Get shippingName
     *
     * @return string 
     */
    public function getShippingName()
    {
        return $this->shippingName;
    }

    /**
     * Set deliveryNumber
     *
     * @param string $deliveryNumber
     * @return TradeDelivery
     */
    public function setDeliveryNumber($deliveryNumber)
    {
        $this->deliveryNumber = $deliveryNumber;

        return $this;
    }

    /**
     * Get deliveryNumber
     *
     * @return string 
     */
    public function getDeliveryNumber()
    {
        return $this->deliveryNumber;
    }

    /**
     * Set deliveryStart
     *
     * @param integer $deliveryStart
     * @return TradeDelivery
     */
    public function setDeliveryStart($deliveryStart)
    {
        $this->deliveryStart = $deliveryStart;

        return $this;
    }

    /**
     * Get deliveryStart
     *
     * @return integer 
     */
    public function getDeliveryStart()
    {
        return $this->deliveryStart;
    }

    /**
     * Set deliveryEnd
     *
     * @param integer $deliveryEnd
     * @return TradeDelivery
     */
    public function setDeliveryEnd($deliveryEnd)
    {
        $this->deliveryEnd = $deliveryEnd;

        return $this;
    }

    /**
     * Get deliveryEnd
     *
     * @return integer 
     */
    public function getDeliveryEnd()
    {
        return $this->deliveryEnd;
    }

    /**
     * Set invoiceNo
     *
     * @param string $invoiceNo
     * @return TradeDelivery
     */
    public function setInvoiceNo($invoiceNo)
    {
        $this->invoiceNo = $invoiceNo;

        return $this;
    }

    /**
     * Get invoiceNo
     *
     * @return string 
     */
    public function getInvoiceNo()
    {
        return $this->invoiceNo;
    }

    /**
     * Set memo
     *
     * @param string $memo
     * @return TradeDelivery
     */
    public function setMemo($memo)
    {
        $this->memo = $memo;

        return $this;
    }

    /**
     * Get memo
     *
     * @return string 
     */
    public function getMemo()
    {
        return $this->memo;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return TradeDelivery
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return TradeDelivery
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set trade
     *
     * @param \TradeBundle\Entity\Trade $trade
     * @return TradeDelivery
     */
    public function setTrade(\TradeBundle\Entity\Trade $trade = null)
    {
        $this->trade = $trade;

        return $this;
    }

    /**
     * Get trade
     *
     * @return \TradeBundle\Entity\Trade 
     */
    public function getTrade()
    {
        return $this->trade;
    }

    /**
     * Set shipping
     *
     * @param \TradeBundle\Entity\Shipping $shipping
     * @return TradeDelivery
     */
    public function setShipping(\TradeBundle\Entity\Shipping $shipping = null)
    {
        $this->shipping = $shipping;

        return $this;
    }

    /**
     * Get shipping
     *
     * @return \TradeBundle\Entity\Shipping
     */
    public function getShipping()
    {
        return $this->shipping;
    }
}
