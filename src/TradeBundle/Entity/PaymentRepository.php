<?php

namespace TradeBundle\Entity;

use CoreBundle\Doctrine\EntityRepository;

/**
 * Payment Repository
 */
class PaymentRepository extends EntityRepository
{
    /**
     * {@inheritdoc}
     */
    public function findPaymentBy(array $criteria)
    {
        return $this->findOneBy($criteria);
    }

}
