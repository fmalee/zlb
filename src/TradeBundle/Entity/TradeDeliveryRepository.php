<?php

namespace TradeBundle\Entity;

use CoreBundle\Doctrine\EntityRepository;

/**
 * Trade Delivery Repository
 */
class TradeDeliveryRepository extends EntityRepository
{
    /**
     * {@inheritdoc}
     */
    public function findDeliveryBy(array $criteria)
    {
        return $this->findOneBy($criteria);
    }

}
