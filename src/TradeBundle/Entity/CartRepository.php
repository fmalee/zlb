<?php

namespace TradeBundle\Entity;

use CoreBundle\Doctrine\EntityRepository;

/**
 * Cart Repository
 */
class CartRepository extends EntityRepository
{
    /**
     * {@inheritdoc}
     */
    public function findPCartBy(array $criteria)
    {
        return $this->findOneBy($criteria);
    }

    /**
     * {@inheritdoc}
     */
    public function updateUserBySession($userId, $sessionId)
    {
        $qb = $this->createQueryBuilder('c')
            ->update()
            ->set('c.userId', ':userId')
            ->where('c.sessionId = :sessionId')
            ->setParameter('userId', $userId)
            ->setParameter('sessionId', $sessionId);

        $qb->getQuery()->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function updateSessionByUser($sessionId, $userId)
    {
        $qb = $this->createQueryBuilder('c')
            ->update()
            ->set('c.sessionId', ':sessionId')
            ->where('c.userId = :userId')
            ->setParameter('sessionId', $sessionId)
            ->setParameter('userId', $userId);

        $qb->getQuery()->execute();
    }
}
