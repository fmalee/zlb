<?php

namespace TradeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cart Entity
 *
 * @ORM\Entity(repositoryClass="TradeBundle\Entity\CartRepository")
 * @ORM\Table(name="mc_cart", indexes={@ORM\Index(columns={"session_id"})})
 * @ORM\HasLifecycleCallbacks()
 */
class Cart
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer")
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="session_id", type="string", length=30)
     */
    private $sessionId;

    /**
     * @var integer
     *
     * @ORM\Column(name="product_id", type="integer")
     */
    private $productId;

    /**
     * @var integer
     *
     * @ORM\Column(name="sku_id", type="integer")
     */
    private $skuId;

    /**
     * @var decimal
     *
     * @ORM\Column(name="current_price", type="decimal", scale=2)
     */
    protected $currentPrice;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="integer")
     */
    private $quantity;

    /**
     * @var string
     *
     * @ORM\Column(name="product_attr", type="string", length=255)
     */
    private $productAttr;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="modified", type="datetime")
     */
    private $modified;

    /**
     * @ORM\ManyToOne(targetEntity="ProductBundle\Entity\Product", inversedBy="carts")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity="ProductBundle\Entity\Sku", inversedBy="carts")
     * @ORM\JoinColumn(name="sku_id", referencedColumnName="id")
     */
    private $sku;

    public function __construct()
    {
        $this->userId       = 0;
        //$this->sessionId   = md5(uniqid(rand()));
        $this->quantity          = 1;
        $this->created      = new \Datetime();
        $this->modified     = new \Datetime();
    }

    public function __toString()
    {
        return $this->sessionId;
    }

    /**
     * @ORM\PreUpdate
     */
    public function setPerUpdate()
    {
        $this->modified = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return Cart
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set sessionId
     *
     * @param string $sessionId
     * @return Cart
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;

        return $this;
    }

    /**
     * Get sessionId
     *
     * @return string 
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * Set productId
     *
     * @param integer $productId
     * @return Cart
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * Get productId
     *
     * @return integer 
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * Set skuId
     *
     * @param integer $skuId
     * @return Cart
     */
    public function setSkuId($skuId)
    {
        $this->skuId = $skuId;

        return $this;
    }

    /**
     * Get skuId
     *
     * @return integer 
     */
    public function getSkuId()
    {
        return $this->skuId;
    }

    /**
     * Set currentPrice
     *
     * @param string $currentPrice
     * @return Cart
     */
    public function setCurrentPrice($currentPrice)
    {
        $this->currentPrice = $currentPrice;

        return $this;
    }

    /**
     * Get currentPrice
     *
     * @return string 
     */
    public function getCurrentPrice()
    {
        return $this->currentPrice;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return Cart
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set productAttr
     *
     * @param string $productAttr
     * @return Cart
     */
    public function setProductAttr($productAttr)
    {
        $this->productAttr = $productAttr;

        return $this;
    }

    /**
     * Get productAttr
     *
     * @return string 
     */
    public function getProductAttr()
    {
        return $this->productAttr;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Cart
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Cart
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set product
     *
     * @param \ProductBundle\Entity\Product $product
     * @return Cart
     */
    public function setProduct(\ProductBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \ProductBundle\Entity\Product 
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set sku
     *
     * @param \ProductBundle\Entity\Sku $sku
     * @return Cart
     */
    public function setSku(\ProductBundle\Entity\Sku $sku = null)
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * Get sku
     *
     * @return \ProductBundle\Entity\Sku 
     */
    public function getSku()
    {
        return $this->sku;
    }
}
