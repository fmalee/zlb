<?php

namespace TradeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Trade Entity
 *
 * @ORM\Entity(repositoryClass="TradeBundle\Entity\TradeRepository")
 * @ORM\Table(name="mc_trade", indexes={@ORM\Index(name="status", columns={"status", "id"})})
 * @ORM\HasLifecycleCallbacks()
 */
class Trade
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer")
     */
    private $userId;

    /**
     * @var smallint
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;

    /**
     * @var smallint
     *
     * @ORM\Column(name="cod_status", type="smallint", nullable=true)
     */
    private $codStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="buyer_message", type="string", length=255, nullable=true)
     */
    private $buyerMessage;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="pay_at", type="datetime", nullable=true)
     */
    private $payAt;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="consign_at", type="datetime", nullable=true)
     */
    private $consignAt;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="shipping_at", type="datetime", nullable=true)
     */
    private $shippingAt;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="end_at", type="datetime", nullable=true)
     */
    private $endAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="alipay_id", type="integer", nullable=true)
     */
    private $alipayId;

    /**
     * @var integer
     *
     * @ORM\Column(name="alipay_no", type="integer", nullable=true)
     */
    private $alipayNo;

    /**
     * @var float
     * 产品总额
     *
     * @ORM\Column(name="product_amount", type="float", precision=10, scale=0)
     */
    private $productAmount;

    /**
     * @var float
     * 快递
     *
     * @ORM\Column(name="shipping_fee", type="float", precision=10, scale=0)
     */
    private $shippingFee;

    /**
     * @var float
     * 优惠
     *
     * @ORM\Column(name="discount_fee", type="float", precision=10, scale=0)
     */
    private $discountFee;

    /**
     * @var float
     * 积分
     *
     * @ORM\Column(name="point_fee", type="float", precision=10, scale=0)
     */
    private $pointFee;

    /**
     * @var float
     * 红包
     *
     * @ORM\Column(name="bonus_fee", type="float", precision=10, scale=0)
     */
    private $bonusFee;

    /**
     * @var float
     * 余额支付
     *
     * @ORM\Column(name="surplus_fee", type="float", precision=10, scale=0)
     */
    private $surplusFee;

    /**
     * @var float
     * 其他支付
     *
     * @ORM\Column(name="pay_fee", type="float", precision=10, scale=0)
     */
    private $payFee;

    /**
     * @var float
     * 应收款
     *
     * @ORM\Column(name="received_Amount", type="float", precision=10, scale=0)
     */
    private $receivedAmount;

    /**
     * @var float
     *
     *
     * @ORM\Column(name="total_fee", type="float", precision=10, scale=0)
     */
    private $totalFee;

    /**
     * @var integer
     *
     * @ORM\Column(name="payment_id", type="integer")
     */
    private $paymentId;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_name", type="string", length=40)
     */
    private $paymentName;

    /**
     * @var smallint
     *
     * @ORM\Column(name="buyer_flag", type="smallint")
     */
    private $buyerFlag;

    /**
     * @var smallint
     *
     * @ORM\Column(name="seller_flag", type="smallint")
     */
    private $sellerFlag;

    /**
     * @var string
     *
     * @ORM\Column(name="seller_memo", type="string", length=255, nullable=true)
     */
    private $sellerMemo;

    /**
     * @var string
     *
     * @ORM\Column(name="buyer_memo", type="string", length=255, nullable=true)
     */
    private $buyerMemo;

    /**
     * @var string
     *
     * @ORM\Column(name="buyer_alipay_no", type="string", length=100, nullable=true)
     */
    private $buyerAlipayNo;

    /**
     * @var string
     *
     * @ORM\Column(name="receiver_name", type="string", length=40)
     */
    private $receiverName;

    /**
     * @var string
     *
     * @ORM\Column(name="receiver_province", type="string", length=40)
     */
    private $receiverProvince;

    /**
     * @var string
     *
     * @ORM\Column(name="receiver_city", type="string", length=40)
     */
    private $receiverCity;

    /**
     * @var string
     *
     * @ORM\Column(name="receiver_district", type="string", length=40)
     */
    private $receiverDistrict;

    /**
     * @var string
     *
     * @ORM\Column(name="receiver_address", type="string", length=255)
     */
    private $receiverAddress;

    /**
     * @var integer
     *
     * @ORM\Column(name="receiver_zip", type="integer")
     */
    private $receiverZip;

    /**
     * @var integer
     *
     * @ORM\Column(name="area_id", type="integer")
     */
    private $areaId;

    /**
     * @var integer
     *
     * @ORM\Column(name="receiver_mobile", type="bigint", nullable=true)
     */
    private $receiverMobile;

    /**
     * @var string
     *
     * @ORM\Column(name="receiver_phone", type="string", length=100, nullable=true)
     */
    private $receiverPhone;

    /**
     * @var string
     *
     * @ORM\Column(name="invoice_name", type="string", length=100, nullable=true)
     */
    private $invoiceName;

    /**
     * @var integer
     *
     * @ORM\Column(name="invoice_no", type="integer", nullable=true)
     */
    private $invoiceNo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="can_rate", type="boolean")
     */
    private $canRate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="rated", type="boolean")
     */
    private $rated;

    /**
     * @var boolean
     *
     * @ORM\Column(name="refunded", type="boolean")
     */
    private $refunded;

    /**
     * @var string
     *
     * @ORM\Column(name="trade_from", type="string", length=100)
     */
    private $tradeFrom;

    /**
     * @var integer
     *
     * @ORM\Column(name="shipping_id", type="integer")
     */
    private $shippingId;

    /**
     * @var string
     *
     * @ORM\Column(name="shipping_name", type="string", length=20)
     */
    private $shippingName;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="service_at", type="datetime", nullable=true)
     */
    private $serviceAt;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="modified", type="datetime")
     */
    private $modified;

    /**
     * @ORM\OneToMany(targetEntity="TradeBundle\Entity\Order", mappedBy="trade")
     */
    private $orders;

    /**
     * @ORM\OneToMany(targetEntity="TradeBundle\Entity\Refund", mappedBy="trade")
     */
    private $refunds;

    /**
     * @ORM\ManyToOne(targetEntity="CommonBundle\Entity\Area", inversedBy="trades")
     * @ORM\JoinColumn(name="area_id", referencedColumnName="id")
     */
    private $area;

    /**
     * @ORM\ManyToOne(targetEntity="TradeBundle\Entity\Shipping", inversedBy="trades")
     * @ORM\JoinColumn(name="Shipping_id", referencedColumnName="id")
     */
    private $shipping;

    /**
     * @ORM\ManyToOne(targetEntity="TradeBundle\Entity\Payment", inversedBy="trades")
     * @ORM\JoinColumn(name="payment_id", referencedColumnName="id")
     */
    private $payment;

    public function __construct()
    {
        $this->status      = 0;
        $this->good_status = 0;
        $this->buyerFlag   = 0;
        $this->sellerFlag  = 0;
        $this->canRate     = true;
        $this->rated       = false;
        $this->refunded    = false;
        $this->created     = new \Datetime();
        $this->modified    = new \Datetime();
        $this->orders      = new ArrayCollection();
        $this->refunds     = new ArrayCollection();
    }

    /**
     * @ORM\PreUpdate
     */
    public function setPerUpdate()
    {
        $this->modified = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return Trade
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return Trade
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Trade
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set codStatus
     *
     * @param integer $codStatus
     * @return Trade
     */
    public function setCodStatus($codStatus)
    {
        $this->codStatus = $codStatus;

        return $this;
    }

    /**
     * Get codStatus
     *
     * @return integer 
     */
    public function getCodStatus()
    {
        return $this->codStatus;
    }

    /**
     * Set buyerMessage
     *
     * @param string $buyerMessage
     * @return Trade
     */
    public function setBuyerMessage($buyerMessage)
    {
        $this->buyerMessage = $buyerMessage;

        return $this;
    }

    /**
     * Get buyerMessage
     *
     * @return string 
     */
    public function getBuyerMessage()
    {
        return $this->buyerMessage;
    }

    /**
     * Set payAt
     *
     * @param \DateTime $payAt
     * @return Trade
     */
    public function setPayAt($payAt)
    {
        $this->payAt = $payAt;

        return $this;
    }

    /**
     * Get payAt
     *
     * @return \DateTime 
     */
    public function getPayAt()
    {
        return $this->payAt;
    }

    /**
     * Set consignAt
     *
     * @param \DateTime $consignAt
     * @return Trade
     */
    public function setConsignAt($consignAt)
    {
        $this->consignAt = $consignAt;

        return $this;
    }

    /**
     * Get consignAt
     *
     * @return \DateTime 
     */
    public function getConsignAt()
    {
        return $this->consignAt;
    }

    /**
     * Set shippingAt
     *
     * @param \DateTime $shippingAt
     * @return Trade
     */
    public function setShippingAt($shippingAt)
    {
        $this->shippingAt = $shippingAt;

        return $this;
    }

    /**
     * Get shippingAt
     *
     * @return \DateTime 
     */
    public function getShippingAt()
    {
        return $this->shippingAt;
    }

    /**
     * Set endAt
     *
     * @param \DateTime $endAt
     * @return Trade
     */
    public function setEndAt($endAt)
    {
        $this->endAt = $endAt;

        return $this;
    }

    /**
     * Get endAt
     *
     * @return \DateTime 
     */
    public function getEndAt()
    {
        return $this->endAt;
    }

    /**
     * Set alipayId
     *
     * @param integer $alipayId
     * @return Trade
     */
    public function setAlipayId($alipayId)
    {
        $this->alipayId = $alipayId;

        return $this;
    }

    /**
     * Get alipayId
     *
     * @return integer 
     */
    public function getAlipayId()
    {
        return $this->alipayId;
    }

    /**
     * Set alipayNo
     *
     * @param integer $alipayNo
     * @return Trade
     */
    public function setAlipayNo($alipayNo)
    {
        $this->alipayNo = $alipayNo;

        return $this;
    }

    /**
     * Get alipayNo
     *
     * @return integer 
     */
    public function getAlipayNo()
    {
        return $this->alipayNo;
    }

    /**
     * Set totalFee
     *
     * @param float $totalFee
     * @return Trade
     */
    public function setTotalFee($totalFee)
    {
        $this->totalFee = $totalFee;

        return $this;
    }

    /**
     * Get totalFee
     *
     * @return float 
     */
    public function getTotalFee()
    {
        return $this->totalFee;
    }

    /**
     * Set productAmount
     *
     * @param float $productAmount
     * @return Trade
     */
    public function setProductAmount($productAmount)
    {
        $this->productAmount = $productAmount;

        return $this;
    }

    /**
     * Get productAmount
     *
     * @return float 
     */
    public function getProductAmount()
    {
        return $this->productAmount;
    }

    /**
     * Set shippingFee
     *
     * @param float $shippingFee
     * @return Trade
     */
    public function setShippingFee($shippingFee)
    {
        $this->shippingFee = $shippingFee;

        return $this;
    }

    /**
     * Get shippingFee
     *
     * @return float 
     */
    public function getShippingFee()
    {
        return $this->shippingFee;
    }

    /**
     * Set discountFee
     *
     * @param string $discountFee
     * @return Trade
     */
    public function setDiscountFee($discountFee)
    {
        $this->discountFee = $discountFee;

        return $this;
    }

    /**
     * Get discountFee
     *
     * @return string 
     */
    public function getDiscountFee()
    {
        return $this->discountFee;
    }

    /**
     * Set pointFee
     *
     * @param float $pointFee
     * @return Trade
     */
    public function setPointFee($pointFee)
    {
        $this->pointFee = $pointFee;

        return $this;
    }

    /**
     * Get pointFee
     *
     * @return float 
     */
    public function getPointFee()
    {
        return $this->pointFee;
    }

    /**
     * Set bonusFee
     *
     * @param float $bonusFee
     * @return Trade
     */
    public function setBonusFee($bonusFee)
    {
        $this->bonusFee = $bonusFee;

        return $this;
    }

    /**
     * Get bonusFee
     *
     * @return float 
     */
    public function getBonusFee()
    {
        return $this->bonusFee;
    }

    /**
     * Set surplusFee
     *
     * @param float $surplusFee
     * @return Trade
     */
    public function setSurplusFee($surplusFee)
    {
        $this->surplusFee = $surplusFee;

        return $this;
    }

    /**
     * Get surplusFee
     *
     * @return float 
     */
    public function getSurplusFee()
    {
        return $this->surplusFee;
    }

    /**
     * Set payFee
     *
     * @param float $payFee
     * @return Trade
     */
    public function setPayFee($payFee)
    {
        $this->payFee = $payFee;

        return $this;
    }

    /**
     * Get payFee
     *
     * @return float
     */
    public function getPayFee()
    {
        return $this->payFee;
    }

    /**
     * Set receivedAmount
     *
     * @param float $receivedAmount
     * @return Trade
     */
    public function setReceivedAmount($receivedAmount)
    {
        $this->receivedAmount = $receivedAmount;

        return $this;
    }

    /**
     * Get receivedAmount
     *
     * @return float
     */
    public function getReceivedAmount()
    {
        return $this->receivedAmount;
    }

    /**
     * Set paymentId
     *
     * @param integer $paymentId
     * @return Trade
     */
    public function setPaymentId($paymentId)
    {
        $this->paymentId = $paymentId;

        return $this;
    }

    /**
     * Get paymentId
     *
     * @return integer
     */
    public function getPaymentId()
    {
        return $this->paymentId;
    }

    /**
     * Set paymentName
     *
     * @param string $paymentName
     * @return Trade
     */
    public function setPaymentName($paymentName)
    {
        $this->paymentName = $paymentName;

        return $this;
    }

    /**
     * Get paymentName
     *
     * @return string
     */
    public function getPaymentName()
    {
        return $this->paymentName;
    }
    /**
     * Set buyerFlag
     *
     * @param integer $buyerFlag
     * @return Trade
     */
    public function setBuyerFlag($buyerFlag)
    {
        $this->buyerFlag = $buyerFlag;

        return $this;
    }

    /**
     * Get buyerFlag
     *
     * @return integer 
     */
    public function getBuyerFlag()
    {
        return $this->buyerFlag;
    }

    /**
     * Set sellerFlag
     *
     * @param integer $sellerFlag
     * @return Trade
     */
    public function setSellerFlag($sellerFlag)
    {
        $this->sellerFlag = $sellerFlag;

        return $this;
    }

    /**
     * Get sellerFlag
     *
     * @return integer 
     */
    public function getSellerFlag()
    {
        return $this->sellerFlag;
    }

    /**
     * Set sellerMemo
     *
     * @param string $sellerMemo
     * @return Trade
     */
    public function setSellerMemo($sellerMemo)
    {
        $this->sellerMemo = $sellerMemo;

        return $this;
    }

    /**
     * Get sellerMemo
     *
     * @return string 
     */
    public function getSellerMemo()
    {
        return $this->sellerMemo;
    }

    /**
     * Set buyerMemo
     *
     * @param string $buyerMemo
     * @return Trade
     */
    public function setBuyerMemo($buyerMemo)
    {
        $this->buyerMemo = $buyerMemo;

        return $this;
    }

    /**
     * Get buyerMemo
     *
     * @return string 
     */
    public function getBuyerMemo()
    {
        return $this->buyerMemo;
    }

    /**
     * Set buyerAlipayNo
     *
     * @param string $buyerAlipayNo
     * @return Trade
     */
    public function setBuyerAlipayNo($buyerAlipayNo)
    {
        $this->buyerAlipayNo = $buyerAlipayNo;

        return $this;
    }

    /**
     * Get buyerAlipayNo
     *
     * @return string 
     */
    public function getBuyerAlipayNo()
    {
        return $this->buyerAlipayNo;
    }

    /**
     * Set receiverName
     *
     * @param string $receiverName
     * @return Trade
     */
    public function setReceiverName($receiverName)
    {
        $this->receiverName = $receiverName;

        return $this;
    }

    /**
     * Get receiverName
     *
     * @return string 
     */
    public function getReceiverName()
    {
        return $this->receiverName;
    }

    /**
     * Set receiverProvince
     *
     * @param string $receiverProvince
     * @return Trade
     */
    public function setReceiverProvince($receiverProvince)
    {
        $this->receiverProvince = $receiverProvince;

        return $this;
    }

    /**
     * Get receiverProvince
     *
     * @return string 
     */
    public function getReceiverProvince()
    {
        return $this->receiverProvince;
    }

    /**
     * Set receiverCity
     *
     * @param string $receiverCity
     * @return Trade
     */
    public function setReceiverCity($receiverCity)
    {
        $this->receiverCity = $receiverCity;

        return $this;
    }

    /**
     * Get receiverCity
     *
     * @return string 
     */
    public function getReceiverCity()
    {
        return $this->receiverCity;
    }

    /**
     * Set receiverDistrict
     *
     * @param string $receiverDistrict
     * @return Trade
     */
    public function setReceiverDistrict($receiverDistrict)
    {
        $this->receiverDistrict = $receiverDistrict;

        return $this;
    }

    /**
     * Get receiverDistrict
     *
     * @return string 
     */
    public function getReceiverDistrict()
    {
        return $this->receiverDistrict;
    }

    /**
     * Set receiverAddress
     *
     * @param string $receiverAddress
     * @return Trade
     */
    public function setReceiverAddress($receiverAddress)
    {
        $this->receiverAddress = $receiverAddress;

        return $this;
    }

    /**
     * Get receiverAddress
     *
     * @return string 
     */
    public function getReceiverAddress()
    {
        return $this->receiverAddress;
    }

    /**
     * Set receiverZip
     *
     * @param integer $receiverZip
     * @return Trade
     */
    public function setReceiverZip($receiverZip)
    {
        $this->receiverZip = $receiverZip;

        return $this;
    }

    /**
     * Get receiverZip
     *
     * @return integer 
     */
    public function getReceiverZip()
    {
        return $this->receiverZip;
    }

    /**
     * Set areaId
     *
     * @param integer $areaId
     * @return Trade
     */
    public function setAreaId($areaId)
    {
        $this->areaId = $areaId;

        return $this;
    }

    /**
     * Get areaId
     *
     * @return integer 
     */
    public function getAreaId()
    {
        return $this->areaId;
    }

    /**
     * Set receiverMobile
     *
     * @param integer $receiverMobile
     * @return Trade
     */
    public function setReceiverMobile($receiverMobile)
    {
        $this->receiverMobile = $receiverMobile;

        return $this;
    }

    /**
     * Get receiverMobile
     *
     * @return integer 
     */
    public function getReceiverMobile()
    {
        return $this->receiverMobile;
    }

    /**
     * Set receiverPhone
     *
     * @param string $receiverPhone
     * @return Trade
     */
    public function setReceiverPhone($receiverPhone)
    {
        $this->receiverPhone = $receiverPhone;

        return $this;
    }

    /**
     * Get receiverPhone
     *
     * @return string 
     */
    public function getReceiverPhone()
    {
        return $this->receiverPhone;
    }

    /**
     * Set invoiceName
     *
     * @param string $invoiceName
     * @return Trade
     */
    public function setInvoiceName($invoiceName)
    {
        $this->invoiceName = $invoiceName;

        return $this;
    }

    /**
     * Get invoiceName
     *
     * @return string 
     */
    public function getInvoiceName()
    {
        return $this->invoiceName;
    }

    /**
     * Set invoiceNo
     *
     * @param integer $invoiceNo
     * @return Trade
     */
    public function setInvoiceNo($invoiceNo)
    {
        $this->invoiceNo = $invoiceNo;

        return $this;
    }

    /**
     * Get invoiceNo
     *
     * @return integer 
     */
    public function getInvoiceNo()
    {
        return $this->invoiceNo;
    }

    /**
     * Set canRate
     *
     * @param boolean $canRate
     * @return Trade
     */
    public function setCanRate($canRate)
    {
        $this->canRate = $canRate;

        return $this;
    }

    /**
     * Get canRate
     *
     * @return boolean 
     */
    public function getCanRate()
    {
        return $this->canRate;
    }

    /**
     * Set rated
     *
     * @param boolean $rated
     * @return Trade
     */
    public function setRated($rated)
    {
        $this->rated = $rated;

        return $this;
    }

    /**
     * Get rated
     *
     * @return boolean 
     */
    public function getRated()
    {
        return $this->rated;
    }

    /**
     * Set refunded
     *
     * @param boolean $refunded
     * @return Trade
     */
    public function setRefunded($refunded)
    {
        $this->refunded = $refunded;

        return $this;
    }

    /**
     * Get refunded
     *
     * @return boolean 
     */
    public function getRefunded()
    {
        return $this->refunded;
    }

    /**
     * Set tradeFrom
     *
     * @param string $tradeFrom
     * @return Trade
     */
    public function setTradeFrom($tradeFrom)
    {
        $this->tradeFrom = $tradeFrom;

        return $this;
    }

    /**
     * Get tradeFrom
     *
     * @return string 
     */
    public function getTradeFrom()
    {
        return $this->tradeFrom;
    }

    /**
     * Set shippingId
     *
     * @param integer $shippingId
     * @return Trade
     */
    public function setShippingId($shippingId)
    {
        $this->shippingId = $shippingId;

        return $this;
    }

    /**
     * Get shippingId
     *
     * @return integer 
     */
    public function getShippingId()
    {
        return $this->shippingId;
    }

    /**
     * Set shippingName
     *
     * @param string $shippingName
     * @return Trade
     */
    public function setShippingName($shippingName)
    {
        $this->shippingName = $shippingName;

        return $this;
    }

    /**
     * Get shippingName
     *
     * @return string 
     */
    public function getShippingName()
    {
        return $this->shippingName;
    }

    /**
     * Set serviceAt
     *
     * @param \DateTime $serviceAt
     * @return Trade
     */
    public function setServiceAt($serviceAt)
    {
        $this->serviceAt = $serviceAt;

        return $this;
    }

    /**
     * Get serviceAt
     *
     * @return \DateTime 
     */
    public function getServiceAt()
    {
        return $this->serviceAt;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Trade
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Trade
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Add orders
     *
     * @param \TradeBundle\Entity\Order $orders
     * @return Trade
     */
    public function addOrder(\TradeBundle\Entity\Order $orders)
    {
        $this->orders[] = $orders;

        return $this;
    }

    /**
     * Remove orders
     *
     * @param \TradeBundle\Entity\Order $orders
     */
    public function removeOrder(\TradeBundle\Entity\Order $orders)
    {
        $this->orders->removeElement($orders);
    }

    /**
     * Get orders
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Add refunds
     *
     * @param \TradeBundle\Entity\Refund $refunds
     * @return Trade
     */
    public function addRefund(\TradeBundle\Entity\Refund $refunds)
    {
        $this->refunds[] = $refunds;

        return $this;
    }

    /**
     * Remove refunds
     *
     * @param \TradeBundle\Entity\Refund $refunds
     */
    public function removeRefund(\TradeBundle\Entity\Refund $refunds)
    {
        $this->refunds->removeElement($refunds);
    }

    /**
     * Get refunds
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRefunds()
    {
        return $this->refunds;
    }

    /**
     * Set area
     *
     * @param \CommonBundle\Entity\Area $area
     * @return Trade
     */
    public function setArea(\CommonBundle\Entity\Area $area = null)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area
     *
     * @return \CommonBundle\Entity\Area 
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set shipping
     *
     * @param \TradeBundle\Entity\Shipping $shipping
     * @return Trade
     */
    public function setShipping(\TradeBundle\Entity\Shipping $shipping = null)
    {
        $this->shipping = $shipping;

        return $this;
    }

    /**
     * Get shipping
     *
     * @return \TradeBundle\Entity\Shipping
     */
    public function getShipping()
    {
        return $this->shipping;
    }

    /**
     * Set payment
     *
     * @param \TradeBundle\Entity\Payment $payment
     * @return Trade
     */
    public function setPayment(\TradeBundle\Entity\Payment $payment = null)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * Get payment
     *
     * @return \TradeBundle\Entity\Payment 
     */
    public function getPayment()
    {
        return $this->payment;
    }
}
