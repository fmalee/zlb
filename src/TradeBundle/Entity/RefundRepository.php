<?php

namespace TradeBundle\Entity;

use CoreBundle\Doctrine\EntityRepository;

/**
 * Refund Repository
 */
class RefundRepository extends EntityRepository
{
    /**
     * {@inheritdoc}
     */
    public function findBrandBy(array $criteria)
    {
        return $this->findOneBy($criteria);
    }

}
