<?php

namespace TradeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Shipping Entity
 *
 * @ORM\Entity(repositoryClass="TradeBundle\Entity\ShippingRepository")
 * @ORM\Table(name="mc_shipping", indexes={@ORM\Index(name="name", columns={"name", "id"})})
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(
 *     fields={"name"},
 *     message="该标签名已存在",
 *     groups={"Edit"}
 * )
 */
class Shipping
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=30, unique=true)
     *
     * @Assert\Length(
     *      max = "30",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     * @Assert\Regex(
     *      pattern="/^[^-_]+[a-z0-9-_]+[^-_]$/",
     *      message="标签名只能包含字母、数字、_或减号，不能以_或减号开头或结尾。",
     *      groups={"Edit"}
     * )
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=50)
     *
     * @Assert\NotBlank(message = "标题不能为空", groups={"Edit"})
     * @Assert\Length(
     *      max = "50",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="homepage", type="string", length=128, nullable=true)
     *
     * @Assert\Length(
     *      max = "128",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     */
    private $homePage;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     *
     * @Assert\Length(
     *      max = "255",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     */
    private $description;

    /**
     * @var bool
     *
     * @ORM\Column(name="enabled", type="boolean")
     */
    protected $enabled;

    /**
     * @var string
     *
     * @ORM\Column(name="insure", type="string", length=30)
     *
     * @Assert\Length(
     *      max = "30",
     *      maxMessage = "保价长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     */
    private $insure;

    /**
     * @var bool
     *
     * @ORM\Column(name="support_cod", type="boolean")
     */
    protected $supportCod;

    /**
     * @var integer
     *
     * @ORM\Column(name="sort_order", type="integer")
     */
    private $sortOrder;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="modified", type="datetime")
     */
    private $modified;

    /**
     * @ORM\OneToMany(targetEntity="TradeBundle\Entity\Trade", mappedBy="trade")
     */
    private $trades;

    /**
     * @ORM\OneToMany(targetEntity="TradeBundle\Entity\TradeDelivery", mappedBy="shipping")
     */
    private $tradeDeliveries;

    public function __construct()
    {
        $this->enabled     = true;
        $this->supportCod     = false;
        $this->insure      = 0;
        $this->sortOrder      = 10;
        $this->created     = new \Datetime();
        $this->modified    = new \Datetime();
        $this->trades      = new ArrayCollection();
        $this->tradeDeliveries     = new ArrayCollection();
    }

    /**
     * @ORM\PreUpdate
     */
    public function setPerUpdate()
    {
        $this->modified = new \DateTime();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Shipping
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Shipping
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set homePage
     *
     * @param string $homePage
     * @return Shipping
     */
    public function setHomePage($homePage)
    {
        $this->homePage = $homePage;

        return $this;
    }

    /**
     * Get homePage
     *
     * @return string 
     */
    public function getHomePage()
    {
        return $this->homePage;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Shipping
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return Shipping
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set insure
     *
     * @param string $insure
     * @return Shipping
     */
    public function setInsure($insure)
    {
        $this->insure = $insure;

        return $this;
    }

    /**
     * Get insure
     *
     * @return string 
     */
    public function getInsure()
    {
        return $this->insure;
    }

    /**
     * Set supportCod
     *
     * @param boolean $supportCod
     * @return Shipping
     */
    public function setSupportCod($supportCod)
    {
        $this->supportCod = $supportCod;

        return $this;
    }

    /**
     * Get supportCod
     *
     * @return boolean 
     */
    public function getSupportCod()
    {
        return $this->supportCod;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return Shipping
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Shipping
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Shipping
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Add trades
     *
     * @param \TradeBundle\Entity\Trade $trades
     * @return Shipping
     */
    public function addTrade(\TradeBundle\Entity\Trade $trades)
    {
        $this->trades[] = $trades;

        return $this;
    }

    /**
     * Remove trades
     *
     * @param \TradeBundle\Entity\Trade $trades
     */
    public function removeTrade(\TradeBundle\Entity\Trade $trades)
    {
        $this->trades->removeElement($trades);
    }

    /**
     * Get trades
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTrades()
    {
        return $this->trades;
    }

    /**
     * Add tradeDeliveries
     *
     * @param \TradeBundle\Entity\TradeDelivery $tradeDeliveries
     * @return Shipping
     */
    public function addTradeDelivery(\TradeBundle\Entity\TradeDelivery $tradeDeliveries)
    {
        $this->tradeDeliveries[] = $tradeDeliveries;

        return $this;
    }

    /**
     * Remove tradeDeliveries
     *
     * @param \TradeBundle\Entity\TradeDelivery $tradeDeliveries
     */
    public function removeTradeDelivery(\TradeBundle\Entity\TradeDelivery $tradeDeliveries)
    {
        $this->tradeDeliveries->removeElement($tradeDeliveries);
    }

    /**
     * Get tradeDeliveries
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTradeDeliveries()
    {
        return $this->tradeDeliveries;
    }

}
