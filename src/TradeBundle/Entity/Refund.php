<?php

namespace TradeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Refund Entity
 *
 * @ORM\Entity(repositoryClass="TradeBundle\Entity\RefundRepository")
 * @ORM\Table(name="mc_refund", indexes={@ORM\Index(name="status", columns={"status", "id"})})
 * @ORM\HasLifecycleCallbacks()
 */
class Refund
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="trade_id", type="integer")
     */
    private $tradeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_id", type="integer")
     */
    private $orderId;

    /**
     * @var integer
     *
     * @ORM\Column(name="product_id", type="integer")
     */
    private $productId;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", length=50)
     */
    private $userId;

    /**
     * @var smallint
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;

    /**
     * @var smallint
     *
     * @ORM\Column(name="good_status", type="smallint")
     */
    private $goodStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="alipay_no", type="string", length=50)
     */
    private $alipayNo;

    /**
     * @var float
     *
     * @ORM\Column(name="total_fee", type="float", precision=10, scale=0)
     */
    private $totalFee;

    /**
     * @var float
     *
     * @ORM\Column(name="payment", type="float", precision=10, scale=0)
     */
    private $payment;

    /**
     * @var float
     *
     * @ORM\Column(name="refund_fee", type="float", precision=10, scale=0)
     */
    private $refundFee;

    /**
     * @var integer
     *
     * @ORM\Column(name="num", type="integer")
     */
    private $num;

    /**
     * @var string
     *
     * @ORM\Column(name="reason", type="string", length=50)
     */
    private $reason;

    /**
     * @var string
     * Reference desc
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="company_name", type="string", length=30)
     */
    private $companyName;

    /**
     * @var string
     *
     * @ORM\Column(name="sid", type="string", length=30)
     */
    private $sid;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=150)
     */
    private $address;

    /**
     * @var boolean
     *
     * @ORM\Column(name="returned", type="boolean")
     */
    private $returned;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="return_at", type="datetime", nullable=true)
     */
    private $returnAt;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="modified", type="datetime")
     */
    private $modified;

    /**
     * @var User[]
     * @ORM\ManyToOne(targetEntity="AccountBundle\Entity\User", inversedBy="refunds")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @var Trade[]
     * @ORM\ManyToOne(targetEntity="TradeBundle\Entity\Trade", inversedBy="refunds")
     * @ORM\JoinColumn(name="trade_id", referencedColumnName="id")
     */
    private $trade;

    /**
     * @var Order[]
     * @ORM\ManyToOne(targetEntity="TradeBundle\Entity\Order", inversedBy="refunds")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     */
    private $order;

    /**
     * @var Order[]
     * @ORM\ManyToOne(targetEntity="ProductBundle\Entity\Product", inversedBy="refunds")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    private $product;

    public function __construct()
    {
        $this->status      = 0;
        $this->good_status = 0;
        $this->returned    = false;
        $this->created     = new \Datetime();
        $this->modified    = new \Datetime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function setPerUpdate()
    {
        $this->modified = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return Refund
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set tradeId
     *
     * @param integer $tradeId
     * @return Refund
     */
    public function setTradeId($tradeId)
    {
        $this->tradeId = $tradeId;

        return $this;
    }

    /**
     * Get tradeId
     *
     * @return integer 
     */
    public function getTradeId()
    {
        return $this->tradeId;
    }

    /**
     * Set orderId
     *
     * @param integer $orderId
     * @return Refund
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;

        return $this;
    }

    /**
     * Get orderId
     *
     * @return integer 
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * Set productId
     *
     * @param integer $productId
     * @return Refund
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * Get productId
     *
     * @return integer 
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * Set userId
     *
     * @param string $userId
     * @return Refund
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return string 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Refund
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set goodStatus
     *
     * @param integer $goodStatus
     * @return Refund
     */
    public function setGoodStatus($goodStatus)
    {
        $this->goodStatus = $goodStatus;

        return $this;
    }

    /**
     * Get goodStatus
     *
     * @return integer 
     */
    public function getGoodStatus()
    {
        return $this->goodStatus;
    }

    /**
     * Set alipayNo
     *
     * @param string $alipayNo
     * @return Refund
     */
    public function setAlipayNo($alipayNo)
    {
        $this->alipayNo = $alipayNo;

        return $this;
    }

    /**
     * Get alipayNo
     *
     * @return string 
     */
    public function getAlipayNo()
    {
        return $this->alipayNo;
    }

    /**
     * Set totalFee
     *
     * @param float $totalFee
     * @return Refund
     */
    public function setTotalFee($totalFee)
    {
        $this->totalFee = $totalFee;

        return $this;
    }

    /**
     * Get totalFee
     *
     * @return float 
     */
    public function getTotalFee()
    {
        return $this->totalFee;
    }

    /**
     * Set payment
     *
     * @param float $payment
     * @return Refund
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * Get payment
     *
     * @return float 
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * Set refundFee
     *
     * @param float $refundFee
     * @return Refund
     */
    public function setRefundFee($refundFee)
    {
        $this->refundFee = $refundFee;

        return $this;
    }

    /**
     * Get refundFee
     *
     * @return float 
     */
    public function getRefundFee()
    {
        return $this->refundFee;
    }

    /**
     * Set num
     *
     * @param integer $num
     * @return Refund
     */
    public function setNum($num)
    {
        $this->num = $num;

        return $this;
    }

    /**
     * Get num
     *
     * @return integer 
     */
    public function getNum()
    {
        return $this->num;
    }

    /**
     * Set reason
     *
     * @param string $reason
     * @return Refund
     */
    public function setReason($reason)
    {
        $this->reason = $reason;

        return $this;
    }

    /**
     * Get reason
     *
     * @return string 
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Refund
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set companyName
     *
     * @param string $companyName
     * @return Refund
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get companyName
     *
     * @return string 
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set sid
     *
     * @param string $sid
     * @return Refund
     */
    public function setSid($sid)
    {
        $this->sid = $sid;

        return $this;
    }

    /**
     * Get sid
     *
     * @return string 
     */
    public function getSid()
    {
        return $this->sid;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Refund
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set returned
     *
     * @param boolean $returned
     * @return Refund
     */
    public function setReturned($returned)
    {
        $this->returned = $returned;

        return $this;
    }

    /**
     * Get returned
     *
     * @return boolean 
     */
    public function getReturned()
    {
        return $this->returned;
    }

    /**
     * Set returnAt
     *
     * @param \DateTime $returnAt
     * @return Refund
     */
    public function setReturnAt($returnAt)
    {
        $this->returnAt = $returnAt;

        return $this;
    }

    /**
     * Get returnAt
     *
     * @return \DateTime 
     */
    public function getReturnAt()
    {
        return $this->returnAt;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Refund
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Refund
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set user
     *
     * @param \AccountBundle\Entity\User $user
     * @return Refund
     */
    public function setUser(\AccountBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AccountBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set trade
     *
     * @param \TradeBundle\Entity\Trade $trade
     * @return Refund
     */
    public function setTrade(\TradeBundle\Entity\Trade $trade = null)
    {
        $this->trade = $trade;

        return $this;
    }

    /**
     * Get trade
     *
     * @return \TradeBundle\Entity\Trade 
     */
    public function getTrade()
    {
        return $this->trade;
    }

    /**
     * Set order
     *
     * @param \TradeBundle\Entity\Order $order
     * @return Refund
     */
    public function setOrder(\TradeBundle\Entity\Order $order = null)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return \TradeBundle\Entity\Order 
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set product
     *
     * @param \ProductBundle\Entity\Product $product
     * @return Refund
     */
    public function setProduct(\ProductBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \ProductBundle\Entity\Product 
     */
    public function getProduct()
    {
        return $this->product;
    }
}
