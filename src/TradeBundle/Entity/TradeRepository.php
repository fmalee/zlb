<?php

namespace TradeBundle\Entity;

use CoreBundle\Doctrine\EntityRepository;

/**
 * Trade Repository
 */
class TradeRepository extends EntityRepository
{
    /**
     * {@inheritdoc}
     */
    public function findTradeBy(array $criteria)
    {
        return $this->findOneBy($criteria);
    }

}
