<?php

namespace TradeBundle\Entity;

use CoreBundle\Doctrine\EntityRepository;

/**
 * Shipping Repository
 */
class ShippingRepository extends EntityRepository
{

    /**
     * {@inheritdoc}
     */
    public function findShippingBy(array $criteria)
    {
        return $this->findOneBy($criteria);
    }

    /**
     * {@inheritdoc}
     */
    public function findEnableShipping($limit = 0)
    {
        $qb = $this->createQueryBuilder('s')
            ->andWhere('s.enabled = true')
            ->addOrderBy('s.sortOrder', 'ASC');
        if ($limit) {
            $qb = $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function findAll($limit = 0)
    {
        $qb = $this->createQueryBuilder('s')
            ->addOrderBy('s.sortOrder', 'ASC');
        if ($limit) {
            $qb = $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->getResult();
    }


}
