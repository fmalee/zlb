<?php

namespace TradeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Order Entity
 *
 * @ORM\Entity(repositoryClass="TradeBundle\Entity\OrderRepository")
 * @ORM\Table(name="mc_order", indexes={@ORM\Index(name="id", columns={"id", "trade_id", "id", "sku_id"})})
 * @ORM\HasLifecycleCallbacks()
 */
class Order
{
     /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="trade_id", type="integer")
     */
    private $tradeId;

    /**
     * @var integerd
     *
     * @ORM\Column(name="product_id", type="integer")
     */
    private $productId;

    /**
     * @var integer
     *
     * @ORM\Column(name="sku_id", type="integer")
     */
    private $skuId;

    /**
     * @var integer
     *
     * @ORM\Column(name="refund_id", type="integer", nullable=true)
     */
    private $refundId;

    /**
     * @var smallint
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="icon", type="string", length=255)
     */
    private $icon;

    /**
     * @var integer
     *
     * @ORM\Column(name="num", type="integer")
     */
    private $num;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", precision=10, scale=0)
     */
    private $price;

    /**
     * @var float
     *
     * @ORM\Column(name="payment", type="float", precision=10, scale=0)
     */
    private $payment;

    /**
     * @var float
     *
     * @ORM\Column(name="adjust_fee", type="float", precision=10, scale=0)
     */
    private $adjustFee;

    /**
     * @var float
     *
     * @ORM\Column(name="discount_fee", type="float", precision=10, scale=0)
     */
    private $discountFee;

    /**
     * @var float
     *
     * @ORM\Column(name="total_fee", type="float", precision=10, scale=0)
     */
    private $totalFee;

    /**
     * @var string
     *
     * @ORM\Column(name="property", type="string", length=255)
     */
    private $property;

    /**
     * @var string
     *
     * @ORM\Column(name="logistics_company", type="string", length=30)
     */
    private $logisticsCompany;

    /**
     * @var string
     *
     * @ORM\Column(name="invoice_no", type="string", length=30)
     */
    private $invoiceNo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="rated", type="boolean")
     */
    private $rated;

    /**
     * @var string
     *
     * @ORM\Column(name="timeout_action_time", type="string", length=30)
     */
    private $timeoutActionTime;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="consign_at", type="datetime")
     */
    private $consignAt;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="end_at", type="datetime")
     */
    private $endAt;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="modified", type="datetime")
     */
    private $modified;

    /**
     * @var Trade[]
     * @ORM\ManyToOne(targetEntity="TradeBundle\Entity\Trade", inversedBy="orders")
     * @ORM\JoinColumn(name="trade_id", referencedColumnName="id")
     */
    private $trade;

    /**
     * @var Product[]
     * @ORM\ManyToOne(targetEntity="ProductBundle\Entity\Product", inversedBy="orders")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    private $product;

    /**
     * @var Sku[]
     * @ORM\ManyToOne(targetEntity="ProductBundle\Entity\Sku", inversedBy="orders")
     * @ORM\JoinColumn(name="sku_id", referencedColumnName="id")
     */
    private $sku;

    /**
     * @ORM\OneToMany(targetEntity="TradeBundle\Entity\Refund", mappedBy="order")
     */
    private $refunds;

    public function __construct()
    {
        $this->status      = 0;
        $this->good_status = 0;
        $this->returned    = false;
        $this->created     = new \Datetime();
        $this->modified    = new \Datetime();
        $this->refunds     = new ArrayCollection();
    }

    /**
     * @ORM\PreUpdate
     */
    public function setPerUpdate()
    {
        $this->modified = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return Order
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set tradeId
     *
     * @param integer $tradeId
     * @return Order
     */
    public function setTradeId($tradeId)
    {
        $this->tradeId = $tradeId;

        return $this;
    }

    /**
     * Get tradeId
     *
     * @return integer 
     */
    public function getTradeId()
    {
        return $this->tradeId;
    }

    /**
     * Set productId
     *
     * @param integer $productId
     * @return Order
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * Get productId
     *
     * @return integer 
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * Set skuId
     *
     * @param integer $skuId
     * @return Order
     */
    public function setSkuId($skuId)
    {
        $this->skuId = $skuId;

        return $this;
    }

    /**
     * Get skuId
     *
     * @return integer 
     */
    public function getSkuId()
    {
        return $this->skuId;
    }

    /**
     * Set refundId
     *
     * @param integer $refundId
     * @return Order
     */
    public function setRefundId($refundId)
    {
        $this->refundId = $refundId;

        return $this;
    }

    /**
     * Get refundId
     *
     * @return integer 
     */
    public function getRefundId()
    {
        return $this->refundId;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Order
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Order
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set icon
     *
     * @param string $icon
     * @return Order
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string 
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set num
     *
     * @param integer $num
     * @return Order
     */
    public function setNum($num)
    {
        $this->num = $num;

        return $this;
    }

    /**
     * Get num
     *
     * @return integer 
     */
    public function getNum()
    {
        return $this->num;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Order
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set payment
     *
     * @param float $payment
     * @return Order
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * Get payment
     *
     * @return float 
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * Set adjustFee
     *
     * @param float $adjustFee
     * @return Order
     */
    public function setAdjustFee($adjustFee)
    {
        $this->adjustFee = $adjustFee;

        return $this;
    }

    /**
     * Get adjustFee
     *
     * @return float 
     */
    public function getAdjustFee()
    {
        return $this->adjustFee;
    }

    /**
     * Set discountFee
     *
     * @param float $discountFee
     * @return Order
     */
    public function setDiscountFee($discountFee)
    {
        $this->discountFee = $discountFee;

        return $this;
    }

    /**
     * Get discountFee
     *
     * @return float 
     */
    public function getDiscountFee()
    {
        return $this->discountFee;
    }

    /**
     * Set totalFee
     *
     * @param float $totalFee
     * @return Order
     */
    public function setTotalFee($totalFee)
    {
        $this->totalFee = $totalFee;

        return $this;
    }

    /**
     * Get totalFee
     *
     * @return float 
     */
    public function getTotalFee()
    {
        return $this->totalFee;
    }

    /**
     * Set property
     *
     * @param string $property
     * @return Order
     */
    public function setProperty($property)
    {
        $this->property = $property;

        return $this;
    }

    /**
     * Get property
     *
     * @return string 
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * Set logisticsCompany
     *
     * @param string $logisticsCompany
     * @return Order
     */
    public function setLogisticsCompany($logisticsCompany)
    {
        $this->logisticsCompany = $logisticsCompany;

        return $this;
    }

    /**
     * Get logisticsCompany
     *
     * @return string 
     */
    public function getLogisticsCompany()
    {
        return $this->logisticsCompany;
    }

    /**
     * Set invoiceNo
     *
     * @param string $invoiceNo
     * @return Order
     */
    public function setInvoiceNo($invoiceNo)
    {
        $this->invoiceNo = $invoiceNo;

        return $this;
    }

    /**
     * Get invoiceNo
     *
     * @return string 
     */
    public function getInvoiceNo()
    {
        return $this->invoiceNo;
    }

    /**
     * Set rated
     *
     * @param boolean $rated
     * @return Order
     */
    public function setRated($rated)
    {
        $this->rated = $rated;

        return $this;
    }

    /**
     * Get rated
     *
     * @return boolean 
     */
    public function getRated()
    {
        return $this->rated;
    }

    /**
     * Set timeoutActionTime
     *
     * @param string $timeoutActionTime
     * @return Order
     */
    public function setTimeoutActionTime($timeoutActionTime)
    {
        $this->timeoutActionTime = $timeoutActionTime;

        return $this;
    }

    /**
     * Get timeoutActionTime
     *
     * @return string 
     */
    public function getTimeoutActionTime()
    {
        return $this->timeoutActionTime;
    }

    /**
     * Set consignAt
     *
     * @param \DateTime $consignAt
     * @return Order
     */
    public function setConsignAt($consignAt)
    {
        $this->consignAt = $consignAt;

        return $this;
    }

    /**
     * Get consignAt
     *
     * @return \DateTime 
     */
    public function getConsignAt()
    {
        return $this->consignAt;
    }

    /**
     * Set endAt
     *
     * @param \DateTime $endAt
     * @return Order
     */
    public function setEndAt($endAt)
    {
        $this->endAt = $endAt;

        return $this;
    }

    /**
     * Get endAt
     *
     * @return \DateTime 
     */
    public function getEndAt()
    {
        return $this->endAt;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Order
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Order
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set trade
     *
     * @param \TradeBundle\Entity\Trade $trade
     * @return Order
     */
    public function setTrade(\TradeBundle\Entity\Trade $trade = null)
    {
        $this->trade = $trade;

        return $this;
    }

    /**
     * Get trade
     *
     * @return \TradeBundle\Entity\Trade 
     */
    public function getTrade()
    {
        return $this->trade;
    }

    /**
     * Set product
     *
     * @param \ProductBundle\Entity\Product $product
     * @return Order
     */
    public function setProduct(\ProductBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \ProductBundle\Entity\Product 
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set sku
     *
     * @param \ProductBundle\Entity\Sku $sku
     * @return Order
     */
    public function setSku(\ProductBundle\Entity\Sku $sku = null)
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * Get sku
     *
     * @return \ProductBundle\Entity\Sku 
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * Add refunds
     *
     * @param \TradeBundle\Entity\Refund $refunds
     * @return Order
     */
    public function addRefund(\TradeBundle\Entity\Refund $refunds)
    {
        $this->refunds[] = $refunds;

        return $this;
    }

    /**
     * Remove refunds
     *
     * @param \TradeBundle\Entity\Refund $refunds
     */
    public function removeRefund(\TradeBundle\Entity\Refund $refunds)
    {
        $this->refunds->removeElement($refunds);
    }

    /**
     * Get refunds
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRefunds()
    {
        return $this->refunds;
    }
}
