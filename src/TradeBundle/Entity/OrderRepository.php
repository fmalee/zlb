<?php

namespace TradeBundle\Entity;

use CoreBundle\Doctrine\EntityRepository;

/**
 * Order Repository
 */
class OrderRepository extends EntityRepository
{
    /**
     * {@inheritdoc}
     */
    public function findOrderBy(array $criteria)
    {
        return $this->findOneBy($criteria);
    }

}
