<?php

namespace TradeBundle\Event;

use TradeBundle\Entity\Cart;
use TradeBundle\Entity\CartRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;

/**
 * 将游客的购物车转换成用户
 */
class UpdateCartListener implements EventSubscriberInterface
{
    protected $cartRepository;
    protected $request;
    protected $requestStack;

    public function __construct(CartRepository $cartRepository, RequestStack $requestStack)
    {
        $this->cartRepository  = $cartRepository;
        $this->requestStack = $requestStack;
        $this->request = $this->requestStack->getCurrentRequest();
    }

    public static function getSubscribedEvents()
    {
        return array(
            SecurityEvents::INTERACTIVE_LOGIN   => 'onSecurityInteractiveCart',
        );
    }

    public function onSecurityInteractiveCart(InteractiveLoginEvent $event)
    {
        $user = $event->getAuthenticationToken()->getUser();
        $userId = $user->getId();

        $sessionId = $this->request->cookies->get('PHPSESSID');

        $this->cartRepository->updateUserBySession($sessionId, $userId);
    }
}