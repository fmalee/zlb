<?php
namespace SkillBundle\Controller;

use CoreBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @author Wenming Tang <tang@babyfamily.com>
 */
class SkillController extends Controller
{
    /**
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function indexAction(Request $request)
    {
        if (null == $user = $this->getUser()) {
            throw new AccessDeniedException();
        }

        $this->get('core.breadcrumb')->add('技能信息');

        /** @var SkillBundle\EntityManager\SkillManager $skillManager */
        $skillManager = $this->get('skill.entity.skill_repository');

        $skills       = $skillManager->findSkillsByUser($user);

        // new skill
        $skill = $skillManager->createSkill($user);
        //$skill = new \SkillBundle\Entity\Skill;

        /** @var \Symfony\Component\Form\FormInterface $form */
       /* $form = $this->get('skill.form.skill_form_type');
        $form->setData($skill);

        $form->handleRequest($request);*/
        $form = $this->createBoundObjectForm($skill);
        //var_dump($form->createView());
        //exit();

        if ($form->isBound() && $form->isValid()) {
            $this->persist($skill, true);
            //$skillManager->updateSkill($skill);

            //$this->get('session')->getFlashBag()->add('success', '技能信息已保存');
            $this->addFlash('success', '技能信息已保存');

            //return $this->redirect($this->generateUrl('skill_list'));
            return $this->redirectToRoute('skill_list');
        }

        return $this->render('SkillBundle:Skill:index.html.twig', array(
            'skills' => $skills,
            'form'   => $form->createView()
        ));
    }

    public function editAction(Request $request, $skillId)
    {
        if (null == $user = $this->getUser()) {
            throw new AccessDeniedException();
        }

        $this->get('breadcrumb')->add('技能信息', $this->generateUrl('skill_list'))->add('编辑技能信息');

        /** @var \Zlb\SkillBundle\EntityManager\SkillManager $skillManager */
        //$skillManager = $this->get('skill.manager');
        $skillManager = $this->get('skill.entity.skill_repository');
        $skill        = $skillManager->findSkillById($skillId);

        if (null == $skill || !$user->equals($skill->getUser())) {
            throw $this->createNotFoundException();
        }

        /** @var \Symfony\Component\Form\FormInterface $form */
        $form = $this->get('skill.form');
        $form->setData($skill);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $skillManager->updateSkill($skill);

            $this->get('session')->getFlashBag()->add('success', '技能信息已更新');

            return $this->redirect($this->generateUrl('skill_list'));
        }

        return $this->render('ZlbSkillBundle:Skill:edit.html.twig', array(
            'form'  => $form->createView(),
            'skill' => $skill
        ));
    }

    public function deleteAction($skillId)
    {
        if (null == $user = $this->getUser()) {
            throw new AccessDeniedException();
        }

        $skill = $this->findOr404('SkillBundle:Skill', array('id' => $skillId));
        $this->remove($skill, true);

        $this->addFlash('success', '技能信息已删除');

        return $this->redirectToRoute('skill_list');
        /** @var \Zlb\SkillBundle\EntityManager\SkillManager $skillManager */
        $skillManager = $this->get('skill.manager');
        $skill        = $skillManager->findSkillById($skillId);

        if (null == $skill || !$user->equals($skill->getUser())) {
            throw $this->createNotFoundException();
        }

        $skillManager->deleteSkill($skill);

        $this->get('session')->getFlashBag()->add('success', '技能信息已删除');

        return $this->redirect($this->generateUrl('skill_list'));
    }
}