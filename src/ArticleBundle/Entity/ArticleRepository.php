<?php

namespace ArticleBundle\Entity;

use CoreBundle\Doctrine\EntityRepository;

/**
 * ArticleRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ArticleRepository extends EntityRepository
{
    /**
     * {@inheritdoc}
     */
    public function findArticlePagerByLatest($page = 1)
    {
        $qb = $this->createQueryBuilder('a')
            ->select('a, u, c')
            ->join('a.user', 'u')
            ->join('a.category', 'c')
            ->andWhere('a.status = true')
            ->addOrderBy('a.created', 'DESC');

        return $qb;
    }

    /**
     * {@inheritdoc}
     */
    public function findArticleByCategoryId($categoryId)
    {
        $qb = $this->createQueryBuilder('a')
            ->select('a, u')
            ->join('a.user', 'u')
            ->andWhere('a.status = true')
            ->andWhere('a.categoryId = :categoryId')
            ->addOrderBy('a.created', 'DESC')
            ->setParameter('categoryId', $categoryId);

        return $qb;
    }

    /**
     * {@inheritdoc}
     */
    public function findArticleByCategory($categoryIds)
    {
        if (!is_array($categoryIds)) {
            $categoryIds = explode(',', $categoryIds);
        }

        $qb = $this->createQueryBuilder('a');
        $qb->select('a, u')
            ->join('a.user', 'u')
            ->andWhere('a.status = true')
            ->andWhere($qb->expr()->in('a.categoryId', ':categoryId'))
            ->addOrderBy('a.created', 'DESC')
            ->setParameter('categoryId', $categoryIds);

        return $qb;
    }
}
