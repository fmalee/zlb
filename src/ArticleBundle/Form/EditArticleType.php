<?php

namespace ArticleBundle\Form;

use ArticleBundle\Entity\Article;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EditArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('realName', 'text', array('label' => '真实姓名', 'required' => false))
            ->add('gender', 'choice', array(
                'label'       => '性别',
                'choices'     => Profile::getGenders(),
                'expanded'    => true,
                'empty_value' => false,
                'attr'        => array(
                    'class' => 'embed'
                )
            ))
            ->add('birthday', 'birthday', array(
                'label'    => '生日',
                'format'   => 'yyyy-MM-dd',
                'widget'   => 'single_text',
                'required' => false,
                'attr'     => array(
                    'class'       => 'embed',
                    'placeholder' => '格式: 2013-06-02'
                )
            ))
            ->add('positionTitle', 'text', array(
                'label'    => '当前职位或头衔',
                'required' => false
            ))
            ->add('companyName', 'text', array(
                'label'    => '公司或机构名称',
                'required' => false
            ))
            ->add('phoneNumber', 'text', array(
                'label'    => '手机号码',
                'required' => false
            ))
            ->add('homepage', 'url', array(
                'label'    => '个人主页',
                'required' => false
            ))
            ->add('qq', 'text', array(
                'label'    => 'QQ',
                'required' => false
            ))
            ->add('bio', 'textarea', array(
                'label'    => '个人介绍',
                'required' => false,
                'attr'     => array(
                    'rows'  => 8,
                    'cols'  => 55,
                    'class' => 'form-control'
                )
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'        => 'ArticleBundle\Entity\Article',
            'validation_groups' => array('Edit')
        ));
    }

    public function getName()
    {
        return 'article_edit';
    }
}