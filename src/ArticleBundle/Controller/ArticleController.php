<?php

namespace ArticleBundle\Controller;

use CoreBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * @Route("/article")
 */
class ArticleController extends Controller
{
    /**
     * @Route("/", name="article_home")
     * @Template()
     */
    public function indexAction()
    {
        $categorys = $this->getCategoryRepository()->getCategories(1);
        $this->dump($categorys);

        $categorys = $this->getCategoryRepository()->findByModule(1);
        //$this->dump($categorys);

        return array();
    }

    /**
     * @Route("/{cate}/{page}", name="article_list", requirements={"cate" = "\w+", "page" = "\d+"}, defaults={"page" = 1})
     * @Template()
     */
    public function listAction($cate, $page)
    {
        $category = $this->getCategoryRepository()->findCategoryByName($cate);
        if (null == $category || $category->getModule() != 1) {
            throw $this->createNotFoundException('没有找到对应的栏目： ' . $cate);
        }

        $categories = $this->getCategories();
        if ($category->getParented()) {
            $ids = trim($categories[$category->getId()]['arrChild'], ',');
            $qb = $this->getArticleRepository()->findArticleByCategory($ids);
        } else {
            $qb = $this->getArticleRepository()->findArticleByCategoryId($category->getId());
        }

        $pagination = $this->paginate($qb, $page, 3);
        //$this->dump($pagination);

        $this->setCategoryBreadcrumb($category->getId(), 'article_list')->add('列表');

        $template = $category->getTemplateLists() ?: 'ArticleBundle:Article:list.html.twig';

        return $this->render($template, array(
            'pagination' => $pagination,
            'category' => $category,
            'categories' => $categories
        ));
    }

    public function listAction1($cate, $page)
    {
        $category = $this->getCategoryRepository()->findCategoryByName($cate);
        if (null == $category) {
            throw $this->createNotFoundException('没有找到对应的栏目： ' . $cate);
        }
        if ($category->isParented()) {
            $ids = $this->getCategoryRepository()->findCategoryIdByParent($category->getId());
            $qb = $this->getArticleRepository()->findArticleByCategory($ids);
        } else {
            $qb = $this->getArticleRepository()->findArticleByCategoryId($category->getId());
        }

        $pagination = $this->paginate($qb, $page, 3);
        //$this->dump($pagination);

        $this->get('core.breadcrumb')
            ->add($category->getTitle(), $this->generateUrl('article_list', array('cate' => $category->getName())))
            ->add('列表');

        $template = $category->getTemplateLists() ?: 'ArticleBundle:Article:list.html.twig';

        return $this->render($template, array(
            'pagination' => $pagination,
            'category' => $category
        ));
    }

    /**
     * @Route("/{cate}/detail/{id}.html", name="article_detail", requirements={"cate" = "\w+", "id" = "\d+"})
     * @Route("/{cate}/detail/{name}.html", name="article_name_detail", requirements={"cate" = "\w+", "name" = "\w+"})
     * @Template()
     */
    public function detailAction($cate, $id = null, $name = null)
    {
        $category = $this->getCategoryRepository()->findOneByName($cate);
        if (null == $category || !$category->isEnabled()) {
            throw $this->createNotFoundException('没有找到对应的栏目： ' . $cate);
        }

        if ($id) {
            $article = $this->getArticleRepository()->find($id);
        } elseif ($name) {
            $article = $this->getArticleRepository()->findOneByName($name);
        }
        if (!$article || $article->getCategoryId() != $category->getId()) {
            throw $this->createNotFoundException('没有找到对应的文章');
        }

        $this->setCategoryBreadcrumb($category->getId(), 'article_list')->add($article->getTitle());

        return array('article' => $article, 'category' => $category);
    }

    /**
     * @return ArticleRepository
     */
    private function getArticleRepository()
    {
        return $this->get('article.entity.article_repository');
    }
}
