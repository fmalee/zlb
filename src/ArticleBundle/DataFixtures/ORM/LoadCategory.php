<?php

namespace ArticleBundle\DataFixtures\ORM;

use CoreBundle\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use SiteBundle\Entity\Category;

class LoadCategory extends AbstractFixture
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $this
            ->createObjectFactory($manager, 'SiteBundle\Entity\Category')
            ->setDefaults(['module' => 1, 'status' => 1])
            ->add(['title' => '一级分类', 'name' => 'bar'])
            ->add(['title' => '二级分类', 'name' => 'car'])
            ->add(['title' => '三级分类', 'name' => 'dar'])
        ;

        //$manager->flush();

    }
}