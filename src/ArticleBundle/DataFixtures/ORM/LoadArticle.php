<?php

namespace ArticleBundle\DataFixtures\ORM;

use CoreBundle\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use ArticleBundle\Entity\Article;

class LoadArticle extends AbstractFixture
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $this
            ->createObjectFactory($manager, 'ArticleBundle\Entity\Article')
            ->setDefaults(['categoryId' => 1])
            ->add(['name' => 'foo', 'description' => 'bar', 'content' => 'baz'])
            ->add(['name' => 'goo', 'description' => 'car', 'content' => 'caz'])
            ->add(['name' => 'goo1', 'description' => 'dar', 'content' => 'daz'])
        ;

        $manager->flush();

    }
}