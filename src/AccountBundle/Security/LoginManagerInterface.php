<?php
namespace AccountBundle\Security;

use AccountBundle\Entity\User;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Wenming Tang <tang@babyfamily.com>
 */
interface LoginManagerInterface
{
    public function loginUser($firewallName, User $user, Response $response = null);
}