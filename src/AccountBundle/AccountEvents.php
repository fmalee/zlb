<?php

namespace AccountBundle;

/**
 * @author Wenming Tang <tang@babyfamily.com>
 */
final class AccountEvents
{
    const PROFILE_EDIT_COMPLETED = 'account.profile_edit.completed';

    const EMAIL_CHANGE_COMPLETED = 'account.email_change.completed';

    const REGISTRATION_SUCCESS = 'account.registration.success';

    const REGISTRATION_COMPLETED = 'account.registration.completed';

    const REGISTRATION_CONFIRMED = 'account.registration.confirmed';

    const CHANGE_PASSWORD_COMPLETED = 'account.change_password.edit.completed';
 
    const PASSWORD_RECOVERY = 'account.password.recovery';

    const RESETTING_RESET_COMPLETED = 'account.resetting.reset.completed';

    const SECURITY_IMPLICIT_LOGIN = 'account.security.implicit_login';
}