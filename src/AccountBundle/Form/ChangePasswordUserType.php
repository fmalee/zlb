<?php

namespace AccountBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ChangePasswordUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('currentPassword', 'password', array(
                'label'       => '当前密码',
//                'label_attr' => array(
//                    'class' =>'ss',
//                ),
                'attr' => array(
                    'placeholder' => '请先输入当前的密码',
                ),
            ))
            ->add('plainPassword', 'repeated', array(
                'type'            => 'password',
                'first_options'   => array('label' => '新密码'),
                'second_options'  => array('label' => '重新输入密码'),
                'invalid_message' => '两次输入密码不匹配'
            ))
            ->add('save', 'submit', array('label' => '提交'));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'        => 'AccountBundle\Entity\User',
            'validation_groups' => array('ChangePassword')
        ));
    }

    public function getName()
    {
        return 'account_password';
    }
}