<?php

namespace AccountBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EditLocationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('areaId', 'hidden', array(
                'required' => true
            ))
            ->add('address', 'text', array(
                'label'       => '街道地址',
                'attr' => array(
                    'placeholder' => '不需要重复填写省/市/区部分',
                ),
            ))
            ->add('zipCode', 'number', array(
                'label'       => '邮政编码',
                'required' => false,
                'invalid_message'       => '只能输入数字',
            ))
            ->add('contact', 'text', array(
                'label'       => '联系人',
            ))
            ->add('phone', 'text', array(
                'label'       => '固定电话',
                'required' => false,
                'attr' => array(
                    'placeholder' => '如 021-12345678',
                )
            ))
            ->add('mobilePhone', 'text', array(
                'label'       => '手机',
                'required' => false,
                'attr' => array(
                    'placeholder' => '如 15158899866',
                )
            ))
            ->add('email', 'email', array(
                'label'       => '邮件地址',
                'required' => false,
                'attr' => array(
                    'placeholder' => '收发货的时候我们会同时邮件提醒',
                )
            ))
            ->add('company', 'text', array(
                'label'       => '公司名称',
                'required' => false,
                'attr' => array(
                    'placeholder' => '让我们更容易找到您',
                )
            ))
            ->add('memo', 'textarea', array(
                'label'       => '备注',
                'required' => false,
                'attr'     => array(
                    'rows'  => 2,
                    'cols'  => 50
                )
            ))
            ->add('defaulted', 'choice', array(
                'label'       => '首选地址',
                'choices' => array(0 => '不是', 1 => '是'),
                'expanded'    => true
            ))
            ->add('save', 'submit', array('label' => '保存'));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'        => 'AccountBundle\Entity\Location',
            'validation_groups' => array('Edit')
        ));
    }

    public function getName()
    {
        return 'location';
    }
}