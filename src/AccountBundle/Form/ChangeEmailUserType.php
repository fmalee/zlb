<?php

namespace AccountBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ChangeEmailUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('currentPassword', 'password', array(
                'label'       => '当前密码',
                'attr' => array(
                    'placeholder' => '请先输入当前的密码',
                ),
            ))
            ->add('email', 'email', array(
                'label'       => '新登录邮箱',
                'data' => null,
                'attr' => array(
                    'placeholder' => '请先输入新的电子邮件地址',
                ),

            ))
            ->add('save', 'submit', array('label' => '提交'));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'        => 'AccountBundle\Entity\User',
            'validation_groups' => array('ChangeEmail')
        ));
    }

    public function getName()
    {
        return 'account_email';
    }
}