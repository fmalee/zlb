<?php

namespace AccountBundle\Form;

use AccountBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * @author Wenming Tang <tang@babyfamily.com>
 */
class RegistrationUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', 'text', array(
                    'label' => '用户名',
                    'attr'  => array(
                        'placeholder' => '字母、数字、_或-'
                    ))
            )
            ->add('email', 'email', array('label' => '电子邮箱'))
            ->add('plainPassword', 'password', array(
                'label' => '密码',
            ))
           ->add('plainPassword', 'repeated', array(
               'type'            => 'password',
               'first_options'   => array('label' => '密码'),
               'second_options'  => array('label' => '确认密码'),
               'invalid_message' => '两次输入密码不匹配'
           ))
            ->add('screenName', 'text', array('label' => '昵称'))
            ->add('save', 'submit', array('label' => '注 册'));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'        => 'AccountBundle\Entity\User',
            'validation_groups' => array('Registration')
        ));
    }

    public function getName()
    {
        return 'account_registration';
    }
}