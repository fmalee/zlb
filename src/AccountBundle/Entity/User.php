<?php

namespace AccountBundle\Entity;

use Symfony\Component\Security\Core\User\AdvancedUserInterface;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;

/**
 * User
 *
 * @ORM\Entity(repositoryClass="AccountBundle\Entity\UserRepository")
 * @ORM\Table(name="mc_user",indexes={@ORM\Index(name="search_idx", columns={"created"})})
 * @UniqueEntity(
 *     fields={"username"},
 *     message="该用户名已存在",
 *     groups={"Registration", "Profile"}
 * )
 * @UniqueEntity(
 *     fields={"email"},
 *     message="该电子邮件地址已存在",
 *     groups={"Registration", "ChangeEmail"}
 * )
 */
class User implements AdvancedUserInterface, \Serializable
{
    const ROLE_DEFAULT     = 'ROLE_USER';
    const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=20, unique=true)
     *
     * @Assert\NotBlank(message = "用户名不能为空", groups={"Registration"})
     * @Assert\Length(
     *      min = "3",
     *      max = "20",
     *      minMessage = "用户名长度至少需要{{ limit }}个字符",
     *      maxMessage = "用户名长度不能超过{{ limit }}个字符",
     *      groups={"Registration"}
     * )
     * @Assert\Regex(
     *      pattern="/^[^-_]+[a-z0-9-_]+[^-_]$/",
     *      message="用户名只能包含字母、数字、_或减号，不能以_或减号开头或结尾。",
     *      groups={"Registration"}
     * )
     */
    protected $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
     */
    protected $password;

    /**
     * @var string
     *
     * @Assert\NotBlank(message = "密码不能为空", groups={"Registration", "ChangePassword", "Recovery"})
     * @Assert\Length(
     *      min = "6",
     *      minMessage = "密码长度至少需要 {{ limit }} 位",
     *      groups={"Registration", "ChangePassword", "Recovery"}
     * )
     */
    protected $plainPassword;

    /**
     * @var string
     *
     * @SecurityAssert\UserPassword(
     *     message = "当前密码不正确",
     *     groups={"ChangePassword", "ChangeEmail"}
     * )
     */
    protected $currentPassword;

    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=255)
     */
    protected $salt;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     *
     * @Assert\NotBlank(message = "电子邮件地址不能为空", groups={"Registration", "ChangeEmail"})
     * @Assert\Email(
     *      message = "'{{ value }}' 不是一个有效的电子邮件地址",
     *      groups={"Registration", "ChangeEmail"}
     * )
     */
    protected $email;

    /**
     * @var string
     *
     * @ORM\Column(name="screen_name", type="string", length=20)
     *
     * @Assert\NotBlank(message = "昵称不能为空", groups={"Registration", "Profile"})
     * @Assert\Length(
     *      min = "2",
     *      max = "20",
     *      minMessage = "昵称长度至少需要{{ limit }}个字符",
     *      maxMessage = "昵称长度不能超过{{ limit }}个字符",
     *      groups={"Registration", "Profile"}
     * )
     */
    protected $screenName;

    /**
     * @var decimal
     *
     * @ORM\Column(name="amount", type="decimal", scale=2)
     */
    protected $amount;

    /**
     * @var integer
     *
     * @ORM\Column(name="score", type="integer")
     */
    protected $score;

    /**
     * @var string
     *
     * @ORM\Column(name="avatar_big_url", type="string", length=255, nullable=true)
     */
    protected $avatarBigUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="avatar_small_url", type="string", length=255, nullable=true)
     */
    protected $avatarSmallUrl;

    /**
     * @var boolean
     *
     * @ORM\Column(name="email_confirmed", type="boolean")
     */
    protected $emailConfirmed;

    /**
     * @var string
     *
     * @ORM\Column(name="confirmation_token", type="string", nullable=true)
     */
    protected $confirmationToken;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="recovery_at", type="datetime", nullable=true)
     */
    protected $recoveryAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="locked", type="boolean")
     */
    protected $locked;

    /**
     * @var bool
     *
     * @ORM\Column(name="enabled", type="boolean")
     */
    protected $enabled;

    /**
     * @var bool
     *
     * @ORM\Column(name="expired", type="boolean")
     */
    protected $expired;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="expires_at", type="datetime", nullable=true)
     */
    protected $expiresAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="credentials_expired", type="boolean")
     */
    protected $credentialsExpired;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="credentials_expires_at", type="datetime", nullable=true)
     */
    protected $credentialsExpiresAt;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="last_login", type="datetime", nullable=true)
     */
    protected $lastLogin;

    /**
     * @var string
     *
     * @ORM\Column(name="last_login_ip", type="string", length=15, nullable=true)
     */
    protected $lastLoginIp;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    protected $created;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="modified", type="datetime")
     */
    protected $modified;

    /**
     * @var Profile[]
     * @ORM\OneToOne(targetEntity="Profile", mappedBy="user", cascade={"remove"})
     */
    protected $profile;

    /**
     * @var Connect[]
     * @ORM\OneToMany(targetEntity="Connect", mappedBy="user", cascade={"remove"})
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    protected $connects;

    /**
     * @var Group[]
     * @ORM\ManyToMany(targetEntity="Group", inversedBy="users", cascade={"remove"})
     */
    protected $groups;

    /**
     * @var Trade[]
     * @ORM\OneToMany(targetEntity="TradeBundle\Entity\Trade", mappedBy="user", cascade={"remove"})
     * @ORM\OrderBy({"created" = "DESC"})
     */
    protected $trades;

    /**
     * @var Refund[]
     * @ORM\OneToMany(targetEntity="TradeBundle\Entity\Refund", mappedBy="user", cascade={"remove"})
     * @ORM\OrderBy({"created" = "DESC"})
     */
    protected $refunds;

    /**
     * @var Location[]
     * @ORM\OneToMany(targetEntity="AccountBundle\Entity\Location", mappedBy="user", cascade={"remove"})
     * @ORM\OrderBy({"created" = "DESC"})
     */
    protected $locations;

    /**
     * @var Article[]
     * @ORM\OneToMany(targetEntity="ArticleBundle\Entity\Article", mappedBy="user")
     */
    protected $articles;

    /**
     * @var Mall[]
     * @ORM\OneToMany(targetEntity="ProductBundle\Entity\Mall", mappedBy="user")
     */
    protected $malls;

    /**
     * @var Brand[]
     * @ORM\OneToMany(targetEntity="ProductBundle\Entity\Brand", mappedBy="user")
     */
    protected $brands;

    /**
     * @var Product[]
     * @ORM\OneToMany(targetEntity="ProductBundle\Entity\Product", mappedBy="user")
     */
    protected $products;

    /**
     * @var TbShop[]
     * @ORM\OneToMany(targetEntity="TaobaoBundle\Entity\TbShop", mappedBy="user")
     */
    protected $shops;

    /**
     * @var TbItem[]
     * @ORM\OneToMany(targetEntity="TaobaoBundle\Entity\TbItem", mappedBy="user")
     */
    protected $items;

    public function __construct()
    {
        $this->locked             = false;
        $this->expired            = false;
        $this->credentialsExpired = false;
        $this->enabled            = true;
        $this->emailConfirmed     = false;
        $this->salt               = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
        $this->amount             = 0.00;
        $this->score              = 0;
        $this->created            = new \Datetime();
        $this->modified           = new \Datetime();
        $this->connects           = new ArrayCollection();
        $this->groups             = new ArrayCollection();
        $this->articles           = new ArrayCollection();
        $this->malls              = new ArrayCollection();
        $this->brands             = new ArrayCollection();
        $this->products           = new ArrayCollection();
        $this->shops              = new ArrayCollection();
        $this->items              = new ArrayCollection();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * {@inheritdoc}
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * {@inheritdoc}
     */
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * {@inheritdoc}
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * {@inheritdoc}
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * {@inheritdoc}
     */
    public function setScreenName($screenName)
    {
        $this->screenName = $screenName;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getScreenName()
    {
        return $this->screenName;
    }

    /**
     * {@inheritdoc}
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * {@inheritdoc}
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * {@inheritdoc}
     */
    public function setAvatarBigUrl($avatarBigUrl)
    {
        $this->avatarBigUrl = $avatarBigUrl;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getAvatarBigUrl()
    {
        return $this->avatarBigUrl ? : 'img/default-avatar128.png';
    }

    /**
     * {@inheritdoc}
     */
    public function setAvatarSmallUrl($avatarSmallUrl)
    {
        $this->avatarSmallUrl = $avatarSmallUrl;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getAvatarSmallUrl()
    {
        return $this->avatarSmallUrl ? : 'img/default-avatar48.png';
    }

    /**
     * {@inheritdoc}
     */
    public function setEmailConfirmed($boolean)
    {
        $this->emailConfirmed = $boolean;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function isEmailConfirmed()
    {
        return $this->emailConfirmed;
    }

    /**
     * {@inheritdoc}
     */
    public function setConfirmationToken($confirmationToken)
    {
        $this->confirmationToken = $confirmationToken;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getConfirmationToken()
    {
        return $this->confirmationToken;
    }

    /**
     * {@inheritdoc}
     */
    public function setRecoveryAt(\DateTime $date = null)
    {
        $this->recoveryAt = $date;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getRecoveryAt()
    {
        return $this->recoveryAt;
    }

    /**
     * {@inheritdoc}
     */
    public function isRecoveryNonExpired($ttl)
    {
        return $this->recoveryAt instanceof \DateTime && $this->recoveryAt->getTimestamp() + $ttl > time();
    }

    /**
     * {@inheritdoc}
     * catch from $this->groups
     */
    public function getRoles()
    {
        $roles = $this->groups->toArray();

        return $roles;
    }

    /**
     * {@inheritdoc}
     */
    public function hasRole($role)
    {
        return in_array(strtoupper($role), $this->getRoles(), true);
    }

    /**
     * {@inheritdoc}
     */
    public function setEnabled($boolean)
    {
        $this->enabled = (Boolean)$boolean;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * {@inheritdoc}
     */
    public function isSuperAdmin()
    {
        return $this->hasRole(static::ROLE_SUPER_ADMIN);
    }

    /**
     * {@inheritdoc}
     */
    public function setSuperAdmin($boolean)
    {
        if (true === $boolean) {
            $this->addRole(static::ROLE_SUPER_ADMIN);
        } else {
            $this->removeRole(static::ROLE_SUPER_ADMIN);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setLocked($boolean)
    {
        $this->locked = (Boolean)$boolean;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function isAccountNonLocked()
    {
        return !$this->locked;
    }

    /**
     * {@inheritdoc}
     */
    public function setExpired($expired)
    {
        $this->expired = (Boolean)$expired;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function isAccountNonExpired()
    {
        if (true == $this->expired) {
            return false;
        }

        if (null !== $this->expiresAt && $this->expiresAt->getTimestamp() < time()) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function setExpiresAt(\Datetime $datetime)
    {
        $this->expiresAt = $datetime;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setCredentialsExpired($boolean)
    {
        $this->credentialsExpired = $boolean;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setCredentialsExpiresAt(\Datetime $datetime)
    {
        $this->credentialsExpiresAt = $datetime;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function isCredentialsNonExpired()
    {
        if (true == $this->credentialsExpired) {
            return false;
        }

        if (null !== $this->credentialsExpiresAt && $this->credentialsExpiresAt->getTimestamp() < time()) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * {@inheritdoc}
     */
    public function setLastLogin($lastLogin)
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getLastLogin()
    {
        return $this->lastLogin;
    }

    /**
     * {@inheritdoc}
     */
    public function setLastLoginIp($lastLoginIp)
    {
        $this->lastLoginIp = $lastLoginIp;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getLastLoginIp()
    {
        return $this->lastLoginIp;
    }

    /**
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
        return $this->plainPassword = null;
    }

    /**
     * {@inheritdoc}
     */
    public function equals(AdvancedUserInterface $user)
    {
        return null !== $user && $this->getId() === $user->getId();
    }

    public function serialize()
    {
        return serialize(array(
            $this->password,
            $this->salt,
            $this->username,
            $this->expired,
            $this->locked,
            $this->credentialsExpired,
            $this->enabled,
            $this->id,
        ));
    }

    /**
     * Unserializes the user.
     *
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        $data = unserialize($serialized);
        // add a few extra elements in the array to ensure that we have enough keys when unserializing
        // older data which does not include all properties.
        $data = array_merge($data, array_fill(0, 2, null));

        list(
            $this->password,
            $this->salt,
            $this->username,
            $this->expired,
            $this->locked,
            $this->credentialsExpired,
            $this->enabled,
            $this->id
            ) = $data;
    }

    /**
     * no need
     *
     * {@inheritdoc}
     */
    public function setSalt($salt){}
    public function getEmailConfirmed(){}
    public function getLocked(){}
    public function getEnabled(){}
    public function getExpired(){}
    public function getExpiresAt(){}
    public function getCredentialsExpired(){}
    public function getCredentialsExpiresAt(){}

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return User
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return User
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set profile
     *
     * @param \AccountBundle\Entity\Profile $profile
     * @return User
     */
    public function setProfile(Profile $profile = null)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get profile
     *
     * @return \AccountBundle\Entity\Profile 
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Add connects
     *
     * @param \AccountBundle\Entity\Connect $connects
     * @return User
     */
    public function addConnect(\AccountBundle\Entity\Connect $connects)
    {
        $this->connects[] = $connects;

        return $this;
    }

    /**
     * Remove connects
     *
     * @param \AccountBundle\Entity\Connect $connects
     */
    public function removeConnect(\AccountBundle\Entity\Connect $connects)
    {
        $this->connects->removeElement($connects);
    }

    /**
     * Get connects
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getConnects()
    {
        return $this->connects;
    }

    /**
     * Add groups
     *
     * @param \AccountBundle\Entity\Group $groups
     * @return User
     */
    public function addGroup(\AccountBundle\Entity\Group $groups)
    {
        $this->groups[] = $groups;

        return $this;
    }

    /**
     * Remove groups
     *
     * @param \AccountBundle\Entity\Group $groups
     */
    public function removeGroup(\AccountBundle\Entity\Group $groups)
    {
        $this->groups->removeElement($groups);
    }

    /**
     * Get groups
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * Get currentPassword
     *
     * @return string
     */
    public function getCurrentPassword()
    {
        return $this->currentPassword;
    }

    /**
     * Set currentPassword
     *
     * @param string $currentPassword
     * @return User
     */
    public function setCurrentPassword($currentPassword)
    {
        $this->currentPassword = $currentPassword;

        return $this;
    }

    /**
     * Add trades
     *
     * @param \TradeBundle\Entity\Trade $trades
     * @return User
     */
    public function addTrade(\TradeBundle\Entity\Trade $trades)
    {
        $this->trades[] = $trades;

        return $this;
    }

    /**
     * Remove trades
     *
     * @param \TradeBundle\Entity\Trade $trades
     */
    public function removeTrade(\TradeBundle\Entity\Trade $trades)
    {
        $this->trades->removeElement($trades);
    }

    /**
     * Get trades
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTrades()
    {
        return $this->trades;
    }

    /**
     * Add refunds
     *
     * @param \TradeBundle\Entity\Refund $refunds
     * @return User
     */
    public function addRefund(\TradeBundle\Entity\Refund $refunds)
    {
        $this->refunds[] = $refunds;

        return $this;
    }

    /**
     * Remove refunds
     *
     * @param \TradeBundle\Entity\Refund $refunds
     */
    public function removeRefund(\TradeBundle\Entity\Refund $refunds)
    {
        $this->refunds->removeElement($refunds);
    }

    /**
     * Get refunds
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRefunds()
    {
        return $this->refunds;
    }

    /**
     * Add locations
     *
     * @param \AccountBundle\Entity\Location $locations
     * @return User
     */
    public function addLocation(\AccountBundle\Entity\Location $locations)
    {
        $this->locations[] = $locations;

        return $this;
    }

    /**
     * Remove locations
     *
     * @param \AccountBundle\Entity\Location $locations
     */
    public function removeLocation(\AccountBundle\Entity\Location $locations)
    {
        $this->locations->removeElement($locations);
    }

    /**
     * Get locations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLocations()
    {
        return $this->locations;
    }

    /**
     * Add malls
     *
     * @param \ProductBundle\Entity\Mall $malls
     * @return User
     */
    public function addMall(\ProductBundle\Entity\Mall $malls)
    {
        $this->malls[] = $malls;

        return $this;
    }

    /**
     * Remove malls
     *
     * @param \ProductBundle\Entity\Mall $malls
     */
    public function removeMall(\ProductBundle\Entity\Mall $malls)
    {
        $this->malls->removeElement($malls);
    }

    /**
     * Get malls
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMalls()
    {
        return $this->malls;
    }

    /**
     * Add brands
     *
     * @param \ProductBundle\Entity\Brand $brands
     * @return User
     */
    public function addBrand(\ProductBundle\Entity\Brand $brands)
    {
        $this->brands[] = $brands;

        return $this;
    }

    /**
     * Remove brands
     *
     * @param \ProductBundle\Entity\Brand $brands
     */
    public function removeBrand(\ProductBundle\Entity\Brand $brands)
    {
        $this->brands->removeElement($brands);
    }

    /**
     * Get brands
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBrands()
    {
        return $this->brands;
    }

    /**
     * Add products
     *
     * @param \ProductBundle\Entity\Product $products
     * @return User
     */
    public function addProduct(\ProductBundle\Entity\Product $products)
    {
        $this->products[] = $products;

        return $this;
    }

    /**
     * Remove products
     *
     * @param \ProductBundle\Entity\Product $products
     */
    public function removeProduct(\ProductBundle\Entity\Product $products)
    {
        $this->products->removeElement($products);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProducts()
    {
        return $this->products;
    }
 
    /**
     * Add articles
     *
     * @param \ArticleBundle\Entity\Article $articles
     * @return User
     */
    public function addArticle(\ArticleBundle\Entity\Article $articles)
    {
        $this->articles[] = $articles;

        return $this;
    }

    /**
     * Remove articles
     *
     * @param $articles
     */
    public function removeArticle(\ArticleBundle\Entity\Article $articles)
    {
        $this->articles->removeElement($articles);
    }

    /**
     * Get articles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * Add shops
     *
     * @param \TaobaoBundle\Entity\TbShop $shops
     * @return User
     */
    public function addShop(\TaobaoBundle\Entity\TbShop $shops)
    {
        $this->shops[] = $shops;

        return $this;
    }

    /**
     * Remove shops
     *
     * @param \TaobaoBundle\Entity\TbShop $shops
     */
    public function removeShop(\TaobaoBundle\Entity\TbShop $shops)
    {
        $this->shops->removeElement($shops);
    }

    /**
     * Get shops
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getShops()
    {
        return $this->shops;
    }

    /**
     * Add items
     *
     * @param \TaobaoBundle\Entity\TbItem $items
     * @return User
     */
    public function addItem(\TaobaoBundle\Entity\TbItem $items)
    {
        $this->items[] = $items;

        return $this;
    }

    /**
     * Remove items
     *
     * @param \TaobaoBundle\Entity\TbItem $items
     */
    public function removeItem(\TaobaoBundle\Entity\TbItem $items)
    {
        $this->items->removeElement($items);
    }

    /**
     * Get items
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getItems()
    {
        return $this->items;
    }
}
