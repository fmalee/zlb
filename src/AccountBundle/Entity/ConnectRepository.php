<?php

namespace AccountBundle\Entity;

use CoreBundle\Doctrine\EntityRepository;

/**
 * ConnectRepository
 */
class ConnectRepository extends EntityRepository
{
    /**
     * {@inheritdoc}
     */
    public function createConnect(User $user)
    {
        $class = $this->getClassName();
        /** @var AccountBundle\Entity\Connect $skill */
        $connect = new $class;
        $connect->setUser($user);

        return $connect;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function findConnectById($id)
    {
        return $this->findOneBy(array('id' => $id));
    }
}
