<?php

namespace AccountBundle\Entity;

use CoreBundle\Doctrine\EntityRepository;
use AccountBundle\Entity\Location;

/**
 * Location Repository
 */
class LocationRepository extends EntityRepository
{
    /**
     * {@inheritdoc}
     */
    public function findLocationBy(array $criteria)
    {
        return $this->findOneBy($criteria);
    }

    /**
     * {@inheritdoc}
     */
    public function countLocationByUserId($userId)
    {
        $q = $this->createQueryBuilder('l');
        $q->select($q->expr()->count('l.id'))
            ->where('l.userId = :userId')
            ->setParameter('userId', $userId);

        return $q->getQuery()->getSingleScalarResult();
    }

    /**
     * {@inheritdoc}
     */
    public function countLocationByUser($userId)
    {
        $dql = 'select count(l.id) as id from AccountBundle\Entity\Location l where l.userId = :userId';
        $q = $this->getEntityManager()->createQuery($dql);
        $q->setParameter('userId', $userId);
        $count = $q->getSingleScalarResult();

        return $count;
    }
}
