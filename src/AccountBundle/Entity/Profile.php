<?php

namespace AccountBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Profile
 *
 * @ORM\Entity(repositoryClass="AccountBundle\Entity\ProfileRepository")
 * @ORM\Table(name="mc_profile")
 * @ORM\HasLifecycleCallbacks()
 */
class Profile
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="real_name", type="string", length=20, nullable=true)
     *
     * @Assert\Length(
     *      min = "2",
     *      max = "20",
     *      minMessage = "姓名长度至少需要{{ limit }}个字符",
     *      maxMessage = "姓名长度不能超过{{ limit }}个字符<",
     *      groups={"Profile"}
     * )
     */
    protected $realName;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", length=6, nullable=true)
     *
     * @Assert\Choice(choices = {"male", "female"}, message = "请选择有效的选项", groups={"Profile"})
     */
    protected $gender;

    /**
     * @var string
     *
     * @ORM\Column(name="birthday", type="date", nullable=true)
     *
     * @Assert\Date(message = "请填写有效的生日", groups={"Profile"})
     */
    protected $birthday;

    /**
     * @var int
     *
     * @ORM\Column(name="degree", type="smallint", nullable=true)
     *
     * @Assert\Length(
     *      min = "1",
     *      max = "5",
     *      minMessage = "请选择正确的学历",
     *      maxMessage = "请选择正确的学历",
     *      groups={"Profile"}
     * )
     */
    protected $degree;

    /**
     * @var string
     *
     * @ORM\Column(name="position_title", type="text", length=100, nullable=true)
     *
     * @Assert\Length(
     *      max = "100",
     *      maxMessage = "职位或头衔长度不能超过{{ limit }}个字符",
     *      groups={"Profile"}
     * )
     */
    protected $positionTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="company_name", type="text", length=200, nullable=true)
     *
     * @Assert\Length(
     *      max = "200",
     *      maxMessage = "公司或机构名称长度不能超过{{ limit }}个字符",
     *      groups={"Profile"}
     * )
     */
    protected $companyName;

    /**
     * @var string
     *
     * @ORM\Column(name="phone_number", type="string", length=11, nullable=true)
     *
     * @Assert\Regex(
     *      pattern="/^1\d{10}$/",
     *      message="手机号码格式不正确",
     *      groups={"Profile"}
     * )
     */
    protected $phoneNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="homepage", type="string", length=255, nullable=true)
     *
     * @Assert\Url(message = "个人主页地址格式不正确", groups={"Profile"})
     */
    protected $homepage;

    /**
     * @var string
     *
     * @ORM\Column(name="bio", type="text", nullable=true)
     *
     * @Assert\Length(
     *      max = "500",
     *      maxMessage = "个人介绍长度不能超过{{ limit }}个字符",
     *      groups={"Profile"}
     * )
     */
    protected $bio;

    /**
     * @var string
     *
     * @ORM\Column(name="qq", type="string", nullable=true)
     *
     * @Assert\Length(
     *      max = "20",
     *      maxMessage = "QQ长度不能超过{{ limit }}个字符",
     *      groups={"Profile"}
     * )
     */
    protected $qq;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", nullable=true)
     *
     * @Assert\Length(
     *      max = "255",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Profile"}
     * )
     */
    protected $location;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="modified", type="datetime")
     */
    protected $modified;

    /**
     * @var User[]
     * @ORM\OneToOne(targetEntity="User", inversedBy="profile", cascade={"remove"})]
     * @ORM\JoinColumn(name="id", referencedColumnName="id")
     */
    protected $user;

    public function __construct()
    {
        $this->modified = new \Datetime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function setPerUdate()
    {
        $this->modified = new \DateTime();
    }

    public function getGenderName()
    {
        $genders = static::getGenders();

        return $genders[$this->gender];
    }

    public static function getGenders()
    {
        return array(
            'male'   => '男',
            'female' => '女'
        );
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Set id
     *
     * @param string $id
     * @return Profile
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Set realName
     *
     * @param string $realName
     * @return Profile
     */
    public function setRealName($realName)
    {
        $this->realName = $realName;

        return $this;
    }

    /**
     * Get realName
     *
     * @return string 
     */
    public function getRealName()
    {
        return $this->realName;
    }

    /**
     * Set gender
     *
     * @param string $gender
     * @return Profile
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string 
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     * @return Profile
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime 
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set degree
     *
     * @param integer $degree
     * @return Profile
     */
    public function setDegree($degree)
    {
        $this->degree = $degree;

        return $this;
    }

    /**
     * Get degree
     *
     * @return integer 
     */
    public function getDegree()
    {
        return $this->degree;
    }

    /**
     * Set positionTitle
     *
     * @param string $positionTitle
     * @return Profile
     */
    public function setPositionTitle($positionTitle)
    {
        $this->positionTitle = $positionTitle;

        return $this;
    }

    /**
     * Get positionTitle
     *
     * @return string 
     */
    public function getPositionTitle()
    {
        return $this->positionTitle;
    }

    /**
     * Set companyName
     *
     * @param string $companyName
     * @return Profile
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get companyName
     *
     * @return string 
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     * @return Profile
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string 
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set homepage
     *
     * @param string $homepage
     * @return Profile
     */
    public function setHomepage($homepage)
    {
        $this->homepage = $homepage;

        return $this;
    }

    /**
     * Get homepage
     *
     * @return string 
     */
    public function getHomepage()
    {
        return $this->homepage;
    }

    /**
     * Set bio
     *
     * @param string $bio
     * @return Profile
     */
    public function setBio($bio)
    {
        $this->bio = $bio;

        return $this;
    }

    /**
     * Get bio
     *
     * @return string 
     */
    public function getBio()
    {
        return $this->bio;
    }

    /**
     * Set qq
     *
     * @param string $qq
     * @return Profile
     */
    public function setQq($qq)
    {
        $this->qq = $qq;

        return $this;
    }

    /**
     * Get qq
     *
     * @return string 
     */
    public function getQq()
    {
        return $this->qq;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return Profile
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Profile
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }


    /**
     * Set user
     *
     * @param \AccountBundle\Entity\User $user
     * @return Profile
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AccountBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
