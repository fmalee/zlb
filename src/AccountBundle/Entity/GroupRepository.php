<?php

namespace AccountBundle\Entity;

use CoreBundle\Doctrine\EntityRepository;

/**
 * GroupRepository
 */
class GroupRepository extends EntityRepository
{
    const ROLE_DEFAULT     = 'ROLE_USER';
    const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';

    /**
     * {@inheritDoc}
     */
    public function createGroup($name)
    {
        $class = $this->getClassName();

        return new $class($name);
    }

    /**
     * {@inheritDoc}
     */
    public function getDefaultGroup()
    {
        return $this->findGroupByRole(static::ROLE_DEFAULT);
    }

    /**
     * {@inheritDoc}
     */
    public function findGroupByName($name)
    {
        return $this->findGroupBy(array('name' => $name));
    }

    /**
     * {@inheritDoc}
     */
    public function findGroupByRole($role)
    {
        return $this->findGroupBy(array('role' => $role));
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function findGroupBy(array $criteria)
    {
        return $this->findOneBy($criteria);
    }
}
