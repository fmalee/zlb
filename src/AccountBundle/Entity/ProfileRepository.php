<?php

namespace AccountBundle\Entity;

use CoreBundle\Doctrine\EntityRepository;

/**
 * ProfileRepository
 */
class ProfileRepository extends EntityRepository
{
    /**
     * {@inheritDoc}
     */
    public function createProfile($user = null)
    {
        $class = $this->getClassName();

        $profile = new $class();
        $profile->setUser($user);

        return $profile;
    }

    /**
     * {@inheritDoc}
     */
    public function findGroupByName($name)
    {
        return $this->findGroupBy(array('name' => $name));
    }

    /**
     * {@inheritDoc}
     */
    public function findGroupById($id)
    {
        return $this->findGroupBy(array('name' => $name));
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function findGroupBy(array $criteria)
    {
        return $this->findOneBy($criteria);
    }
}
