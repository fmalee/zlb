<?php
namespace AccountBundle\Event;

use AccountBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Wenming Tang <tang@babyfamily.com>
 */
class FilterUserResponseEvent extends AccountEvent
{
    private $response;

    public function __construct(User $user, Request $request, Response $response)
    {
        parent::__construct($user, $request);
        $this->response = $response;
    }

    public function getResponse()
    {
        return $this->response;
    }
}