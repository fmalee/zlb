<?php

namespace AccountBundle\Controller;

use AccountBundle\AccountEvents;
use AccountBundle\Event\AccountEvent;
use AccountBundle\Event\FilterUserResponseEvent;
use CoreBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/account")
 */
class AccountController extends Controller
{
    /**
     * @Route("/", name="account_home")
     * @Security("is_granted('IS_AUTHENTICATED_ANONYMOUSLY')")
     * @Template()
     */
    public function homeAction(Request $request)
    {
        $this->get('core.breadcrumb')->add('会员中心');

        return array();
    }

    /**
     * @Route("/confirm/{token}", name="account_confirm")
     */
    public function confirmAction($token)
    {
        $user = $this->getUserRepository()->findUserByConfirmationToken($token);

        if (null === $user) {
            throw $this->createNotFoundException(sprintf('The user with confirmation token "%s" does not exist', $token));
        }

        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->container->get('event_dispatcher');
        
        $user->setConfirmationToken(null);
        $user->setEmailConfirmed(true);

        $this->getUserRepository()->updateUser($user);

        $this->addFlash('success', '您的电子邮件地址已确认');

        return $this->redirectToRoute('account_home');
    }

    /**
     * @Route("/email", name="account_change_email")
     * @Security("has_role('ROLE_USER')")
     * @Template()
     */
    public function emailAction(Request $request)
    {
        if (null == $user = $this->getUser()) {
            throw new AccessDeniedException();
        }
        $oldUser = clone $user;

        $this->get('core.breadcrumb')
            ->add('会员中心', $this->generateUrl('account_home'))
            ->add('更改登录邮箱');

        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $form = $this->createBoundObjectForm($user, 'changeEmail');

        if ($form->isBound() && $form->isValid()) {

            if ($oldUser->getEmail() !== $user->getEmail()) {
                $dispatcher->dispatch(AccountEvents::EMAIL_CHANGE_COMPLETED, new AccountEvent($user, $request));

                $this->addFlash('success', sprintf('验证邮件已发送至%s', $user->getEmail()));

                $this->getUserRepository()->updateUser($user);

                return $this->redirectToRoute('account_home');
            }

            $this->addFlash('error', '请输入不同的邮件地址');
        }

        return array('form' => $form->createView());
    }

    /**
     * @Route("/password", name="account_change_password")
     * @Template()
     */
    public function passwordAction(Request $request)
    {
        if (null == $user = $this->getUser()) {
            throw new AccessDeniedException();
        }

        $this->get('core.breadcrumb')
            ->add('会员中心', $this->generateUrl('account_home'))
            ->add('更改登录密码');

        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        //$dispatcher = $this->get('event_dispatcher');

        $form = $this->createBoundObjectForm($user, 'changePassword');

        if ($form->isBound() && $form->isValid()) {
            $this->getUserRepository()->updateUser($user);

            $this->addFlash('success', '密码已更新');

            //$response = $this->redirectToRoute('account_change_password');

            //$dispatcher->dispatch(AccountEvents::CHANGE_PASSWORD_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

            //return $response;
        }

        return array('form' => $form->createView());
    }

    /**
     * @Route("/reconfirm", name="account_resend_confirm")
     * @Template()
     */
    public function reConfirmAction(Request $request)
    {
        if (null == $user = $this->getUser()) {
            throw new AccessDeniedException();
        }

        if ($user->isEmailConfirmed()) {
            $this->addFlash('error', '您的邮箱地址已经确认过了，无需再次确认');

            if (null !== $from = $request->get('from')) {
                $response = $this->redirect($from);
            } else {
                $response = $this->redirectToRoute('account_home');
            }

            return $response;
        }

        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $this->addFlash('success', '确认邮件已重新发送');

        $response = $this->redirectToRoute('account_home');

        $dispatcher->dispatch(AccountEvents::REGISTRATION_SUCCESS, new FilterUserResponseEvent($user, $request, $response));

        return $response;
    }

    /**
     * @return \AccountBundle\Entity\UserRepository
     */
    private function getUserRepository()
    {
        return $this->get('account.entity.user_repository');
    }
}