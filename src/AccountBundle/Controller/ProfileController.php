<?php

namespace AccountBundle\Controller;

use AccountBundle\AccountEvents;
use AccountBundle\Event\FilterUserResponseEvent;
use AccountBundle\Event\AccountEvent;
use AccountBundle\Form\Photo;
use AccountBundle\Form\ChangeEmailUserType;
use CoreBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/account")
 */
class ProfileController extends Controller
{
    /**
     * @Route("/profile", name="profile_edit")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function editAction(Request $request)
    {
        $user = $this->getUser();
        $profile = $this->getUser()->getProfile();
        if (!$profile) {
            $profile = $this->getProfileRepository()->createProfile($user);
        }

        $this->get('core.breadcrumb')
            ->add('会员中心', $this->generateUrl('account_home'))
            ->add('基本资料');

        $form = $this->createBoundObjectForm($profile, 'edit');

        if ($form->isBound() && $form->isValid()) {

            $this->persist($profile, true);

            $this->addFlash('success', '基本资料已更新');

            if (null !== $from = $request->get('from')) {
                $response = $this->redirect($from);
            } else {
                $response = $this->redirectToRoute('profile_edit');
            }

            return $response;
        }

        return array('form' => $form->createView());
    }

    /**
     * @Route("/show/{username}", name="profile_show")
     * @Template()
     */
    public function showAction($username)
    {
        $user = $this->getUserRepository()->findUserByUsername($username);

        if (null == $user) {
            throw $this->createNotFoundException();
        }

        $this->get('core.breadcrumb')
            ->add($user->getScreenName(), $this->generateUrl('profile_show', array('username' => $username)));

        $pager = $this->getPositionManager()->findPositionsPagerByUser($user);

        if (!$user) {
            throw $this->createNotFoundException();
        }

        return  array(
            'user'  => $user,
            'pager' => $pager,
        );
    }

    /**
     * @Route("/{username}/positions", name="profile_positions")
     * @Template()
     */
    public function positionsAction(Request $request, $username)
    {
        $user = $this->getUserRepository()->findUserByUsername($username);

        if (null == $user) {
            throw $this->createNotFoundException();
        }

        $this->get('breadcrumb')
            ->add($user->getScreenName(), $this->generateUrl('profile_show', array('username' => $username)))
            ->add('全部职位');

        $pager = $this->getPositionManager()->findPositionsPagerByUser($user, $request->get('page', 1));

        return array(
            'user'  => $user,
            'pager' => $pager,
        );
    }

    /**
     * @Route("/face", name="upload_face")
     * @Template()
     */
    public function uploadAction(Request $request)
    {
        if (null == $user = $this->getUser()) {
            throw new AccessDeniedException();
        }

        $this->get('breadcrumb')->add('基本资料', $this->generateUrl('profile_edit'))->add('上传头像');

        /** @var \Symfony\Component\Form\FormInterface $form */
        $form = $this->get('account.form.photo');

        /** @var \AccountBundle\EntityManager\UserManager $userManager */
        $userManager = $this->get('account.entity.user_repository');

        $photo = new Photo();
        $photo->setUser($user);

        $form->setData($photo);

        $form->handleRequest($request);

        if ($form->isValid()) {
            if ($photo->handleFile()) {
                $user->setAvatarBigUrl($photo->getAvatarBigUrl());
                $user->setAvatarSmallUrl($photo->getAvatarSmallUrl());

                $this->getUserRepository()->updateUser($user);

                return $this->redirect($this->generateUrl('profile_edit'));
            }
        }

        return $this->render('AccountBundle:Photo:upload.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @return \AccountBundle\Entity\UserRepository
     */
    private function getUserRepository()
    {
        return $this->get('account.entity.user_repository');
    }

    /**
     * @return \AccountBundle\Entity\ProfileRepository
     */
    private function getProfileRepository()
    {
        return $this->get('account.entity.profile_repository');
    }
}