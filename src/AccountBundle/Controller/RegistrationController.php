<?php

namespace AccountBundle\Controller;

use AccountBundle\AccountEvents;
use AccountBundle\Event\AccountEvent;
use AccountBundle\Event\FilterUserResponseEvent;
use CoreBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class RegistrationController extends Controller
{
    /**
     * @Route("/register", name="register")
     * @Security("is_granted('IS_AUTHENTICATED_ANONYMOUSLY')")
     * @Template()
     */
    public function registerAction(Request $request)
    {
        if (null != $this->getUser()) {
            return $this->redirectToRoute('account_home');
        }

        $this->get('core.breadcrumb')->add('注册会员');

        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user = $this->getUserRepository()->createUser();
        $form = $this->createBoundObjectForm($user, 'registration');

        if ($form->isBound() && $form->isValid()) {
            $dispatcher->dispatch(AccountEvents::REGISTRATION_SUCCESS, new AccountEvent($user, $request));

            //设置默认权限
            $group = $this->get('account.entity.group_repository')->getDefaultGroup();
            $user->addGroup($group);
            $this->getUserRepository()->updateUser($user);

            //增加资料表
            $profile = $this->get('account.entity.profile_repository')->createProfile();
            $profile->setUser($user);
            $this->persist($profile, true);

            $this->addFlash('success', '注册完成，请完善您的资料。');

            $response = $this->redirectToRoute('profile_edit');

            $dispatcher->dispatch(AccountEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

            return $response;
        }

        return array('form' => $form->createView());
    }

    /**
     * @return \AccountBundle\Entity\UserRepository
     */
    private function getUserRepository()
    {
        return $this->get('account.entity.user_repository');
    }
}