<?php

namespace AccountBundle\Controller;

use AccountBundle\Entity\User;
use AccountBundle\AccountEvents;
use AccountBundle\Event\AccountEvent;
use CoreBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * @Route("/account")
 */
class RecoveryController extends Controller
{
    /**
     * @Route("/recovery", name="account_recovery")
     * @Template()
     */
    public function recoveryAction(Request $request)
    {
        if (null != $this->getUser()) {
            return $this->redirectToRoute('account_home');
        }

        $this->get('core.breadcrumb')->add('找回密码');

        $email = $request->request->get('email');

        if ($email) {
            $dispatcher = $this->get('event_dispatcher');

            $user = $this->getUserRepository()->findUserByUsernameOrEmail($email);

            if (null === $user) {
                return array('error' => '用户名或电子邮件地址无效');
            }

            if ($user->isRecoveryNonExpired($this->container->getParameter('user_resetting_token_ttl'))) {
                return array('error' => '请超过24小时候再次重置密码');
            }

            $dispatcher->dispatch(AccountEvents::PASSWORD_RECOVERY, new AccountEvent($user, $request));

            $this->getUserRepository()->updateUser($user);

            $email = $this->getObfuscatedEmail($user);
            $this->addFlash('success', '重置密码邮件已发送至 '. $email .'，请点击邮件中的链接进行密码重置操作。');

            return $this->redirectToRoute('home');
        }
    
        return array();
    }

    /**
     * @Route("/recovery/{token}", name="account_recovery_check")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function resetAction(Request $request, $token)
    {
        $user = $this->getUserRepository()->findUserByConfirmationToken($token);
        if (null === $user) {
            throw new NotFoundHttpException(sprintf('The user with "confirmation token" does not exist for value "%s"', $token));
        }

        if (!$user->isRecoveryNonExpired($this->getParameter('user_resetting_token_ttl'))) {
            $this->addFlash('success', '您的密码重置请求已经过期，请重新申请。');
            return $this->redirectToRoute('account_recovery');
        }

        $form = $this->createBoundObjectForm($user, 'recovery');

        if ($form->isBound() && $form->isValid()) {
            $user->setConfirmationToken(null);
            $user->setRecoveryAt(null);

            $this->getUserRepository()->updateUser($user);

            $this->addFlash('success', '您的密码已重置，请使用新密码登录。');

            return $this->redirectToRoute('login');
        }

        return array(
            'token' => $token,
            'user' => $user,
            'form'  => $form->createView()
        );
    }

    private function getObfuscatedEmail(User $user)
    {
        $email = $user->getEmail();
        if (false !== $pos = strpos($email, '@')) {
            $email = '...' . substr($email, $pos);
        }

        return $email;
    }

    /**
     * @return \AccountBundle\Entity\UserRepository
     */
    private function getUserRepository()
    {
        return $this->get('account.entity.user_repository');
    }
}