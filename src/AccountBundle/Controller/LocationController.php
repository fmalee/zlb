<?php

namespace AccountBundle\Controller;

use CoreBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AccountBundle\Entity\Location;
/**
 * @Route("/account/location")
 */
class LocationController extends Controller
{
    /**
     * @Route("/", name="account_location")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction()
    {
        $maxAddress = 20;

        $locations = $this->getLocationRepository()->findByUserId($this->getUser()->getId());
        $count = count($locations);

        $this->setAccountBreadcrumb()->add('地址库');

        return array(
            'locations' => $locations,
            'count' => $count,
        );
    }

    /**
     * @Route("/add", name="location_add")
     * @Route("/edit/{id}", name="location_edit")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function editAction(Request $request, $id = null)
    {
        $userId = $this->getUser()->getId();

        if ($id) {
            $location = $this->getLocationRepository()->find($id);
            if (!$location || $location->getUserId() != $userId) {
                throw new NotFoundHttpException;
            }
            $area = $location->getArea();
            $arrParent = trim($area->getArrparent() . $location->getAreaId(), ',');
        } else {
            $count = $this->getLocationRepository()->countLocationByUserId($userId);
            if ($count > 20) {
                throw new NotFoundHttpException;
            }

            $location = $this->getLocationRepository()->createNew();
            $arrParent = 0;
        }

        $form = $this->createBoundObjectForm($location, 'edit');

        if ($form->isBound() && $form->isValid()) {
            if (!$id) {
                $areaId = $location->getAreaId();
                $area = $this->getAreaRepository()->find($areaId);
                $location->setArea($area);

                $location->setUser($this->getUser());
                $location->setUserId($userId);
            }

            $this->persist($location, true);

            $this->addFlash('地址编辑成功');

            return $this->redirectToRoute('account_location');
        }

        $this->setAccountBreadcrumb()->add('编辑地址库');

        return array(
            'form' => $form->createView(),
            'id' => $id,
            'arrParent' => $arrParent
        );
    }

    /**
     * @Route("/ajaxedit", name="location_ajaxedit")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function ajaxEditAction()
    {
        return $this->ajaxEdit($this->getLocationRepository());
    }

    /**
     * @Route("/delete/{id}", name="location_delete")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deleteAction($id)
    {
        $location = $this->getLocationRepository()->find($id);
        if (!$location) {
            throw $this->createNotFoundException('不存在的记录');
        }

        $this->remove($location, true);

        $this->addFlash('成功删除地址');

        return $this->ajaxReturn('成功删除');
    }

    /**
     * @return locationRepository
     */
    private function getLocationRepository()
    {
        return $this->get('account.entity.location_repository');
    }

    /**
     * @return MallRepository
     */
    private function getAreaRepository()
    {
        return $this->get('common.entity.area_repository');
    }

    /**
     * {@inheritdoc}
     * 返回后台导航
     *
     * @param string $name
     * @return static|null
     */
    protected function setAccountBreadcrumb()
    {
        $this->getBreadcrumb()
            ->add('会员管理', $this->generateUrl('account_home'))
            ->add('地址管理', $this->generateUrl('account_location'));

        return $this->getBreadcrumb();
    }
}
