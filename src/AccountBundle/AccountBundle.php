<?php

namespace AccountBundle;

use CoreBundle\AppBundle\Bundle;
use AccountBundle\Compiler;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class AccountBundle extends Bundle
{
    public function build(ContainerBuilder $builder)
    {
        parent::build($builder);

        /* 给UserRepository添加encoder_factory服务*/
        $builder->addCompilerPass(new Compiler\AddUserRepositoryPass());
    }
}