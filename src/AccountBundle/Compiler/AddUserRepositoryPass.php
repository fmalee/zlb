<?php

namespace AccountBundle\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Add security.encoder_factory
 *
 */
class AddUserRepositoryPass implements CompilerPassInterface {

    public function process(ContainerBuilder $container)
    {
        $userRepository = $container->findDefinition('account.entity.user_repository');
        $reference = new Reference('security.encoder_factory');
        $userRepository->addMethodCall('setEncoderFactory', array($reference));
    }
}
