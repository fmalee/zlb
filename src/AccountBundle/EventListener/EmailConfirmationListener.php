<?php
namespace AccountBundle\EventListener;

use CoreBundle\Mailer\MessageFactory;
use AccountBundle\Event\AccountEvent;
use AccountBundle\AccountEvents;
use CoreBundle\Util\TokenGeneratorInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @author Wenming Tang <tang@babyfamily.com>
 */
class EmailConfirmationListener implements EventSubscriberInterface
{
    private $message;
    private $mailer;
    private $tokenGenerator;

    public function __construct(MessageFactory $message, TokenGeneratorInterface $tokenGenerator)
    {
        $this->message         = $message;
        $this->mailer         = $this->message->getMailer();
        $this->tokenGenerator = $tokenGenerator;
    }

    public static function getSubscribedEvents()
    {
        return array(
            AccountEvents::REGISTRATION_SUCCESS   => 'onRegistrationSuccess',
            AccountEvents::EMAIL_CHANGE_COMPLETED => 'onEmailChangeCompleted',
            AccountEvents::PASSWORD_RECOVERY => 'onPasswordRecovery',
        );
    }

    public function onEmailChangeCompleted(AccountEvent $event)
    {
        $user = $event->getUser();

        $user->setEmailConfirmed(false);

        if (null === $user->getConfirmationToken()) {
            $user->setConfirmationToken($this->tokenGenerator->generateToken());
        }

        $message = $this->message->createMessage($this, 'email', array('user'=> $user));
        $this->message->send($message, $user->getEmail());
    }

    public function onPasswordRecovery(AccountEvent $event)
    {
        $user = $event->getUser();

        if (null === $user->getConfirmationToken()) {
            $user->setConfirmationToken($this->tokenGenerator->generateToken());
        }

        $user->setRecoveryAt(new \DateTime());

        $message = $this->message->createMessage($this, 'recovery', array('user'=> $user));
        $this->message->send($message, $user->getEmail());
    }

    public function onRegistrationSuccess(AccountEvent $event)
    {
        $user = $event->getUser();

        if (null === $user->getConfirmationToken()) {
            $user->setConfirmationToken($this->tokenGenerator->generateToken());
        }

        $message = $this->message->createMessage($this, 'registration', array('user'=> $user));
        $this->message->send($message, $user->getEmail());
    }
}