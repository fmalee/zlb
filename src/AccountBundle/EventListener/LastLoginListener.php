<?php

namespace AccountBundle\EventListener;

use AccountBundle\AccountEvents;
use AccountBundle\Event\AccountEvent;
use AccountBundle\Entity\User;
use AccountBundle\Entity\UserRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;

/**
 * @author Wenming Tang <tang@babyfamily.com>
 */
class LastLoginListener implements EventSubscriberInterface
{
    protected $userRepository;
    protected $request;
    protected $requestStack;

    public function __construct(UserRepository $userRepository, RequestStack $requestStack)
    {
        $this->userRepository  = $userRepository;
        $this->requestStack = $requestStack;
        $this->request = $this->requestStack->getCurrentRequest();
    }

    public static function getSubscribedEvents()
    {
        return array(
            AccountEvents::SECURITY_IMPLICIT_LOGIN => 'onImplicitLogin',
            SecurityEvents::INTERACTIVE_LOGIN   => 'onSecurityInteractiveLogin',
        );
    }

    public function onImplicitLogin(AccountEvent $event)
    {
        $user = $event->getUser();

        $user->setLastLogin(new \DateTime());
        $user->setLastLoginIp($this->request->getClientIp());
        $this->userRepository->updateUser($user);
    }

    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        $user = $event->getAuthenticationToken()->getUser();

        if ($user instanceof User) {
            $user->setLastLogin(new \DateTime());
            $user->setLastLoginIp($this->request->getClientIp());
            $this->userRepository->updateUser($user);
        }
    }
}