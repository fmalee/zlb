<?php
namespace AccountBundle\EventListener;

use AccountBundle\Event\FilterUserResponseEvent;
use AccountBundle\Event\AccountEvent;
use AccountBundle\Security\LoginManagerInterface;
use AccountBundle\AccountEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Exception\AccountStatusException;

/**
 * @author Wenming Tang <tang@babyfamily.com>
 */
class AuthenticationListener implements EventSubscriberInterface
{
    private $loginManager;
    private $firewallName;

    public function __construct(LoginManagerInterface $loginManager, $firewallName)
    {
        $this->loginManager = $loginManager;
        $this->firewallName = $firewallName;
    }

    public static function getSubscribedEvents()
    {
        return array(
            AccountEvents::REGISTRATION_COMPLETED    => 'authenticate',
            AccountEvents::RESETTING_RESET_COMPLETED => 'authenticate'
        );
    }

    public function authenticate(FilterUserResponseEvent $event)
    {
        if (!$event->getUser()->isEnabled()) {
            return;
        }

        try {
            $this->loginManager->loginUser($this->firewallName, $event->getUser(), $event->getResponse());

            $event->getDispatcher()->dispatch(AccountEvents::SECURITY_IMPLICIT_LOGIN, new AccountEvent($event->getUser(), $event->getRequest()));
        } catch (AccountStatusException $ex) {
            // We simply do not authenticate users which do not pass the user
            // checker (not enabled, expired, etc.).
        }
    }
}