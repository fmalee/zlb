<?php

namespace ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use CommonBundle\Entity\Category;

/**
 * Mall
 *
 * @ORM\Entity(repositoryClass="ProductBundle\Entity\MallRepository")
 * @ORM\Table(name="mc_mall", indexes={@ORM\Index(name="name", columns={"name"})})
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(
 *     fields={"name"},
 *     message="该标签名已存在",
 *     groups={"Edit"}
 * )
 */
class Mall
{
    const STATUS_TRASH      = -1;
    const STATUS_DRAFT      = 0;
    const STATUS_PUBLISH    = 1;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="user_id", type="integer")
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="category_id", type="integer")
     */
    private $categoryId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=30, unique=true)
     *
     * @Assert\Length(
     *      max = "30",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     * @Assert\Regex(
     *      pattern="/^[^-_]+[a-z0-9-_]+[^-_]$/",
     *      message="标签名只能包含字母、数字、_或减号，不能以_或减号开头或结尾。",
     *      groups={"Edit"}
     * )
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=50)
     *
     * @Assert\NotBlank(message = "标题不能为空", groups={"Edit"})
     * @Assert\Length(
     *      max = "50",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     */
    private $title;

    /**
     * @var integer
     *
     * @ORM\Column(name="icon", type="string", length=255, nullable=true)
     */
    private $icon;

    /**
     * @var smallint
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="homePage", type="string", length=255, nullable=true)
     *
     * @Assert\Length(
     *      max = "255",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     */
    private $homePage;

    /**
     * @var string
     *
     * @ORM\Column(name="area_id", type="integer")
     */
    private $areaId;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=200, nullable=true)
     *
     * @Assert\Length(
     *      max = "200",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_title", type="string", length=50, nullable=true)
     *
     * @Assert\Length(
     *      max = "50",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     */
    private $metaTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="keywords", type="string", length=255, nullable=true)
     *
     * @Assert\Length(
     *      max = "255",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     */
    private $keywords;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     *
     * @Assert\Length(
     *      max = "255",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     *
     * @Assert\NotBlank(message = "内容不能为空", groups={"Edit"})
     */
    private $content;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="modified", type="datetime")
     */
    private $modified;

    /**
     * @ORM\ManyToOne(targetEntity="AccountBundle\Entity\User", inversedBy="malls")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="CommonBundle\Entity\Category", inversedBy="malls")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="CommonBundle\Entity\Area", inversedBy="malls")
     * @ORM\JoinColumn(name="area_id", referencedColumnName="id")
     */
    private $area;

    /**
     * @ORM\OneToMany(targetEntity="Brand", mappedBy="mall")
     */
    private $brands;

    public function __construct()
    {
        $this->categoryId     = 0;
        $this->status     = static::STATUS_PUBLISH;
        $this->created     = new \Datetime();
        $this->modified    = new \Datetime();
        $this->brands    = new ArrayCollection();
    }

    /**
     * @ORM\PreUpdate
     */
    public function setPerUpdate()
    {
        $this->modified = new \DateTime();
    }

    public function __toString()
    {
        return $this->title;
    }

    public static function getStatusList()
    {
        return array(
            static::STATUS_DRAFT  => '草稿箱',
            static::STATUS_PUBLISH  => '发布'
        );
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return Mall
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set categoryId
     *
     * @param integer $categoryId
     * @return Mall
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;

        return $this;
    }

    /**
     * Get categoryId
     *
     * @return integer
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * Set areaId
     *
     * @param integer $areaId
     * @return Mall
     */
    public function setAreaId($areaId)
    {
        $this->areaId = $areaId;

        return $this;
    }

    /**
     * Get areaId
     *
     * @return integer
     */
    public function getAreaId()
    {
        return $this->areaId;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Mall
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Mall
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set icon
     *
     * @param string $icon
     * @return Mall
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string 
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set status
     *
     * @param smallint $status
     * @return Mall
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return smallint 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set homePage
     *
     * @param string $homePage
     * @return Mall
     */
    public function setHomePage($homePage)
    {
        $this->homePage = $homePage;

        return $this;
    }

    /**
     * Get homePage
     *
     * @return string 
     */
    public function getHomePage()
    {
        return $this->homePage;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Mall
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set metaTitle
     *
     * @param string $metaTitle
     * @return Mall
     */
    public function setMetaTitle($metaTitle)
    {
        $this->metaTitle = $metaTitle;

        return $this;
    }

    /**
     * Get metaTitle
     *
     * @return string 
     */
    public function getMetaTitle()
    {
        return $this->metaTitle;
    }

    /**
     * Set keywords
     *
     * @param string $keywords
     * @return Mall
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get keywords
     *
     * @return string 
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Mall
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Mall
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Mall
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Mall
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }


    /**
     * Add brands
     *
     * @param \ProductBundle\Entity\Brand $brands
     * @return Mall
     */
    public function addBrand(\ProductBundle\Entity\Brand $brands)
    {
        $this->brands[] = $brands;

        return $this;
    }

    /**
     * Remove brands
     *
     * @param \ProductBundle\Entity\Brand $brands
     */
    public function removeBrand(\ProductBundle\Entity\Brand $brands)
    {
        $this->brands->removeElement($brands);
    }

    /**
     * Get brands
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBrands()
    {
        return $this->brands;
    }

    /**
     * Set area
     *
     * @param \CommonBundle\Entity\Area $area
     * @return Mall
     */
    public function setArea(\CommonBundle\Entity\Area $area = null)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area
     *
     * @return \CommonBundle\Entity\Area
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set category
     *
     * @param \CommonBundle\Entity\Category $category
     * @return Mall
     */
    public function setCategory(\CommonBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \CommonBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set user
     *
     * @param \AccountBundle\Entity\User $user
     * @return Mall
     */
    public function setUser(\AccountBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AccountBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
