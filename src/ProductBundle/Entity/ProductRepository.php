<?php

namespace ProductBundle\Entity;

use CoreBundle\Doctrine\EntityRepository;

/**
 * Product Repository
 */
class ProductRepository extends EntityRepository
{
    /**
     * {@inheritdoc}
     */
    public function findProductBy(array $criteria)
    {
        return $this->findOneBy($criteria);
    }

    /**
     * {@inheritdoc}
     */
    public function findProductByCategoryId($categoryId, $limit = 0)
    {
        $qb = $this->createQueryByCategoryId($categoryId, $limit);

        return $qb->getQuery()->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function createQueryByCategoryId($categoryId, $limit = 0)
    {
        $qb = $this->createQueryBuilder('p')
            ->andWhere('p.status = :status')
            ->andWhere('p.categoryId = :categoryId')
            ->addOrderBy('p.created', 'DESC')
            ->setParameter('status', Product::STATUS_PUBLISH)
            ->setParameter('categoryId', $categoryId);
        if ($limit) {
            $qb = $qb->setMaxResults($limit);
        }

        return $qb;
    }

    /**
     * {@inheritdoc}
     */
    public function findProductByChosen($limit)
    {
        $qb = $this->createQueryBuilder('p')
            ->where('p.status = :status')
            ->andWhere('p.chosen = true')
            ->setParameter('status', Product::STATUS_PUBLISH)
            ->setMaxResults( $limit );

        return $qb->getQuery()->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function findProductByCategory($categoryIds)
    {
        if (!is_array($categoryIds)) {
            $categoryIds = trim($categoryIds, ',');
            $categoryIds = explode(',', $categoryIds);
        }

        $qb = $this->createQueryBuilder('p');
        $qb->Where('p.enabled = true')
            ->andWhere($qb->expr()->in('p.categoryId', ':categoryId'))
            ->addOrderBy('p.created', 'DESC')
            ->setParameter('categoryId', $categoryIds);

        return $qb;
    }


    /**
     * {@inheritdoc}
     */
    public function createQueryByStatus($status = Product::STATUS_PUBLISH)
    {

        $qb = $this->createQueryBuilder('p')
/*            ->select('p, u, c')
            ->join('p.user', 'u')
            ->join('p.category', 'c')*/
            ->Where('p.status = :status')
            ->addOrderBy('p.created', 'DESC')
            ->setParameter('status', $status);

        return $qb;
    }

    /**
     * {@inheritdoc}
     */
    public function addQueryByCategory($qb, $categoryId)
    {
        $qb->andWhere('p.categoryId = :categoryId')
            ->setParameter('categoryId', $categoryId);

        return $qb;
    }

    /**
     * {@inheritdoc}
     */
    public function addQueryByCategories($qb, $categoryIds)
    {
        $qb->andWhere($qb->expr()->in('p.categoryId', ':categoryId'))
            ->setParameter('categoryId', $categoryIds);

        return $qb;
    }

    /**
     * {@inheritdoc}
     */
    public function addQueryByTitle($qb, $title)
    {
        $qb->andWhere($qb->expr()->like('p.title', ':title'))
            ->setParameter('title', "%$title%");

        return $qb;
    }
}
