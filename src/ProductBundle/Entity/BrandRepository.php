<?php

namespace ProductBundle\Entity;

use CoreBundle\Doctrine\EntityRepository;

/**
 * Brand Repository
 */
class BrandRepository extends EntityRepository
{
    /**
     * {@inheritdoc}
     */
    public function findBrandBy(array $criteria)
    {
        return $this->findOneBy($criteria);
    }

    /**
     * {@inheritdoc}
     */
    public function createQueryByStatus($status = Brand::STATUS_PUBLISH)
    {

        $qb = $this->createQueryBuilder('b')
            ->select('b, u, m')
            ->join('b.user', 'u')
            ->join('b.mall', 'm')
            ->Where('b.status = :status')
            ->addOrderBy('b.created', 'DESC')
            ->setParameter('status', $status);

        return $qb;
    }

    /**
     * {@inheritdoc}
     */
    public function addQueryByCategory($qb, $categoryId)
    {
        $qb->andWhere('b.categoryId = :categoryId')
            ->setParameter('categoryId', $categoryId);

        return $qb;
    }

    /**
     * {@inheritdoc}
     */
    public function addQueryByCategories($qb, $categoryIds)
    {
        $qb->andWhere($qb->expr()->in('b.categoryId', ':categoryId'))
            ->setParameter('categoryId', $categoryIds);

        return $qb;
    }

    /**
     * {@inheritdoc}
     */
    public function addQueryByTitle($qb, $title)
    {
        $qb->andWhere($qb->expr()->like('b.title', ':title'))
            ->setParameter('title', "%$title%");

        return $qb;
    }

    /**
     * {@inheritdoc}
     */
    public function findBrandByTitle($title, $limit = 50)
    {
        $qb = $this->createQueryBuilder('b');
        $qb->Where('b.status = :status')
            //->andWhere($qb->expr()->like('b.title', ':title'))
            ->andWhere($qb->expr()->orX($qb->expr()->like('b.title', ':title'), $qb->expr()->like('b.name', ':title')))
            ->addOrderBy('b.created', 'DESC')
            ->setParameter('status', Brand::STATUS_PUBLISH)
            ->setParameter('title', "%$title%");
        if ($limit) {
            $qb = $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->getResult();
    }

}
