<?php

namespace ProductBundle\Entity;

use CoreBundle\Doctrine\EntityRepository;

/**
 * MallRepository
 */
class MallRepository extends EntityRepository
{
    /**
     * {@inheritdoc}
     */
    public function findMallBy(array $criteria)
    {
        return $this->findOneBy($criteria);
    }

    /**
     * {@inheritdoc}
     */
    public function createQueryByStatus($status = Mall::STATUS_PUBLISH)
    {

        $qb = $this->createQueryBuilder('m')
            ->select('m, u, c')
            ->join('m.user', 'u')
            ->join('m.category', 'c')
            ->Where('m.status = :status')
            ->addOrderBy('m.created', 'DESC')
            ->setParameter('status', $status);

        return $qb;
    }

    /**
     * {@inheritdoc}
     */
    public function addQueryByCategory($qb, $categoryId)
    {
        $qb->andWhere('m.categoryId = :categoryId')
            ->setParameter('categoryId', $categoryId);

        return $qb;
    }

    /**
     * {@inheritdoc}
     */
    public function addQueryByCategories($qb, $categoryIds)
    {
        $qb->andWhere($qb->expr()->in('m.categoryId', ':categoryId'))
            ->setParameter('categoryId', $categoryIds);

        return $qb;
    }

    /**
     * {@inheritdoc}
     */
    public function addQueryByTitle($qb, $title)
    {
        $qb->andWhere($qb->expr()->like('m.title', ':title'))
            ->setParameter('title', "%$title%");

        return $qb;
    }
}
