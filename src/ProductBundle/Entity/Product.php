<?php

namespace ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use CommonBundle\Entity\Category;

/**
 * Product
 *
 * @ORM\Entity(repositoryClass="ProductBundle\Entity\ProductRepository")
 * @ORM\Table(name="mc_product", indexes={@ORM\Index(name="name", columns={"name"})})
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(
 *     fields={"name"},
 *     message="该标签名已存在",
 *     groups={"Edit"}
 * )
 */
class Product
{
    const STATUS_TRASH      = -1;
    const STATUS_STOCK      = 0;
    const STATUS_PUBLISH    = 1;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer")
     */
    private $userId;

    /**
     * @var integer
     *
     * @ORM\Column(name="category_id", type="integer")
     */
    private $categoryId;

    /**
     * @var integer
     *
     * @ORM\Column(name="brand_id", type="integer")
     */
    private $brandId;

    /**
     * @var integer
     *
     * @ORM\Column(name="supply_id", type="integer", nullable=true)
     */
    private $supplyId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=30, unique=true)
     *
     * @Assert\Length(
     *      max = "30",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     * @Assert\Regex(
     *      pattern="/^[^-_]+[a-z0-9-_]+[^-_]$/",
     *      message="标签名只能包含字母、数字、_或减号，不能以_或减号开头或结尾。",
     *      groups={"Edit"}
     * )
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=50)
     *
     * @Assert\NotBlank(message = "标题不能为空", groups={"Edit"})
     * @Assert\Length(
     *      max = "50",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     */
    private $title;

    /**
     * @var integer
     *
     * @ORM\Column(name="icon", type="string", length=255, nullable=true)
     */
    private $icon;

    /**
     * @var smallint
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;

    /**
     * @var boolean
     *
     * @ORM\Column(name="chosen", type="boolean")
     */
    private $chosen;

    /**
     * @var string
     *
     * @ORM\Column(name="sku_1", type="string", length=20, nullable=true)
     *
     * @Assert\Length(
     *      max = "20",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     */
    private $sku1;

    /**
     * @var string
     *
     * @ORM\Column(name="sku_2", type="string", length=20)
     *
     * @Assert\Length(
     *      max = "20",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     */
    private $sku2;

    /**
     * @var decimal
     *
     * @ORM\Column(name="selling_price", type="decimal", scale=2)
     */
    protected $sellingPrice;

    /**
     * @var decimal
     *
     * @ORM\Column(name="current_price", type="decimal", scale=2)
     */
    protected $currentPrice;

    /**
     * @var decimal
     *
     * @ORM\Column(name="buiing_price", type="decimal", scale=2)
     */
    protected $buiingPrice;

    /**
     * @var integer
     *
     * @ORM\Column(name="stock", type="integer")
     */
    protected $stock;

    /**
     * @var integer
     *
     * @ORM\Column(name="scrap", type="integer")
     */
    protected $scrap;

    /**
     * @var string
     *
     * @ORM\Column(name="barcode", type="string", length=20, nullable=true)
     *
     * @Assert\Length(
     *      max = "20",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     */
    private $barcode;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_title", type="string", length=50, nullable=true)
     *
     * @Assert\Length(
     *      max = "50",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     */
    private $metaTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="keywords", type="string", length=128, nullable=true)
     *
     * @Assert\Length(
     *      max = "128",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     */
    private $keywords;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     *
     * @Assert\Length(
     *      max = "255",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     *
     * @Assert\NotBlank(message = "内容不能为空", groups={"Edit"})
     */
    private $content;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="modified", type="datetime")
     */
    private $modified;

    /**
     * @ORM\ManyToOne(targetEntity="AccountBundle\Entity\User", inversedBy="products")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="CommonBundle\Entity\Category", inversedBy="products")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="Brand", inversedBy="products")
     * @ORM\JoinColumn(name="brand_id", referencedColumnName="id")
     */
    private $brand;

    /**
     * @ORM\OneToMany(targetEntity="Supply", mappedBy="product")
     */
    private $supplies;

    /**
     * @ORM\OneToMany(targetEntity="ProductImage", mappedBy="product")
     */
    private $images;

    /**
     * @ORM\OneToMany(targetEntity="Sku", mappedBy="product")
     */
    private $skus;

    /**
     * @ORM\OneToMany(targetEntity="TradeBundle\Entity\Order", mappedBy="product")
     */
    private $orders;

    /**
     * @ORM\OneToMany(targetEntity="TradeBundle\Entity\Cart", mappedBy="product")
     */
    private $carts;

    public function __construct()
    {
        $this->status       = static::STATUS_PUBLISH;
        $this->supplyId     = 0;
        $this->sku1        = '颜色';
        $this->sku2        = '尺码';
        $this->currentPrice  = 0;
        $this->stock        = 100;
        $this->scrap        = 0;
        $this->chosen        = false;
        $this->created      = new \Datetime();
        $this->modified     = new \Datetime();
        $this->skus         = new ArrayCollection();
        $this->supplies     = new ArrayCollection();
        $this->images       = new ArrayCollection();
        $this->orders       = new ArrayCollection();
        $this->carts       = new ArrayCollection();
    }

    /**
     * @ORM\PreUpdate
     */
    public function setPerUpdate()
    {
        $this->modified = new \DateTime();
    }

    public function __toString()
    {
        return $this->title;
    }

    public static function getStatusList()
    {
        return array(
            static::STATUS_STOCK  => '仓库',
            static::STATUS_PUBLISH  => '发布'
        );
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return Product
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set categoryId
     *
     * @param integer $categoryId
     * @return Product
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;

        return $this;
    }

    /**
     * Get categoryId
     *
     * @return integer 
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * Set brandId
     *
     * @param integer $brandId
     * @return Product
     */
    public function setBrandId($brandId)
    {
        $this->brandId = $brandId;

        return $this;
    }

    /**
     * Get brandId
     *
     * @return integer 
     */
    public function getBrandId()
    {
        return $this->brandId;
    }

    /**
     * Set supplyId
     *
     * @param integer $supplyId
     * @return Product
     */
    public function setSupplyId($supplyId)
    {
        $this->supplyId = $supplyId;

        return $this;
    }

    /**
     * Get supplyId
     *
     * @return integer 
     */
    public function getSupplyId()
    {
        return $this->supplyId;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Product
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set icon
     *
     * @param string $icon
     * @return Product
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string 
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set status
     *
     * @param smallint $status
     * @return Product
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return smallint 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set chosen
     *
     * @param boolean $chosen
     * @return Product
     */
    public function setChosen($chosen)
    {
        $this->chosen = $chosen;

        return $this;
    }

    /**
     * Get chosen
     *
     * @return boolean 
     */
    public function getChosen()
    {
        return $this->chosen;
    }

    /**
     * Set sellingPrice
     *
     * @param string $sellingPrice
     * @return Product
     */
    public function setSellingPrice($sellingPrice)
    {
        $this->sellingPrice = $sellingPrice;

        return $this;
    }

    /**
     * Get sellingPrice
     *
     * @return string 
     */
    public function getSellingPrice()
    {
        return $this->sellingPrice;
    }

    /**
     * Set currentPrice
     *
     * @param string $currentPrice
     * @return Product
     */
    public function setCurrentPrice($currentPrice)
    {
        $this->currentPrice = $currentPrice;

        return $this;
    }

    /**
     * Get currentPrice
     *
     * @return string 
     */
    public function getCurrentPrice()
    {
        return $this->currentPrice;
    }

    /**
     * Set buiingPrice
     *
     * @param string $buiingPrice
     * @return Product
     */
    public function setBuiingPrice($buiingPrice)
    {
        $this->buiingPrice = $buiingPrice;

        return $this;
    }

    /**
     * Get buiingPrice
     *
     * @return string 
     */
    public function getBuiingPrice()
    {
        return $this->buiingPrice;
    }

    /**
     * Set stock
     *
     * @param integer $stock
     * @return Product
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return integer 
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set scrap
     *
     * @param integer $scrap
     * @return Product
     */
    public function setScrap($scrap)
    {
        $this->scrap = $scrap;

        return $this;
    }

    /**
     * Get scrap
     *
     * @return integer 
     */
    public function getScrap()
    {
        return $this->scrap;
    }

    /**
     * Set sku1
     *
     * @param string $sku1
     * @return Product
     */
    public function setSku1($sku1)
    {
        $this->sku1 = $sku1;

        return $this;
    }

    /**
     * Get sku1
     *
     * @return string 
     */
    public function getSku1()
    {
        return $this->sku1;
    }

    /**
     * Set sku2
     *
     * @param string $sku2
     * @return Product
     */
    public function setSku2($sku2)
    {
        $this->sku2 = $sku2;

        return $this;
    }

    /**
     * Get sku2
     *
     * @return string 
     */
    public function getSku2()
    {
        return $this->sku2;
    }

    /**
     * Set barcode
     *
     * @param string $barcode
     * @return Product
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;

        return $this;
    }

    /**
     * Get barcode
     *
     * @return string 
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * Set metaTitle
     *
     * @param string $metaTitle
     * @return Product
     */
    public function setMetaTitle($metaTitle)
    {
        $this->metaTitle = $metaTitle;

        return $this;
    }

    /**
     * Get metaTitle
     *
     * @return string 
     */
    public function getMetaTitle()
    {
        return $this->metaTitle;
    }

    /**
     * Set keywords
     *
     * @param string $keywords
     * @return Product
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get keywords
     *
     * @return string 
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Product
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Product
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Product
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set category
     *
     * @param \CommonBundle\Entity\Category $category
     * @return Product
     */
    public function setCategory(\CommonBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \CommonBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set brand
     *
     * @param \ProductBundle\Entity\Brand $brand
     * @return Product
     */
    public function setBrand(\ProductBundle\Entity\Brand $brand = null)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand
     *
     * @return \ProductBundle\Entity\Brand 
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Add supplies
     *
     * @param \ProductBundle\Entity\Supply $supplies
     * @return Product
     */
    public function addSupply(\ProductBundle\Entity\Supply $supplies)
    {
        $this->supplies[] = $supplies;

        return $this;
    }

    /**
     * Remove supplies
     *
     * @param \ProductBundle\Entity\Supply $supplies
     */
    public function removeSupply(\ProductBundle\Entity\Supply $supplies)
    {
        $this->supplies->removeElement($supplies);
    }

    /**
     * Get supplies
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSupplies()
    {
        return $this->supplies;
    }

    /**
     * Add skus
     *
     * @param \ProductBundle\Entity\Sku $skus
     * @return Product
     */
    public function addSkus(\ProductBundle\Entity\Sku $skus)
    {
        $this->skus[] = $skus;

        return $this;
    }

    /**
     * Remove skus
     *
     * @param \ProductBundle\Entity\Sku $skus
     */
    public function removeSkus(\ProductBundle\Entity\Sku $skus)
    {
        $this->skus->removeElement($skus);
    }

    /**
     * Get skus
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSkus()
    {
        return $this->skus;
    }

    /**
     * Add images
     *
     * @param \ProductBundle\Entity\ProductImage $images
     * @return Product
     */
    public function addImage(\ProductBundle\Entity\ProductImage $images)
    {
        $this->images[] = $images;

        return $this;
    }

    /**
     * Remove images
     *
     * @param \ProductBundle\Entity\ProductImage $images
     */
    public function removeImage(\ProductBundle\Entity\ProductImage $images)
    {
        $this->images->removeElement($images);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Add orders
     *
     * @param \TradeBundle\Entity\Order $orders
     * @return Product
     */
    public function addOrder(\TradeBundle\Entity\Order $orders)
    {
        $this->orders[] = $orders;

        return $this;
    }

    /**
     * Remove orders
     *
     * @param \TradeBundle\Entity\Order $orders
     */
    public function removeOrder(\TradeBundle\Entity\Order $orders)
    {
        $this->orders->removeElement($orders);
    }

    /**
     * Get orders
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Set user
     *
     * @param \AccountBundle\Entity\User $user
     * @return Product
     */
    public function setUser(\AccountBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AccountBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add carts
     *
     * @param \TradeBundle\Entity\Cart $carts
     * @return Product
     */
    public function addCart(\TradeBundle\Entity\Cart $carts)
    {
        $this->carts[] = $carts;

        return $this;
    }

    /**
     * Remove carts
     *
     * @param \TradeBundle\Entity\Cart $carts
     */
    public function removeCart(\TradeBundle\Entity\Cart $carts)
    {
        $this->carts->removeElement($carts);
    }

    /**
     * Get carts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCarts()
    {
        return $this->carts;
    }
}
