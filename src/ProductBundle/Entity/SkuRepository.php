<?php

namespace ProductBundle\Entity;

use CoreBundle\Doctrine\EntityRepository;

/**
 * Sku Repository
 */
class SkuRepository extends EntityRepository
{
    /**
     * {@inheritdoc}
     */
    public function findSkuBy(array $criteria)
    {
        return $this->findOneBy($criteria);
    }

    /**
     * {@inheritdoc}
     */
    public function findSkuByProductId($productId)
    {
        $qb = $this->createQueryBuilder('s')
            ->andWhere('s.enabled = true')
            ->andWhere('s.productId = :productId')
            ->addOrderBy('m.created', 'DESC')
            ->setParameter('productId', $productId);

        return $qb;
    }

    /**
     * {@inheritdoc}
     */
    public function findSkuByCategory($categoryIds)
    {
        if (!is_array($categoryIds)) {
            $categoryIds = explode(',', $categoryIds);
        }

        $qb = $this->createQueryBuilder('m');
        $qb->Where('m.enabled = true')
            ->andWhere($qb->expr()->in('m.categoryId', ':categoryId'))
            ->addOrderBy('m.created', 'DESC')
            ->setParameter('categoryId', $categoryIds);

        return $qb;
    }

    /**
     * {@inheritdoc}
     */
    public function findSkuBySkuId($productId, $sku1, $sku2 = null)
    {
        $qb = $this->createQueryBuilder('s')
            ->where('s.enabled = true')
            ->andWhere('s.productId = :productId')
            ->andWhere('s.sku1 = :sku1')
            ->setParameter('productId', $productId)
            ->setParameter('sku1', $sku1);

        if ($sku2) {
            $qb->andWhere('s.sku2 = :sku2')
                ->setParameter('sku2', $sku2);
        }

        $result = $qb->getQuery()->getResult();

        return $result ? array_shift($result) : $result;
    }
}
