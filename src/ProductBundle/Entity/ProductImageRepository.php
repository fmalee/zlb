<?php

namespace ProductBundle\Entity;

use CoreBundle\Doctrine\EntityRepository;

/**
 * Product Image Repository
 */
class ProductImageRepository extends EntityRepository
{
    /**
     * {@inheritdoc}
     */
    public function findSkuBy(array $criteria)
    {
        return $this->findOneBy($criteria);
    }

    /**
     * {@inheritdoc}
     */
    public function findSkuByProductId($productId)
    {
        $qb = $this->createQueryBuilder('s')
            ->andWhere('s.enabled = true')
            ->andWhere('s.productId = :productId')
            ->addOrderBy('m.created', 'DESC')
            ->setParameter('productId', $productId);

        return $qb;
    }

    /**
     * {@inheritdoc}
     */
    public function findSkuByCategory($categoryIds)
    {
        if (!is_array($categoryIds)) {
            $categoryIds = explode(',', $categoryIds);
        }

        $qb = $this->createQueryBuilder('m');
        $qb->Where('m.enabled = true')
            ->andWhere($qb->expr()->in('m.categoryId', ':categoryId'))
            ->addOrderBy('m.created', 'DESC')
            ->setParameter('categoryId', $categoryIds);

        return $qb;
    }
}
