<?php

namespace ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Sku
 *
 * @ORM\Entity(repositoryClass="ProductBundle\Entity\SkuRepository")
 * @ORM\Table(name="mc_sku", indexes={@ORM\Index(name="name", columns={"name"})})
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(
 *     fields={"name"},
 *     message="该标签名已存在",
 *     groups={"Edit"}
 * )
 */
class Sku
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="product_id", type="integer")
     */
    private $productId;

    /**
     * @var string
     *
     * @ORM\Column(name="supply_id", type="integer")
     */
    private $supplyId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=30, unique=true)
     *
     * @Assert\Length(
     *      max = "30",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     * @Assert\Regex(
     *      pattern="/^[^-_]+[a-z0-9-_]+[^-_]$/",
     *      message="标签名只能包含字母、数字、_或减号，不能以_或减号开头或结尾。",
     *      groups={"Registration"}
     * )
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="icon", type="string", length=255, nullable=true)
     */
    private $icon;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean")
     */
    private $enabled;

    /**
     * @var string
     *
     * @ORM\Column(name="sku_1", type="string", length=20, nullable=true)
     *
     * @Assert\Length(
     *      max = "20",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     */
    private $sku1;

    /**
     * @var string
     *
     * @ORM\Column(name="sku_2", type="string", length=20, nullable=true)
     *
     * @Assert\Length(
     *      max = "20",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     */
    private $sku2;

    /**
     * @var decimal
     *
     * @ORM\Column(name="selling_price", type="decimal", scale=2)
     */
    protected $sellingPrice;

    /**
     * @var decimal
     *
     * @ORM\Column(name="current_price", type="decimal", scale=2)
     */
    protected $currentPrice;

    /**
     * @var decimal
     *
     * @ORM\Column(name="buiing_price", type="decimal", scale=2)
     */
    protected $buiingPrice;

    /**
     * @var integer
     *
     * @ORM\Column(name="stock", type="integer")
     */
    protected $stock;

    /**
     * @var integer
     *
     * @ORM\Column(name="scrap", type="integer")
     */
    protected $scrap;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="modified", type="datetime")
     */
    private $modified;

    /**
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="skus")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity="Supply", inversedBy="skus")
     * @ORM\JoinColumn(name="supply_id", referencedColumnName="id")
     */
    private $supply;

    /**
     * @ORM\OneToMany(targetEntity="TradeBundle\Entity\Order", mappedBy="sku")
     */
    private $orders;

    /**
     * @ORM\OneToMany(targetEntity="TradeBundle\Entity\Cart", mappedBy="product")
     */
    private $carts;

    public function __construct()
    {
        $this->enabled     = true;
        $this->currentPrice  = 0;
        $this->stock        = 100;
        $this->scrap        = 0;
        $this->created     = new \Datetime();
        $this->modified    = new \Datetime();
        $this->brands      = new ArrayCollection();
        $this->orders      = new ArrayCollection();
    }

    /**
     * @ORM\PreUpdate
     */
    public function setPerUpdate()
    {
        $this->modified = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set productId
     *
     * @param integer $productId
     * @return Sku
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * Get productId
     *
     * @return integer 
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * Set supplyId
     *
     * @param integer $supplyId
     * @return Sku
     */
    public function setSupplyId($supplyId)
    {
        $this->supplyId = $supplyId;

        return $this;
    }

    /**
     * Get supplyId
     *
     * @return integer 
     */
    public function getSupplyId()
    {
        return $this->supplyId;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Sku
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set icon
     *
     * @param string $icon
     * @return Sku
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string 
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return Sku
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set sku1
     *
     * @param string $sku1
     * @return Sku
     */
    public function setSku1($sku1)
    {
        $this->sku1 = $sku1;

        return $this;
    }

    /**
     * Get sku1
     *
     * @return string
     */
    public function getSku1()
    {
        return $this->sku1;
    }

    /**
     * Set sku2
     *
     * @param string $sku2
     * @return Sku
     */
    public function setSku2($sku2)
    {
        $this->sku2 = $sku2;

        return $this;
    }

    /**
     * Get sku2
     *
     * @return string
     */
    public function getSku2()
    {
        return $this->sku2;
    }

    /**
     * Set sellingPrice
     *
     * @param string $sellingPrice
     * @return Sku
     */
    public function setSellingPrice($sellingPrice)
    {
        $this->sellingPrice = $sellingPrice;

        return $this;
    }

    /**
     * Get sellingPrice
     *
     * @return string 
     */
    public function getSellingPrice()
    {
        return $this->sellingPrice;
    }

    /**
     * Set currentPrice
     *
     * @param string $currentPrice
     * @return Sku
     */
    public function setCurrentPrice($currentPrice)
    {
        $this->currentPrice = $currentPrice;

        return $this;
    }

    /**
     * Get currentPrice
     *
     * @return string 
     */
    public function getCurrentPrice()
    {
        return $this->currentPrice;
    }

    /**
     * Set buiingPrice
     *
     * @param string $buiingPrice
     * @return Sku
     */
    public function setBuiingPrice($buiingPrice)
    {
        $this->buiingPrice = $buiingPrice;

        return $this;
    }

    /**
     * Get buiingPrice
     *
     * @return string 
     */
    public function getBuiingPrice()
    {
        return $this->buiingPrice;
    }

    /**
     * Set stock
     *
     * @param integer $stock
     * @return Sku
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return integer 
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set scrap
     *
     * @param integer $scrap
     * @return Sku
     */
    public function setScrap($scrap)
    {
        $this->scrap = $scrap;

        return $this;
    }

    /**
     * Get scrap
     *
     * @return integer 
     */
    public function getScrap()
    {
        return $this->scrap;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Sku
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Sku
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set product
     *
     * @param \ProductBundle\Entity\Product $product
     * @return Sku
     */
    public function setProduct(\ProductBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \ProductBundle\Entity\Product 
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set supply
     *
     * @param \ProductBundle\Entity\Supply $supply
     * @return Sku
     */
    public function setSupply(\ProductBundle\Entity\Supply $supply = null)
    {
        $this->supply = $supply;

        return $this;
    }

    /**
     * Get supply
     *
     * @return \ProductBundle\Entity\Supply 
     */
    public function getSupply()
    {
        return $this->supply;
    }


    /**
     * Add orders
     *
     * @param \TradeBundle\Entity\Order $orders
     * @return Sku
     */
    public function addOrder(\TradeBundle\Entity\Order $orders)
    {
        $this->orders[] = $orders;

        return $this;
    }

    /**
     * Remove orders
     *
     * @param \TradeBundle\Entity\Order $orders
     */
    public function removeOrder(\TradeBundle\Entity\Order $orders)
    {
        $this->orders->removeElement($orders);
    }

    /**
     * Get orders
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Add carts
     *
     * @param \TradeBundle\Entity\Cart $carts
     * @return Sku
     */
    public function addCart(\TradeBundle\Entity\Cart $carts)
    {
        $this->carts[] = $carts;

        return $this;
    }

    /**
     * Remove carts
     *
     * @param \TradeBundle\Entity\Cart $carts
     */
    public function removeCart(\TradeBundle\Entity\Cart $carts)
    {
        $this->carts->removeElement($carts);
    }

    /**
     * Get carts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCarts()
    {
        return $this->carts;
    }
}
