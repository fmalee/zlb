<?php

namespace ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Brand
 *
 * @ORM\Entity(repositoryClass="ProductBundle\Entity\BrandRepository")
 * @ORM\Table(name="mc_brand", indexes={@ORM\Index(name="name", columns={"name"})})
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(
 *     fields={"name"},
 *     message="该标签名已存在",
 *     groups={"Edit"}
 * )
 */
class Brand
{
    const STATUS_TRASH      = -1;
    const STATUS_DRAFT      = 0;
    const STATUS_PUBLISH    = 1;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="user_id", type="integer")
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="mall_id", type="integer")
     */
    private $mallId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=30, unique=true)
     *
     * @Assert\Length(
     *      max = "30",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     * @Assert\Regex(
     *      pattern="/^[^-_]+[a-z0-9-_]+[^-_]$/",
     *      message="标签名只能包含字母、数字、_或减号，不能以_或减号开头或结尾。",
     *      groups={"Registration"}
     * )
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=50)
     *
     * @Assert\NotBlank(message = "标题不能为空", groups={"Edit"})
     * @Assert\Length(
     *      max = "50",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     */
    private $title;

    /**
     * @var integer
     *
     * @ORM\Column(name="icon", type="string", length=255, nullable=true)
     */
    private $icon;

    /**
     * @var smallint
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;

    /**
     * @var boolean
     *
     * @ORM\Column(name="showed", type="boolean")
     */
    private $showed;

    /**
     * @var boolean
     *
     * @ORM\Column(name="chosen", type="boolean")
     */
    private $chosen;

    /**
     * @var string
     *
     * @ORM\Column(name="contact", type="string", length=20, nullable=true)
     *
     * @Assert\Length(
     *      max = "20",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     */
    private $contact;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=30, nullable=true)
     *
     * @Assert\Length(
     *      max = "30",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="homepage", type="string", length=128, nullable=true)
     *
     * @Assert\Length(
     *      max = "128",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     */
    private $homePage;

    /**
     * @var string
     *
     * @ORM\Column(name="wangwang", type="string", length=30, nullable=true)
     *
     * @Assert\Length(
     *      max = "30",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     */
    private $wangwang;

    /**
     * @var string
     *
     * @ORM\Column(name="qq", type="string", length=30, nullable=true)
     *
     * @Assert\Length(
     *      max = "30",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     */
    private $qq;

    /**
     * @var string
     *
     * @ORM\Column(name="data_packet", type="string", length=128, nullable=true)
     *
     * @Assert\Length(
     *      max = "128",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     */
    private $dataPacket;

    /**
     * @var string
     *
     * @ORM\Column(name="floor", type="string", length=20)
     *
     * @Assert\Length(
     *      max = "20",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     */
    private $floor;

    /**
     * @var string
     *
     * @ORM\Column(name="house_number", type="string", length=100)
     *
     * @Assert\Length(
     *      max = "100",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     */
    private $houseNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=200, nullable=true)
     *
     * @Assert\Length(
     *      max = "200",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_title", type="string", length=50, nullable=true)
     *
     * @Assert\Length(
     *      max = "50",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     */
    private $metaTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="keywords", type="string", length=255, nullable=true)
     *
     * @Assert\Length(
     *      max = "255",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     */
    private $keywords;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     *
     * @Assert\Length(
     *      max = "255",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     *
     * @Assert\NotBlank(message = "内容不能为空", groups={"Edit"})
     */
    private $content;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="modified", type="datetime")
     */
    private $modified;

    /**
     * @ORM\ManyToOne(targetEntity="Mall", inversedBy="brands")
     * @ORM\JoinColumn(name="mall_id", referencedColumnName="id")
     */
    private $mall;

    /**
     * @ORM\ManyToOne(targetEntity="\AccountBundle\Entity\User", inversedBy="products")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="Product", mappedBy="brand")
     */
    private $products;

    /**
     * @ORM\OneToMany(targetEntity="Supply", mappedBy="brand")
     */
    private $supplies;

    public function __construct()
    {
        $this->status     = static::STATUS_PUBLISH;
        $this->showed     = true;
        $this->chosen     = false;
        $this->created     = new \Datetime();
        $this->modified    = new \Datetime();
        $this->products    = new ArrayCollection();
        $this->supplies    = new ArrayCollection();
    }

    /**
     * @ORM\PreUpdate
     */
    public function setPerUpdate()
    {
        $this->modified = new \DateTime();
    }

    public function __toString()
    {
        return $this->title;
    }

    public static function getStatusList()
    {
        return array(
            static::STATUS_DRAFT  => '草稿箱',
            static::STATUS_PUBLISH  => '发布'
        );
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return Brand
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set mallId
     *
     * @param integer $mallId
     * @return Article
     */
    public function setMallId($mallId)
    {
        $this->mallId = $mallId;

        return $this;
    }

    /**
     * Get mallId
     *
     * @return integer
     */
    public function getMallId()
    {
        return $this->mallId;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Brand
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Brand
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set icon
     *
     * @param string $icon
     * @return Brand
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string 
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set status
     *
     * @param smallint $status
     * @return Brand
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return smallint 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set showed
     *
     * @param boolean $showed
     * @return Brand
     */
    public function setShowed($showed)
    {
        $this->showed = $showed;

        return $this;
    }

    /**
     * Get showed
     *
     * @return boolean
     */
    public function getShowed()
    {
        return $this->showed;
    }

    /**
     * Set chosen
     *
     * @param boolean $chosen
     * @return Brand
     */
    public function setChosen($chosen)
    {
        $this->chosen = $chosen;

        return $this;
    }

    /**
     * Get chosen
     *
     * @return boolean
     */
    public function getChosen()
    {
        return $this->chosen;
    }

    /**
     * Set contact
     *
     * @param string $contact
     * @return Brand
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return string
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Brand
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set wangwang
     *
     * @param string $wangwang
     * @return Brand
     */
    public function setWangwang($wangwang)
    {
        $this->wangwang = $wangwang;

        return $this;
    }

    /**
     * Get wangwang
     *
     * @return string
     */
    public function getWangwang()
    {
        return $this->wangwang;
    }

    /**
     * Set qq
     *
     * @param string $qq
     * @return Brand
     */
    public function setQq($qq)
    {
        $this->qq = $qq;

        return $this;
    }

    /**
     * Get qq
     *
     * @return string
     */
    public function getQq()
    {
        return $this->qq;
    }

    /**
     * Set dataPacket
     *
     * @param string $dataPacket
     * @return Brand
     */
    public function setDataPacket($dataPacket)
    {
        $this->dataPacket = $dataPacket;

        return $this;
    }

    /**
     * Get dataPacket
     *
     * @return string
     */
    public function getDataPacket()
    {
        return $this->dataPacket;
    }

    /**
     * Set homePage
     *
     * @param string $homePage
     * @return Brand
     */
    public function setHomePage($homePage)
    {
        $this->homePage = $homePage;

        return $this;
    }

    /**
     * Get homePage
     *
     * @return string 
     */
    public function getHomePage()
    {
        return $this->homePage;
    }

    /**
     * Set floor
     *
     * @param string $floor
     * @return Brand
     */
    public function setFloor($floor)
    {
        $this->floor = $floor;

        return $this;
    }

    /**
     * Get floor
     *
     * @return string 
     */
    public function getFloor()
    {
        return $this->floor;
    }

    /**
     * Set houseNumber
     *
     * @param string $houseNumber
     * @return Brand
     */
    public function setHouseNumber($houseNumber)
    {
        $this->houseNumber = $houseNumber;

        return $this;
    }

    /**
     * Get houseNumber
     *
     * @return string 
     */
    public function getHouseNumber()
    {
        return $this->houseNumber;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Brand
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set metaTitle
     *
     * @param string $metaTitle
     * @return Brand
     */
    public function setMetaTitle($metaTitle)
    {
        $this->metaTitle = $metaTitle;

        return $this;
    }

    /**
     * Get metaTitle
     *
     * @return string 
     */
    public function getMetaTitle()
    {
        return $this->metaTitle;
    }

    /**
     * Set keywords
     *
     * @param string $keywords
     * @return Brand
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get keywords
     *
     * @return string 
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Brand
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Brand
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Brand
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Brand
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set mall
     *
     * @param \ProductBundle\Entity\Mall $mall
     * @return Brand
     */
    public function setMall(Mall $mall = null)
    {
        $this->mall = $mall;

        return $this;
    }

    /**
     * Get mall
     *
     * @return \ProductBundle\Entity\Mall 
     */
    public function getMall()
    {
        return $this->mall;
    }


    /**
     * Add products
     *
     * @param \ProductBundle\Entity\Product $products
     * @return Brand
     */
    public function addProduct(\ProductBundle\Entity\Product $products)
    {
        $this->products[] = $products;

        return $this;
    }

    /**
     * Remove products
     *
     * @param \ProductBundle\Entity\Product $products
     */
    public function removeProduct(\ProductBundle\Entity\Product $products)
    {
        $this->products->removeElement($products);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Add supplies
     *
     * @param \ProductBundle\Entity\Supply $supplies
     * @return Brand
     */
    public function addSupply(\ProductBundle\Entity\Supply $supplies)
    {
        $this->supplies[] = $supplies;

        return $this;
    }

    /**
     * Remove supplies
     *
     * @param \ProductBundle\Entity\Supply $supplies
     */
    public function removeSupply(\ProductBundle\Entity\Supply $supplies)
    {
        $this->supplies->removeElement($supplies);
    }

    /**
     * Get supplies
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSupplies()
    {
        return $this->supplies;
    }

    /**
     * Set user
     *
     * @param \ProductBundle\Entity\User $user
     * @return Brand
     */
    public function setUser(\AccountBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return AccountBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
