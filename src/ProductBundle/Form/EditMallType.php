<?php

namespace ProductBundle\Form;

use ProductBundle\Entity\Mall;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EditMallType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('categoryId', 'hidden')
            ->add('areaId', 'hidden')
            ->add('name', 'text', array(
                'label'       => '标签名',
                'attr' => array(
                    'placeholder' => '一个唯一的标识，如 sjq',
                )
            ))
            ->add('title', 'text', array(
                'label'       => '名称',
                'attr' => array(
                    'placeholder' => '商城名称, 如 杭州四季星座',
                )
            ))
            ->add('homePage', 'url', array(
                'label'       => '相关网页',
                'required' => false,
                'attr' => array(
                    'placeholder' => '相关网页',
                )
            ))
            ->add('address', 'text', array(
                'label'       => '营业地址',
                'required' => false
            ))
            ->add('metaTitle', 'text', array(
                'label'       => '网页标题',
                'required' => false,
                'attr' => array(
                    'placeholder' => '用于SEO优化',
                )
            ))
            ->add('keywords', 'text', array(
                'label'       => '网页关键字',
                'required' => false,
                'attr' => array(
                    'placeholder' => '用于SEO优化',
                )
            ))
            ->add('status', 'choice', array(
                'label'       => '状态',
                'choices' => Mall::getStatusList(),
                'expanded'    => true
            ))
            ->add('description', 'textarea', array(
                'label'    => '商城简介',
                'required' => false,
                'attr'     => array(
                    'rows'  => 4,
                    'cols'  => 55,
                    'placeholder' => '用于SEO优化',
                )
            ))
            ->add('content', 'textarea', array(
                'label'    => '商城介绍',
                'required' => false,
                'attr'     => array(
                    'rows'  => 15,
                    'cols'  => 55,
                    'class' => 'form-control wysihtml5'
                )
            ))
            ->add('save', 'submit', array('label' => '保存'));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'        => 'ProductBundle\Entity\Mall',
            'validation_groups' => array('Edit')
        ));
    }

    public function getName()
    {
        return 'mall';
    }
}