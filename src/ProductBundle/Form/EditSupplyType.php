<?php

namespace ProductBundle\Form;

use ProductBundle\Entity\Brand;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use ProductBundle\Entity\Supply;

class EditSupplyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('brandId', 'hidden')
            ->add('categoryId', 'hidden')
            ->add('productId', 'hidden')
            ->add('name', 'text', array(
                'label'       => '货号',
                'required' => false,
                'attr' => array(
                    'placeholder' => '商家的产品货号',
                )
            ))
/*            ->add('icon', 'url', array(
                'label'       => '产品图片',
                'required' => false
            ))*/
            ->add('price', 'money', array(
                'label'       => '产品进价',
                'required' => false
            ))
            ->add('stock', 'number', array(
                'label'       => '库存',
                'required' => false
            ))
            ->add('resource', 'text', array(
                'label'       => '数据来源',
                'required' => false
            ))
            ->add('status', 'choice', array(
                'label'       => '状态',
                'choices' => Supply::getStatusList(),
                'expanded'    => true
            ))
            ->add('memo', 'textarea', array(
                'label'    => '备注',
                'required' => false,
                'attr'     => array(
                    'rows'  => 2,
                    'cols'  => 55
                )
            ))
            ->add('save', 'submit', array('label' => '保存'));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'        => 'ProductBundle\Entity\Supply',
            'validation_groups' => array('Edit')
        ));
    }

    public function getName()
    {
        return 'supply';
    }
}