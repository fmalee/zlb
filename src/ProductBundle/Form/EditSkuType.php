<?php

namespace ProductBundle\Form;

use ProductBundle\Entity\Brand;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EditSkuType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('supplyId', 'hidden', array(
                'label'       => '供应商'
            ))
            ->add('name', 'text', array(
                'label'       => '标签',
                'required' => false,
                'attr' => array(
                    'placeholder' => '商品的子标签，如E40-1',
                )
            ))
/*            ->add('icon', 'url', array(
                'label'       => '商品图片',
                'required' => false
            ))*/
            ->add('sku_1', 'text', array(
                'label'       => '商品属性1',
                'attr' => array(
                    'placeholder' => '商品的规格属性，如尺码',
                )
            ))
            ->add('sku_2', 'text', array(
                'label'       => '商品属性2',
                'required' => false,
                'attr' => array(
                    'placeholder' => '商品的规格属性，如颜色',
                )
            ))
            ->add('sellingPrice', 'number', array(
                'label'       => '售价',
            ))
            ->add('currentPrice', 'number', array(
                'label'       => '促销价',
                'required' => false
            ))
            ->add('buiingPrice', 'number', array(
                'label'       => '进价',
            ))
            ->add('scrap', 'number', array(
                'label'       => '次品数量',
                'required' => false
            ))
            ->add('stock', 'number', array(
                'label'       => '库存数量',
                'required' => false
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'        => 'ProductBundle\Entity\Sku',
            'validation_groups' => array('Edit')
        ));
    }

    public function getName()
    {
        return 'sku';
    }
}