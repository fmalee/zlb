<?php

namespace ProductBundle\Form;

use ProductBundle\Entity\Brand;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EditBrandType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('mallId', 'hidden')
            ->add('title', 'text', array(
                'label'       => '名称',
                'attr' => array(
                    'placeholder' => '品牌名称, 如 杭州四季星座',
                )
            ))
            ->add('name', 'text', array(
                'label'       => '标签名',
                'required' => false,
                'attr' => array(
                    'placeholder' => '一个唯一的标识，如 sjq',
                )
            ))
            ->add('phone', 'text', array(
                'label'       => '联系电话',
                'required' => false
            ))
            ->add('wangwang', 'text', array(
                'label'       => '旺旺',
                'required' => false
            ))
            ->add('qq', 'text', array(
                'label'       => 'QQ',
                'required' => false
            ))
            ->add('homePage', 'url', array(
                'label'       => '相关网页',
                'required' => false,
                'attr' => array(
                    'placeholder' => '相关网页',
                )
            ))
            ->add('dataPacket', 'url', array(
                'label'       => '数据包地址',
                'required' => false,
                'attr' => array(
                    'placeholder' => '相关网页',
                )
            ))
            ->add('address', 'text', array(
                'label'       => '营业地址',
                'required' => false
            ))
            ->add('floor', 'text', array(
                'label'       => '楼层',
                'attr' => array(
                    'placeholder' => '店铺所在楼层，如 2',
                )
            ))
            ->add('houseNumber', 'text', array(
                'label'       => '门牌号',
                'attr' => array(
                    'placeholder' => '店铺的门牌，如 8203',
                )
            ))
            ->add('metaTitle', 'text', array(
                'label'       => '网页标题',
                'required' => false,
                'attr' => array(
                    'placeholder' => '用于SEO优化',
                )
            ))
            ->add('keywords', 'text', array(
                'label'       => '网页关键字',
                'required' => false,
                'attr' => array(
                    'placeholder' => '用于SEO优化',
                )
            ))
            ->add('status', 'choice', array(
                'label'       => '状态',
                'choices' => Brand::getStatusList(),
                'expanded'    => true
            ))
            ->add('description', 'textarea', array(
                'label'    => '品牌简介',
                'required' => false,
                'attr'     => array(
                    'rows'  => 4,
                    'cols'  => 55,
                    'placeholder' => '用于SEO优化',
                )
            ))
            ->add('content', 'textarea', array(
                'label'    => '品牌介绍',
                'required' => false,
                'attr'     => array(
                    'rows'  => 15,
                    'cols'  => 55,
                    'class' => 'form-control wysihtml5'
                )
            ))
            ->add('save', 'submit', array('label' => '保存'));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'        => 'ProductBundle\Entity\Brand',
            'validation_groups' => array('Edit')
        ));
    }

    public function getName()
    {
        return 'brand';
    }
}