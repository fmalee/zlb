<?php

namespace ProductBundle\Form;

use ProductBundle\Entity\Brand;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use ProductBundle\Entity\Product;

class EditProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('categoryId', 'hidden')
            //->add('brandId', 'hidden')
            ->add('brandId', 'text', array(
                'label'       => '所属品牌',
                'attr' => array(
                    'class' => 'bigdrop',
                    'style' => 'width: 100%;',
                    'type' => 'hidden'
                )
            ))
            ->add('title', 'text', array(
                'label'       => '商品名称',
                'attr' => array(
                    'placeholder' => '产品名称, 如 短裤A40',
                )
            ))
            ->add('name', 'text', array(
                'label'       => '标签名',
                'attr' => array(
                    'placeholder' => '一个唯一的标识，即货号，如 A40',
                )
            ))
            ->add('model', 'text', array(
                'label'       => '商品货号',
                'mapped' => false,
                'attr' => array(
                    'placeholder' => '商家提供的货号，如 E4457',
                )
            ))
            ->add('resource', 'text', array(
                'label'       => '数据来源',
                'mapped' => false,
                'attr' => array(
                    'placeholder' => '填写数据包名称或是完整网址',
                )
            ))
/*            ->add('icon', 'url', array(
                'label'       => '产品图片',
                'required' => false
            ))*/
            ->add('sku_1', 'text', array(
                'label'       => '商品属性1',
                'attr' => array(
                    'placeholder' => '商品的规格属性，如尺码',
                )
            ))
            ->add('sku_2', 'text', array(
                'label'       => '商品属性2',
                'required' => false,
                'attr' => array(
                    'placeholder' => '商品的规格属性，如颜色',
                )
            ))
            ->add('sellingPrice', 'number', array(
                'label'       => '售价',
            ))
            ->add('currentPrice', 'number', array(
                'label'       => '促销价',
                'required' => false
            ))
            ->add('buiingPrice', 'number', array(
                'label'       => '进价',
            ))
            ->add('stock', 'number', array(
                'label'       => '库存数量',
                'required' => false
            ))
            ->add('scrap', 'number', array(
                'label'       => '次品数量',
                'required' => false
            ))
            ->add('barcode', 'text', array(
                'label'       => '识别码',
                'required' => false
            ))
            ->add('metaTitle', 'text', array(
                'label'       => '网页标题',
                'required' => false,
                'attr' => array(
                    'placeholder' => '用于SEO优化',
                )
            ))
            ->add('keywords', 'text', array(
                'label'       => '网页关键字',
                'required' => false,
                'attr' => array(
                    'placeholder' => '用于SEO优化',
                )
            ))
            ->add('status', 'choice', array(
                'label'       => '状态',
                'choices' => Product::getStatusList(),
                'expanded'    => true
            ))
            ->add('description', 'textarea', array(
                'label'    => '商品简介',
                'required' => false,
                'attr'     => array(
                    'rows'  => 4,
                    'cols'  => 55,
                    'placeholder' => '用于SEO优化',
                )
            ))
            ->add('content', 'textarea', array(
                'label'    => '商品说明',
                'required' => false,
                'attr'     => array(
                    'rows'  => 25,
                    'cols'  => 55,
                    'class' => 'form-control wysihtml5'
                )
            ))
            ->add('save', 'submit', array('label' => '保存'));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'        => 'ProductBundle\Entity\Product',
            'validation_groups' => array('Edit')
        ));
    }

    public function getName()
    {
        return 'product';
    }
}