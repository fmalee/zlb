<?php

namespace ProductBundle\Controller;

use CoreBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/mall")
 */
class MallController extends Controller
{
    /**
     * @Route("/", name="mall_home")
     * @Template()
     */
    public function indexAction()
    {
        $categorys = $this->getCategoryRepository()->getCategories(1);
        $this->dump($categorys);

        $categorys = $this->getCategoryRepository()->findByModule(1);
        //$this->dump($categorys);

        return array();
    }

    /**
     * @Route("/childs", name="mall_childs")
     * @Security("has_role('ROLE_USER')")
     */
    public function childsAction(Request $request)
    {
        $id = $request->query->get('id') ?: 0;
        $areas = $this->getAreaRepository()->findByParentId($id);

        $childs = array();
        if ($areas) {
            foreach ($areas as $key=>$value) {
                $childs[$key]['id'] = $value->getId();
                $childs[$key]['name'] = $value->getName();
            }

            return $this->ajaxReturn($childs);
        } else {
            $area = $this->getAreaRepository()->find($id);
            if ($area->getType() == 4) {
                $malls = $this->getMallRepository()->findByAreaId($id);
                //$this->dump($malls);
                if ($malls) {
                    foreach ($malls as $key=>$value) {
                        $childs[$key]['id'] = $value->getId();
                        $childs[$key]['name'] = $value->getTitle();
                    }

                    return $this->ajaxReturn($childs);
                }
            }
        }

        return $this->ajaxReturn('没有记录', false);
    }

    /**
     * @Route("/{cate}/{page}", name="mall_list", requirements={"cate" = "\w+", "page" = "\d+"}, defaults={"page" = 1})
     * @Template()
     */
    public function listAction($cate, $page)
    {
        $category = $this->getCategoryRepository()->findCategoryByName($cate);
        if (null == $category || $category->getModule() != 'mall') {
            throw $this->createNotFoundException('没有找到对应的栏目： ' . $cate);
        }

        $qb = $this->getMallRepository()->createQueryByStatus();

        $categories = $this->getCategories();
        if ($category->getParented()) {
            $categoryIds = explode(',', $category->getArrChild());

            $qb = $this->getMallRepository()->addQueryByCategories($qb, $categoryIds);
        } else {
            $qb = $this->getMallRepository()->findMallByCategoryId($category->getId());
        }

        $pagination = $this->paginate($qb, $page, 3);

        $this->setCategoryBreadcrumb($category->getId(), 'mall_list')->add('列表');

        $template = $category->getTemplateList() ?: 'ProductBundle:Mall:list.html.twig';

        return $this->render($template, array(
            'pagination' => $pagination,
            'category' => $category,
            'categories' => $categories
        ));
    }

    /**
     * @Route("/{cate}/detail/{id}.html", name="mall_name_detail", requirements={"cate" = "\w+", "id" = "\d+"})
     * @Route("/{cate}/{name}.html", name="mall_detail", requirements={"cate" = "\w+", "name" = "\w+"})
     * @Template()
     */
    public function detailAction($cate, $id = null, $name = null)
    {

        $category = $this->getCategoryRepository()->findOneByName($cate);
        if (null == $category || !$category->isEnabled() || $category->getModule() != 'mall') {
            throw $this->createNotFoundException('没有找到对应的栏目： ' . $cate);
        }
        $categories = $this->getCategories();

        $mall = $this->getMallRepository()->findOneByName($name);
        if (!$mall || $mall->getCategoryId() != $category->getId()) {
            throw $this->createNotFoundException('没有找到对应的文章');
        }

        $this->setCategoryBreadcrumb($category->getId(), 'mall_list')->add($mall->getTitle());

        return array(
            'mall' => $mall,
            'category' => $category,
            'categories' => $categories
        );
    }

    /**
     * @return MallRepository
     */
    private function getMallRepository()
    {
        return $this->get('product.entity.mall_repository');
    }

    /**
     * @return MallRepository
     */
    private function getAreaRepository()
    {
        return $this->get('common.entity.area_repository');
    }

}
