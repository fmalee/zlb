<?php

namespace ProductBundle\Controller;

use CoreBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * @Route("/brand")
 */
class BrandController extends Controller
{
    /**
     * @Route("/", name="brand_home")
     * @Template()
     */
    public function indexAction()
    {
        $categorys = $this->getCategoryRepository()->getCategories(1);
        $this->dump($categorys);

        $categorys = $this->getCategoryRepository()->findByModule(1);
        //$this->dump($categorys);

        return array();
    }

    /**
     * @Route("/{cate}/{page}", name="brand_list", requirements={"cate" = "\w+", "page" = "\d+"}, defaults={"page" = 1})
     * @Template()
     */
    public function listAction($cate, $page)
    {
        $category = $this->getCategoryRepository()->findCategoryByName($cate);
        if (null == $category || $category->getModule() != 'brand') {
            throw $this->createNotFoundException('没有找到对应的栏目： ' . $cate);
        }

        $categories = $this->getCategories();
        if ($category->getParented()) {
            $ids = trim($categories[$category->getId()]['arrChild'], ',');
            $qb = $this->getMallRepository()->findMallByCategory($ids);
        } else {
            $qb = $this->getMallRepository()->findMallByCategoryId($category->getId());
        }

        $pagination = $this->paginate($qb, $page, 3);
        //$this->dump($pagination);

        $this->setCategoryBreadcrumb($category->getId(), 'brand_list')->add('列表');

        $template = $category->getTemplateLists() ?: 'ProductBundle:Mall:list.html.twig';

        return $this->render($template, array(
            'pagination' => $pagination,
            'category' => $category,
            'categories' => $categories
        ));
    }

    /**
     * @Route("/mall/{cate}/{mall}/{name}.html", name="brand_detail", requirements={"cate" = "\w+", "name" = "\w+"})
     * @Template()
     */
    public function detailAction($cate, $mall = null, $name = null)
    {

        $category = $this->getCategoryRepository()->findOneByName($cate);
        if (null == $category || !$category->isEnabled() || $category->getModule() != 'mall') {
            throw $this->createNotFoundException('没有找到对应的栏目： ' . $cate);
        }
        $categoryId = $category->getId();
        $categories = $this->getCategories();

        $brand = $this->getBrandRepository()->findOneByName($name);
        $mall = $brand->getMall();
        //$mall = $this->getMallRepository()->findOneByName($mall);
        //$this->dump($mall);
        if (!$mall || $mall->getCategoryId() != $categoryId || !$brand) {
            throw $this->createNotFoundException('没有找到对应的文章');
        }

        $mallUrl = $this->generateUrl('mall_detail', array('cate' => $categories[$categoryId]['name'], 'name' => $mall->getName()));
        $this->setCategoryBreadcrumb($categoryId, 'mall_list')
            ->add($mall->getTitle(), $mallUrl)
            ->add($brand->getTitle());

        return array(
            'brand' => $brand,
            'mall' => $mall,
            'category' => $category,
            'categories' => $categories
        );
    }

    /**
     * @return BrandRepository
     */
    private function getBrandRepository()
    {
        return $this->get('product.entity.brand_repository');
    }

    /**
     * @return MallRepository
     */
    private function getMallRepository()
    {
        return $this->get('product.entity.mall_repository');
    }

}
