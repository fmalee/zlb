<?php

namespace ProductBundle\Controller;

use CoreBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * @Route("/product")
 */
class ProductController extends Controller
{
    /**
     * @Route("/", name="product_home")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }

    /**
     * @Route("/{id}.html", name="product_view", requirements={"id" = "\d+"})
     * @Template()
     */
    public function viewAction($id)
    {
        $categories = $this->getCategories();

        $product = $this->getProductRepository()->find($id);
        $category = $product->getCategory();
        $categoryId = $category->getId();
        $brand = $product->getBrand();
        //$mall = $this->getMallRepository()->findOneByName($mall);
        //$this->dump($mall);
        if (!$product || !$brand) {
            throw $this->createNotFoundException('没有找到对应的文章');
        }

        foreach ($product->getSkus() as $key => $sku) {
            $skus['sku1'][$sku->getSku1()] = $key;
            $skus['sku2'][$sku->getSku2()] = $key;
        }

        $this->setCategoryBreadcrumb($categoryId, 'product_list')
            ->add($product->getTitle());

        if ($this->getRequest()->get('preview')) {
            $template = 'ProductBundle:Product:preview.html.twig';
        } else {
            $template = 'ProductBundle:Product:view.html.twig';
        }
        return $this->render($template, array(
            'product' => $product,
            'skus' => $skus,
            'brand' => $brand,
            'category' => $category,
            'categories' => $categories
        ));
    }
    /**
     * @Route("/{cate}/{page}", name="product_list", requirements={"page" = "\d+"}, defaults={"page" = 1})
     */
    public function listAction($cate, $page)
    {
        $category = $this->getCategoryRepository()->findCategoryByName($cate);
        if (null == $category || $category->getModule() != 'brand') {
            throw $this->createNotFoundException('没有找到对应的栏目： ' . $cate);
        }

        $categoryId = $category->getId();
        $categories = $this->getCategories();
        if ($category->getParented()) {
            $ids = trim($categories[$categoryId]['arrChild'], ',');
            $qb = $this->getMallRepository()->findMallByCategory($ids);
        } else {
            $qb = $this->getMallRepository()->createQueryByCategoryId($category->getId());
        }

        $pagination = $this->paginate($qb, $page, 3);
        //$this->dump($pagination);

        $this->setCategoryBreadcrumb($categoryId, 'brand_list')->add('列表');

        $template = $category->getTemplateLists() ?: 'ProductBundle:Mall:list.html.twig';

        return $this->render($template, array(
            'pagination' => $pagination,
            'category' => $category,
            'categories' => $categories
        ));
    }


    /**
     * @Route("/{cate}/{name}.html", name="product_detail", requirements={"name" = "\w+"})
     */
    public function detailAction($cate, $mall = null, $name = null)
    {
        $category = $this->getCategoryRepository()->findOneByName($cate);
        if (null == $category || !$category->isEnabled() || $category->getModule() != 'product') {
            throw $this->createNotFoundException('没有找到对应的栏目： ' . $cate);
        }
        $categoryId = $category->getId();
        $categories = $this->getCategories();

        $product = $this->getProductRepository()->findOneByName($name);
        $brand = $product->getBrand();
        //$mall = $this->getMallRepository()->findOneByName($mall);
        //$this->dump($mall);
        if (!$product || $product->getCategoryId() != $categoryId || !$brand) {
            throw $this->createNotFoundException('没有找到对应的文章');
        }

        $this->setCategoryBreadcrumb($categoryId, 'product_list')
            ->add($product->getTitle());

        if ($this->getRequest()->get('preview')) {
            $template = 'ProductBundle:Product:preview.html.twig';
        } else {
            $template = 'ProductBundle:Product:detail.html.twig';
        }
        return $this->render($template, array(
            'brand' => $brand,
            'product' => $product,
            'category' => $category,
            'categories' => $categories
        ));
    }

    /**
     * @Route("/rate/{name}", name="product_rate", requirements={"name" = "\w+"})
     */
    public function rateAction($name = null)
    {
        return new Response('sadfsdafsdf');
    }

    /**
     * @return roductRepository
     */
    private function getProductRepository()
    {
        return $this->get('product.entity.product_repository');
    }

    /**
     * @return BrandRepository
     */
    private function getBrandRepository()
    {
        return $this->get('product.entity.brand_repository');
    }

    /**
     * @return MallRepository
     */
    private function getMallRepository()
    {
        return $this->get('product.entity.mall_repository');
    }

}
