<?php

namespace TaobaoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Taobao Refund
 *
 * @ORM\Table(name="tb_refund", indexes={@ORM\Index(name="status", columns={"status", "id"})})
 * @ORM\Entity
 */
class TbRefund
{
    /**
     * @var integer
     * Reference refund_id
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     * Reference tid
     *
     * @ORM\Column(name="trade_id", type="bigint")
     */
    private $tradeId;

    /**
     * @var integer
     * Reference oid
     *
     * @ORM\Column(name="order_id", type="bigint")
     */
    private $orderId;

    /**
     * @var string
     * Reference seller_nick
     *
     * @ORM\Column(name="seller_nick_name", type="string", length=50)
     */
    private $sellerNickName;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=50)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="good_status", type="string", length=50)
     */
    private $goodStatus;

    /**
     * @var float
     *
     * @ORM\Column(name="total_fee", type="float", precision=10, scale=0)
     */
    private $totalFee;

    /**
     * @var float
     *
     * @ORM\Column(name="payment", type="float", precision=10, scale=0)
     */
    private $payment;

    /**
     * @var float
     *
     * @ORM\Column(name="refund_fee", type="float", precision=10, scale=0)
     */
    private $refundFee;

    /**
     * @var float
     *
     * @ORM\Column(name="split_taobao_fee", type="float", precision=10, scale=0)
     */
    private $splitTaobaoFee;

    /**
     * @var float
     *
     * @ORM\Column(name="split_seller_fee", type="float", precision=10, scale=0)
     */
    private $splitSellerFee;

    /**
     * @var string
     *
     * @ORM\Column(name="reason", type="string", length=50)
     */
    private $reason;

    /**
     * @var string
     * Reference desc
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var boolean
     *
     * @ORM\Column(name="advance_status", type="boolean")
     */
    private $advanceStatus;

    /**
     * @var boolean
     *
     * @ORM\Column(name="cs_status", type="boolean")
     */
    private $csStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="company_name", type="string", length=30)
     */
    private $companyName;

    /**
     * @var string
     *
     * @ORM\Column(name="sid", type="string", length=30)
     */
    private $sid;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=150)
     */
    private $address;

    /**
     * @var boolean
     *
     * @ORM\Column(name="has_good_return", type="boolean")
     */
    private $hasGoodReturn;

    /**
     * @var integer
     *
     * @ORM\Column(name="good_return_time", type="integer")
     */
    private $goodReturnTime;

    /**
     * @var string
     *
     * @ORM\Column(name="refund_remind_timeout", type="text")
     */
    private $refundRemindTimeout;

    /**
     * @var integer
     *
     * @ORM\Column(name="created", type="integer")
     */
    private $created;

    /**
     * @var integer
     *
     * @ORM\Column(name="modified", type="integer")
     */
    private $modified;
    
    /**
     * @ORM\ManyToOne(targetEntity="TaobaoBundle\Entity\TbTrade", inversedBy="refunds")
     * @ORM\JoinColumn(name="trade_id", referencedColumnName="id")
     */
    private $trade;

    public function __construct()
    {
        $this->created     = new \Datetime();
        $this->modified    = new \Datetime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tradeId
     *
     * @param integer $tradeId
     * @return TbRefund
     */
    public function setTradeId($tradeId)
    {
        $this->tradeId = $tradeId;

        return $this;
    }

    /**
     * Get tradeId
     *
     * @return integer 
     */
    public function getTradeId()
    {
        return $this->tradeId;
    }

    /**
     * Set orderId
     *
     * @param integer $orderId
     * @return TbRefund
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;

        return $this;
    }

    /**
     * Get orderId
     *
     * @return integer 
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * Set sellerNickName
     *
     * @param string $sellerNickName
     * @return TbRefund
     */
    public function setSellerNickName($sellerNickName)
    {
        $this->sellerNickName = $sellerNickName;

        return $this;
    }

    /**
     * Get sellerNickName
     *
     * @return string 
     */
    public function getSellerNickName()
    {
        return $this->sellerNickName;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return TbRefund
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set goodStatus
     *
     * @param string $goodStatus
     * @return TbRefund
     */
    public function setGoodStatus($goodStatus)
    {
        $this->goodStatus = $goodStatus;

        return $this;
    }

    /**
     * Get goodStatus
     *
     * @return string 
     */
    public function getGoodStatus()
    {
        return $this->goodStatus;
    }

    /**
     * Set totalFee
     *
     * @param float $totalFee
     * @return TbRefund
     */
    public function setTotalFee($totalFee)
    {
        $this->totalFee = $totalFee;

        return $this;
    }

    /**
     * Get totalFee
     *
     * @return float 
     */
    public function getTotalFee()
    {
        return $this->totalFee;
    }

    /**
     * Set payment
     *
     * @param float $payment
     * @return TbRefund
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * Get payment
     *
     * @return float 
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * Set refundFee
     *
     * @param float $refundFee
     * @return TbRefund
     */
    public function setRefundFee($refundFee)
    {
        $this->refundFee = $refundFee;

        return $this;
    }

    /**
     * Get refundFee
     *
     * @return float 
     */
    public function getRefundFee()
    {
        return $this->refundFee;
    }

    /**
     * Set splitTaobaoFee
     *
     * @param float $splitTaobaoFee
     * @return TbRefund
     */
    public function setSplitTaobaoFee($splitTaobaoFee)
    {
        $this->splitTaobaoFee = $splitTaobaoFee;

        return $this;
    }

    /**
     * Get splitTaobaoFee
     *
     * @return float 
     */
    public function getSplitTaobaoFee()
    {
        return $this->splitTaobaoFee;
    }

    /**
     * Set splitSellerFee
     *
     * @param float $splitSellerFee
     * @return TbRefund
     */
    public function setSplitSellerFee($splitSellerFee)
    {
        $this->splitSellerFee = $splitSellerFee;

        return $this;
    }

    /**
     * Get splitSellerFee
     *
     * @return float 
     */
    public function getSplitSellerFee()
    {
        return $this->splitSellerFee;
    }

    /**
     * Set reason
     *
     * @param string $reason
     * @return TbRefund
     */
    public function setReason($reason)
    {
        $this->reason = $reason;

        return $this;
    }

    /**
     * Get reason
     *
     * @return string 
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return TbRefund
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set advanceStatus
     *
     * @param boolean $advanceStatus
     * @return TbRefund
     */
    public function setAdvanceStatus($advanceStatus)
    {
        $this->advanceStatus = $advanceStatus;

        return $this;
    }

    /**
     * Get advanceStatus
     *
     * @return boolean 
     */
    public function getAdvanceStatus()
    {
        return $this->advanceStatus;
    }

    /**
     * Set csStatus
     *
     * @param boolean $csStatus
     * @return TbRefund
     */
    public function setCsStatus($csStatus)
    {
        $this->csStatus = $csStatus;

        return $this;
    }

    /**
     * Get csStatus
     *
     * @return boolean 
     */
    public function getCsStatus()
    {
        return $this->csStatus;
    }

    /**
     * Set companyName
     *
     * @param string $companyName
     * @return TbRefund
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get companyName
     *
     * @return string 
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set sid
     *
     * @param string $sid
     * @return TbRefund
     */
    public function setSid($sid)
    {
        $this->sid = $sid;

        return $this;
    }

    /**
     * Get sid
     *
     * @return string 
     */
    public function getSid()
    {
        return $this->sid;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return TbRefund
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set hasGoodReturn
     *
     * @param boolean $hasGoodReturn
     * @return TbRefund
     */
    public function setHasGoodReturn($hasGoodReturn)
    {
        $this->hasGoodReturn = $hasGoodReturn;

        return $this;
    }

    /**
     * Get hasGoodReturn
     *
     * @return boolean 
     */
    public function getHasGoodReturn()
    {
        return $this->hasGoodReturn;
    }

    /**
     * Set goodReturnTime
     *
     * @param integer $goodReturnTime
     * @return TbRefund
     */
    public function setGoodReturnTime($goodReturnTime)
    {
        $this->goodReturnTime = $goodReturnTime;

        return $this;
    }

    /**
     * Get goodReturnTime
     *
     * @return integer 
     */
    public function getGoodReturnTime()
    {
        return $this->goodReturnTime;
    }

    /**
     * Set refundRemindTimeout
     *
     * @param string $refundRemindTimeout
     * @return TbRefund
     */
    public function setRefundRemindTimeout($refundRemindTimeout)
    {
        $this->refundRemindTimeout = $refundRemindTimeout;

        return $this;
    }

    /**
     * Get refundRemindTimeout
     *
     * @return string 
     */
    public function getRefundRemindTimeout()
    {
        return $this->refundRemindTimeout;
    }

    /**
     * Set created
     *
     * @param integer $created
     * @return TbRefund
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return integer 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param integer $modified
     * @return TbRefund
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return integer 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set trade
     *
     * @param \TaobaoBundle\Entity\TbTrade $trade
     * @return TbRefund
     */
    public function setTrade(\TaobaoBundle\Entity\TbTrade $trade = null)
    {
        $this->trade = $trade;

        return $this;
    }

    /**
     * Get trade
     *
     * @return \TaobaoBundle\Entity\TbTrade 
     */
    public function getTrade()
    {
        return $this->trade;
    }
}
