<?php

namespace TaobaoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Taobao Shop Category
 *
 * @ORM\Table(name="tb_shop_category", indexes={@ORM\Index(name="parent_id", columns={"parent_id", "id"})})
 * @ORM\Entity
 */
class TbShopCategory
{
    /**
     * @var integer
     * Reference cid
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     * Reference parent_cid
     *
     * @ORM\Column(name="parent_id", type="integer")
     */
    private $parentId;

    /**
     * @var boolean
    * Reference is_parent
     *
     * @ORM\Column(name="parented", type="boolean")
     */
    private $parented;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=80)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="sort_order", type="integer")
     */
    private $sortOrder;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=10)
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\Column(name="updatetime", type="integer")
     */
    private $updatetime;
    
    /**
     * @ORM\OneToMany(targetEntity="TaobaoBundle\Entity\TbShop", mappedBy="category")
     */
    private $shops;

    public function __construct()
    {
        $this->items    = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set parentId
     *
     * @param integer $parentId
     * @return TbShopCategory
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * Get parentId
     *
     * @return integer 
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * Set parented
     *
     * @param boolean $parented
     * @return TbShopCategory
     */
    public function setParented($parented)
    {
        $this->parented = $parented;

        return $this;
    }

    /**
     * Get parented
     *
     * @return boolean 
     */
    public function getParented()
    {
        return $this->parented;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return TbShopCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return TbShopCategory
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return TbShopCategory
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set updatetime
     *
     * @param integer $updatetime
     * @return TbShopCategory
     */
    public function setUpdatetime($updatetime)
    {
        $this->updatetime = $updatetime;

        return $this;
    }

    /**
     * Get updatetime
     *
     * @return integer 
     */
    public function getUpdatetime()
    {
        return $this->updatetime;
    }

    /**
     * Add shops
     *
     * @param \TaobaoBundle\Entity\TbShop $shops
     * @return TbShopCategory
     */
    public function addShop(\TaobaoBundle\Entity\TbShop $shops)
    {
        $this->shops[] = $shops;

        return $this;
    }

    /**
     * Remove shops
     *
     * @param \TaobaoBundle\Entity\TbShop $shops
     */
    public function removeShop(\TaobaoBundle\Entity\TbShop $shops)
    {
        $this->shops->removeElement($shops);
    }

    /**
     * Get shops
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getShops()
    {
        return $this->shops;
    }
}
