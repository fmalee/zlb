<?php

namespace TaobaoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Taobao Sku
 *
 * @ORM\Table(name="tb_sku", indexes={@ORM\Index(name="iid", columns={"item_id", "id", "properties"})})
 * @ORM\Entity
 */
class TbSku
{

    /**
     * @var integer
     * Reference sku_id
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     * Reference num_iid
     *
     * @ORM\Column(name="item_id", type="bigint", nullable=false)
     */
    private $itemId;

    /**
     * @var string
     *
     * @ORM\Column(name="outer_id", type="string", length=20, nullable=false)
     */
    private $outerId;

    /**
     * @var string
     *
     * @ORM\Column(name="properties", type="string", length=100, nullable=false)
     */
    private $properties;

    /**
     * @var string
     *
     * @ORM\Column(name="properties_name", type="string", length=255, nullable=false)
     */
    private $propertiesName;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", precision=10, scale=0, nullable=false)
     */
    private $price;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="integer", nullable=false)
     */
    private $quantity;

    /**
     * @var integer
     *
     * @ORM\Column(name="stock", type="integer", nullable=false)
     */
    private $stock;

    /**
     * @var integer
     *
     * @ORM\Column(name="failed", type="integer", nullable=false)
     */
    private $failed;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=false)
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\Column(name="created", type="integer", nullable=false)
     */
    private $created;

    /**
     * @var integer
     *
     * @ORM\Column(name="modified", type="integer", nullable=false)
     */
    private $modified;
    
    /**
     * @ORM\ManyToOne(targetEntity="TaobaoBundle\Entity\TbItem", inversedBy="skus")
     * @ORM\JoinColumn(name="item_id", referencedColumnName="id")
     */
    private $item;

    public function __construct()
    {
        $this->created     = new \Datetime();
        $this->modified    = new \Datetime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set itemId
     *
     * @param integer $itemId
     * @return TbSku
     */
    public function setItemId($itemId)
    {
        $this->itemId = $itemId;

        return $this;
    }

    /**
     * Get itemId
     *
     * @return integer 
     */
    public function getItemId()
    {
        return $this->itemId;
    }

    /**
     * Set outerId
     *
     * @param string $outerId
     * @return TbSku
     */
    public function setOuterId($outerId)
    {
        $this->outerId = $outerId;

        return $this;
    }

    /**
     * Get outerId
     *
     * @return string 
     */
    public function getOuterId()
    {
        return $this->outerId;
    }

    /**
     * Set properties
     *
     * @param string $properties
     * @return TbSku
     */
    public function setProperties($properties)
    {
        $this->properties = $properties;

        return $this;
    }

    /**
     * Get properties
     *
     * @return string 
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * Set propertiesName
     *
     * @param string $propertiesName
     * @return TbSku
     */
    public function setPropertiesName($propertiesName)
    {
        $this->propertiesName = $propertiesName;

        return $this;
    }

    /**
     * Get propertiesName
     *
     * @return string 
     */
    public function getPropertiesName()
    {
        return $this->propertiesName;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return TbSku
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return TbSku
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set stock
     *
     * @param integer $stock
     * @return TbSku
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return integer 
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set failed
     *
     * @param integer $failed
     * @return TbSku
     */
    public function setFailed($failed)
    {
        $this->failed = $failed;

        return $this;
    }

    /**
     * Get failed
     *
     * @return integer 
     */
    public function getFailed()
    {
        return $this->failed;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return TbSku
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param integer $created
     * @return TbSku
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return integer 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param integer $modified
     * @return TbSku
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return integer 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set item
     *
     * @param \TaobaoBundle\Entity\TbItem $item
     * @return TbSku
     */
    public function setItem(\TaobaoBundle\Entity\TbItem $item = null)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item
     *
     * @return \TaobaoBundle\Entity\TbItem 
     */
    public function getItem()
    {
        return $this->item;
    }
}
