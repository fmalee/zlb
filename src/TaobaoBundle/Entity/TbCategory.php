<?php

namespace TaobaoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Taobao Category
 *
 * @ORM\Table(name="tb_category", indexes={@ORM\Index(name="parent_id", columns={"parent_id", "sort_order", "id"})})
 * @ORM\Entity
 */
class TbCategory
{
    /**
     * @var integer
     * Reference cid
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=80)
     */
    private $name;

    /**
     * @var integer
     * Reference parent_cid
     *
     * @ORM\Column(name="parent_id", type="integer")
     */
    private $parentId;

    /**
     * @var string
     *
     * @ORM\Column(name="arr_parent", type="string", length=255, nullable=true)
     */
    private $arrParent;

    /**
     * @var string
     *
     * @ORM\Column(name="arr_child", type="text", nullable=true)
     */
    private $arrChild;

    /**
     * @var boolean
     * Reference is_parent
     *
     * @ORM\Column(name="parented", type="boolean")
     */
    private $parented;

    /**
     * @var string
     *
     * @ORM\Column(name="model", type="string", length=10, nullable=true)
     */
    private $model;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=20, nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="nick_name", type="string", length=80, nullable=true)
     */
    private $nickName;

    /**
     * @var integer
     *
     * @ORM\Column(name="sort_order", type="integer")
     */
    private $sortOrder;

    /**
     * @var string
     *
     * @ORM\Column(name="pic_url", type="string", length=255, nullable=true)
     */
    private $picUrl;

    /**
     * @var integer
     *
     * @ORM\Column(name="modified", type="integer")
     */
    private $modified;
    
    /**
     * @ORM\OneToMany(targetEntity="TaobaoBundle\Entity\TbItem", mappedBy="category")
     */
    private $items;
    
    /**
     * @ORM\OneToMany(targetEntity="TaobaoBundle\Entity\TbProperty", mappedBy="category")
     */
    private $properties;

    public function __construct()
    {
        $this->created     = new \Datetime();
        $this->modified    = new \Datetime();
        $this->items    = new ArrayCollection();
        $this->properties    = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return TbCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set parentId
     *
     * @param integer $parentId
     * @return TbCategory
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * Get parentId
     *
     * @return integer 
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * Set arrParent
     *
     * @param string $arrParent
     * @return TbCategory
     */
    public function setArrParent($arrParent)
    {
        $this->arrParent = $arrParent;

        return $this;
    }

    /**
     * Get arrParent
     *
     * @return string 
     */
    public function getArrParent()
    {
        return $this->arrParent;
    }

    /**
     * Set arrChild
     *
     * @param string $arrChild
     * @return TbCategory
     */
    public function setArrChild($arrChild)
    {
        $this->arrChild = $arrChild;

        return $this;
    }

    /**
     * Get arrChild
     *
     * @return string 
     */
    public function getArrChild()
    {
        return $this->arrChild;
    }

    /**
     * Set parented
     *
     * @param boolean $parented
     * @return TbCategory
     */
    public function setParented($parented)
    {
        $this->parented = $parented;

        return $this;
    }

    /**
     * Get parented
     *
     * @return boolean 
     */
    public function getParented()
    {
        return $this->parented;
    }

    /**
     * Set model
     *
     * @param string $model
     * @return TbCategory
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Get model
     *
     * @return string 
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return TbCategory
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set nickName
     *
     * @param string $nickName
     * @return TbCategory
     */
    public function setNickName($nickName)
    {
        $this->nickName = $nickName;

        return $this;
    }

    /**
     * Get nickName
     *
     * @return string 
     */
    public function getNickName()
    {
        return $this->nickName;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return TbCategory
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set picUrl
     *
     * @param string $picUrl
     * @return TbCategory
     */
    public function setPicUrl($picUrl)
    {
        $this->picUrl = $picUrl;

        return $this;
    }

    /**
     * Get picUrl
     *
     * @return string 
     */
    public function getPicUrl()
    {
        return $this->picUrl;
    }

    /**
     * Set modified
     *
     * @param integer $modified
     * @return TbCategory
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return integer 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Add items
     *
     * @param \TaobaoBundle\Entity\TbItem $items
     * @return TbCategory
     */
    public function addItem(\TaobaoBundle\Entity\TbItem $items)
    {
        $this->items[] = $items;

        return $this;
    }

    /**
     * Remove items
     *
     * @param \TaobaoBundle\Entity\TbItem $items
     */
    public function removeItem(\TaobaoBundle\Entity\TbItem $items)
    {
        $this->items->removeElement($items);
    }

    /**
     * Get items
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Add properties
     *
     * @param \TaobaoBundle\Entity\TbProperty $properties
     * @return TbCategory
     */
    public function addProperty(\TaobaoBundle\Entity\TbProperty $properties)
    {
        $this->properties[] = $properties;

        return $this;
    }

    /**
     * Remove properties
     *
     * @param \TaobaoBundle\Entity\TbProperty $properties
     */
    public function removeProperty(\TaobaoBundle\Entity\TbProperty $properties)
    {
        $this->properties->removeElement($properties);
    }

    /**
     * Get properties
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProperties()
    {
        return $this->properties;
    }
}
