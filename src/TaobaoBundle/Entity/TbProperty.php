<?php

namespace TaobaoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Taobao Property
 *
 * @ORM\Table(name="tb_property")
 * @ORM\Entity
 */
class TbProperty
{

    /**
     * @var integer
     * Reference cid
     *
     * @ORM\Column(name="category_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $categoryId;

    /**
     * @var integer
     *
     * @ORM\Column(name="pid", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $pid;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=20)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="parent_pid", type="integer")
     */
    private $parentPid;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_color_prop", type="boolean")
     */
    private $isColorProp;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_enum_prop", type="boolean")
     */
    private $isEnumProp;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_input_prop", type="boolean")
     */
    private $isInputProp;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_item_prop", type="boolean")
     */
    private $isItemProp;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_key_prop", type="boolean")
     */
    private $isKeyProp;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_sale_prop", type="boolean")
     */
    private $isSaleProp;

    /**
     * @var boolean
     *
     * @ORM\Column(name="multi", type="boolean")
     */
    private $multi;

    /**
     * @var boolean
     *
     * @ORM\Column(name="must", type="boolean")
     */
    private $must;

    /**
     * @var integer
     *
     * @ORM\Column(name="sort_order", type="integer")
     */
    private $sortOrder;

    /**
     * @var integer
     *
     * @ORM\Column(name="parent_vid", type="integer")
     */
    private $parentVid;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_allow_alias", type="boolean")
     */
    private $isAllowAlias;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=20)
     */
    private $status;

    /**
     * @var string
     * Reference prop_values
     *
     * @ORM\Column(name="value", type="text")
     */
    private $value;

    /**
     * @var string
     *
     * @ORM\Column(name="child_template", type="string", length=50)
     */
    private $childTemplate;

    /**
     * @var integer
     *
     * @ORM\Column(name="modified", type="integer")
     */
    private $modified;

    /**
     * @ORM\ManyToOne(targetEntity="TaobaoBundle\Entity\TbCategory", inversedBy="properties")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    public function __construct()
    {
        $this->modified    = new \Datetime();
    }


    /**
     * Set categoryId
     *
     * @param integer $categoryId
     * @return TbProperty
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;

        return $this;
    }

    /**
     * Get categoryId
     *
     * @return integer 
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * Set pid
     *
     * @param integer $pid
     * @return TbProperty
     */
    public function setPid($pid)
    {
        $this->pid = $pid;

        return $this;
    }

    /**
     * Get pid
     *
     * @return integer 
     */
    public function getPid()
    {
        return $this->pid;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return TbProperty
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set parentPid
     *
     * @param integer $parentPid
     * @return TbProperty
     */
    public function setParentPid($parentPid)
    {
        $this->parentPid = $parentPid;

        return $this;
    }

    /**
     * Get parentPid
     *
     * @return integer 
     */
    public function getParentPid()
    {
        return $this->parentPid;
    }

    /**
     * Set isColorProp
     *
     * @param boolean $isColorProp
     * @return TbProperty
     */
    public function setIsColorProp($isColorProp)
    {
        $this->isColorProp = $isColorProp;

        return $this;
    }

    /**
     * Get isColorProp
     *
     * @return boolean 
     */
    public function getIsColorProp()
    {
        return $this->isColorProp;
    }

    /**
     * Set isEnumProp
     *
     * @param boolean $isEnumProp
     * @return TbProperty
     */
    public function setIsEnumProp($isEnumProp)
    {
        $this->isEnumProp = $isEnumProp;

        return $this;
    }

    /**
     * Get isEnumProp
     *
     * @return boolean 
     */
    public function getIsEnumProp()
    {
        return $this->isEnumProp;
    }

    /**
     * Set isInputProp
     *
     * @param boolean $isInputProp
     * @return TbProperty
     */
    public function setIsInputProp($isInputProp)
    {
        $this->isInputProp = $isInputProp;

        return $this;
    }

    /**
     * Get isInputProp
     *
     * @return boolean 
     */
    public function getIsInputProp()
    {
        return $this->isInputProp;
    }

    /**
     * Set isItemProp
     *
     * @param boolean $isItemProp
     * @return TbProperty
     */
    public function setIsItemProp($isItemProp)
    {
        $this->isItemProp = $isItemProp;

        return $this;
    }

    /**
     * Get isItemProp
     *
     * @return boolean 
     */
    public function getIsItemProp()
    {
        return $this->isItemProp;
    }

    /**
     * Set isKeyProp
     *
     * @param boolean $isKeyProp
     * @return TbProperty
     */
    public function setIsKeyProp($isKeyProp)
    {
        $this->isKeyProp = $isKeyProp;

        return $this;
    }

    /**
     * Get isKeyProp
     *
     * @return boolean 
     */
    public function getIsKeyProp()
    {
        return $this->isKeyProp;
    }

    /**
     * Set isSaleProp
     *
     * @param boolean $isSaleProp
     * @return TbProperty
     */
    public function setIsSaleProp($isSaleProp)
    {
        $this->isSaleProp = $isSaleProp;

        return $this;
    }

    /**
     * Get isSaleProp
     *
     * @return boolean 
     */
    public function getIsSaleProp()
    {
        return $this->isSaleProp;
    }

    /**
     * Set multi
     *
     * @param boolean $multi
     * @return TbProperty
     */
    public function setMulti($multi)
    {
        $this->multi = $multi;

        return $this;
    }

    /**
     * Get multi
     *
     * @return boolean 
     */
    public function getMulti()
    {
        return $this->multi;
    }

    /**
     * Set must
     *
     * @param boolean $must
     * @return TbProperty
     */
    public function setMust($must)
    {
        $this->must = $must;

        return $this;
    }

    /**
     * Get must
     *
     * @return boolean 
     */
    public function getMust()
    {
        return $this->must;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return TbProperty
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set parentVid
     *
     * @param integer $parentVid
     * @return TbProperty
     */
    public function setParentVid($parentVid)
    {
        $this->parentVid = $parentVid;

        return $this;
    }

    /**
     * Get parentVid
     *
     * @return integer 
     */
    public function getParentVid()
    {
        return $this->parentVid;
    }

    /**
     * Set isAllowAlias
     *
     * @param boolean $isAllowAlias
     * @return TbProperty
     */
    public function setIsAllowAlias($isAllowAlias)
    {
        $this->isAllowAlias = $isAllowAlias;

        return $this;
    }

    /**
     * Get isAllowAlias
     *
     * @return boolean 
     */
    public function getIsAllowAlias()
    {
        return $this->isAllowAlias;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return TbProperty
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return TbProperty
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set childTemplate
     *
     * @param string $childTemplate
     * @return TbProperty
     */
    public function setChildTemplate($childTemplate)
    {
        $this->childTemplate = $childTemplate;

        return $this;
    }

    /**
     * Get childTemplate
     *
     * @return string 
     */
    public function getChildTemplate()
    {
        return $this->childTemplate;
    }

    /**
     * Set modified
     *
     * @param integer $modified
     * @return TbProperty
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return integer 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set category
     *
     * @param \TaobaoBundle\Entity\TbCategory $category
     * @return TbProperty
     */
    public function setCategory(\TaobaoBundle\Entity\TbCategory $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \TaobaoBundle\Entity\TbCategory 
     */
    public function getCategory()
    {
        return $this->category;
    }
}
