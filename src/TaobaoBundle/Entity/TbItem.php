<?php

namespace TaobaoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Taobao Item
 *
 * @ORM\Table(name="tb_item", indexes={@ORM\Index(name="status", columns={"status"})})
 * @ORM\Entity
 */
class TbItem
{
    /**
     * @var integer
     * Reference num_iid
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=70)
     */
    private $title;

    /**
     * @var string
     * Reference nick
     *
     * @ORM\Column(name="nick_name", type="string", length=20)
     */
    private $nickName;

    /**
     * @var string
     *
     * @ORM\Column(name="outer_id", type="string", length=15)
     */
    private $outerId;

    /**
     * @var string
     *
     * @ORM\Column(name="approve_status", type="string", length=10)
     */
    private $approveStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="approve_type", type="string", length=10)
     */
    private $approveType;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=10)
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="volume", type="integer")
     */
    private $volume;

    /**
     * @var integer
     * Reference cid
     *
     * @ORM\Column(name="category_id", type="integer")
     */
    private $categoryId;

    /**
     * @var string
     *
     * @ORM\Column(name="seller_cids", type="string", length=255)
     */
    private $sellerCids;

    /**
     * @var string
     *
     * @ORM\Column(name="pic_url", type="string", length=255)
     */
    private $picUrl;

    /**
     * @var integer
     *
     * @ORM\Column(name="list_time", type="integer")
     */
    private $listTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="delist_time", type="integer")
     */
    private $delistTime;

    /**
     * @var string
     *
     * @ORM\Column(name="stuff_status", type="string", length=10)
     */
    private $stuffStatus;

    /**
     * @var string
     * Reference property
     *
     * @ORM\Column(name="property", type="text")
     */
    private $property;

    /**
     * @var string
     *
     * @ORM\Column(name="property_alias", type="text")
     */
    private $propertyAlias;

    /**
     * @var string
     *
     * @ORM\Column(name="input_pids", type="string", length=255)
     */
    private $inputPids;

    /**
     * @var string
     *
     * @ORM\Column(name="input_str", type="string", length=255)
     */
    private $inputStr;

    /**
     * @var integer
     *
     * @ORM\Column(name="auction_point", type="integer")
     */
    private $auctionPoint;

    /**
     * @var integer
     *
     * @ORM\Column(name="template_id", type="integer")
     */
    private $templateId;

    /**
     * @var integer
     *
     * @ORM\Column(name="postage_id", type="integer")
     */
    private $postageId;

    /**
     * @var integer
     *
     * @ORM\Column(name="sub_stock", type="integer")
     */
    private $subStock;

    /**
     * @var integer
     *
     * @ORM\Column(name="num", type="integer")
     */
    private $num;

    /**
     * @var integer
     *
     * @ORM\Column(name="price", type="integer")
     */
    private $price;

    /**
     * @var integer
     *
     * @ORM\Column(name="valid_thru", type="integer")
     */
    private $validThru;

    /**
     * @var integer
     *
     * @ORM\Column(name="express_fee", type="integer")
     */
    private $expressFee;

    /**
     * @var integer
     *
     * @ORM\Column(name="ems_fee", type="integer")
     */
    private $emsFee;

    /**
     * @var integer
     *
     * @ORM\Column(name="post_fee", type="integer")
     */
    private $postFee;

    /**
     * @var integer
     *
     * @ORM\Column(name="product_id", type="integer")
     */
    private $productId;

    /**
     * @var string
     *
     * @ORM\Column(name="freight_payer", type="string", length=10)
     */
    private $freightPayer;

    /**
     * @var string
     *
     * @ORM\Column(name="increment", type="string", length=10)
     */
    private $increment;

    /**
     * @var boolean
     *
     * @ORM\Column(name="has_discount", type="boolean")
     */
    private $hasDiscount;

    /**
     * @var boolean
     *
     * @ORM\Column(name="has_invoice", type="boolean")
     */
    private $hasInvoice;

    /**
     * @var boolean
     *
     * @ORM\Column(name="has_warranty", type="boolean")
     */
    private $hasWarranty;

    /**
     * @var boolean
     *
     * @ORM\Column(name="has_showcase", type="boolean")
     */
    private $hasShowcase;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_lightning_consignment", type="integer")
     */
    private $isLightningConsignment;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_xinpin", type="boolean")
     */
    private $isXinpin;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_virtual", type="boolean")
     */
    private $isVirtual;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_taobao", type="boolean")
     */
    private $isTaobao;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_ex", type="boolean")
     */
    private $isEx;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_timing", type="boolean")
     */
    private $isTiming;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_3D", type="boolean")
     */
    private $is3d;

    /**
     * @var boolean
     *
     * @ORM\Column(name="one_station", type="boolean")
     */
    private $oneStation;

    /**
     * @var boolean
     *
     * @ORM\Column(name="violation", type="boolean")
     */
    private $violation;

    /**
     * @var integer
     *
     * @ORM\Column(name="cod_postage_id", type="integer")
     */
    private $codPostageId;

    /**
     * @var integer
     *
     * @ORM\Column(name="sell_promise", type="integer")
     */
    private $sellPromise;

    /**
     * @var integer
     *
     * @ORM\Column(name="discount", type="integer")
     */
    private $discount;

    /**
     * @var string
     * Reference desc
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var boolean
     *
     * @ORM\Column(name="elited", type="boolean")
     */
    private $elited;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean")
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\Column(name="created", type="integer")
     */
    private $created;

    /**
     * @var integer
     *
     * @ORM\Column(name="modified", type="integer")
     */
    private $modified;
    
    /**
     * @ORM\ManyToOne(targetEntity="AccountBundle\Entity\User", inversedBy="items")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;
    
    /**
     * @ORM\ManyToOne(targetEntity="TaobaoBundle\Entity\TbCategory", inversedBy="items")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="TaobaoBundle\Entity\TbSku", mappedBy="item")
     */
    private $skus;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->skus = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return TbItem
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set nickName
     *
     * @param string $nickName
     * @return TbItem
     */
    public function setNickName($nickName)
    {
        $this->nickName = $nickName;

        return $this;
    }

    /**
     * Get nickName
     *
     * @return string 
     */
    public function getNickName()
    {
        return $this->nickName;
    }

    /**
     * Set outerId
     *
     * @param string $outerId
     * @return TbItem
     */
    public function setOuterId($outerId)
    {
        $this->outerId = $outerId;

        return $this;
    }

    /**
     * Get outerId
     *
     * @return string 
     */
    public function getOuterId()
    {
        return $this->outerId;
    }

    /**
     * Set approveStatus
     *
     * @param string $approveStatus
     * @return TbItem
     */
    public function setApproveStatus($approveStatus)
    {
        $this->approveStatus = $approveStatus;

        return $this;
    }

    /**
     * Get approveStatus
     *
     * @return string 
     */
    public function getApproveStatus()
    {
        return $this->approveStatus;
    }

    /**
     * Set approveType
     *
     * @param string $approveType
     * @return TbItem
     */
    public function setApproveType($approveType)
    {
        $this->approveType = $approveType;

        return $this;
    }

    /**
     * Get approveType
     *
     * @return string 
     */
    public function getApproveType()
    {
        return $this->approveType;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return TbItem
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set volume
     *
     * @param integer $volume
     * @return TbItem
     */
    public function setVolume($volume)
    {
        $this->volume = $volume;

        return $this;
    }

    /**
     * Get volume
     *
     * @return integer 
     */
    public function getVolume()
    {
        return $this->volume;
    }

    /**
     * Set categoryId
     *
     * @param integer $categoryId
     * @return TbItem
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;

        return $this;
    }

    /**
     * Get categoryId
     *
     * @return integer 
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * Set sellerCids
     *
     * @param string $sellerCids
     * @return TbItem
     */
    public function setSellerCids($sellerCids)
    {
        $this->sellerCids = $sellerCids;

        return $this;
    }

    /**
     * Get sellerCids
     *
     * @return string 
     */
    public function getSellerCids()
    {
        return $this->sellerCids;
    }

    /**
     * Set picUrl
     *
     * @param string $picUrl
     * @return TbItem
     */
    public function setPicUrl($picUrl)
    {
        $this->picUrl = $picUrl;

        return $this;
    }

    /**
     * Get picUrl
     *
     * @return string 
     */
    public function getPicUrl()
    {
        return $this->picUrl;
    }

    /**
     * Set listTime
     *
     * @param integer $listTime
     * @return TbItem
     */
    public function setListTime($listTime)
    {
        $this->listTime = $listTime;

        return $this;
    }

    /**
     * Get listTime
     *
     * @return integer 
     */
    public function getListTime()
    {
        return $this->listTime;
    }

    /**
     * Set delistTime
     *
     * @param integer $delistTime
     * @return TbItem
     */
    public function setDelistTime($delistTime)
    {
        $this->delistTime = $delistTime;

        return $this;
    }

    /**
     * Get delistTime
     *
     * @return integer 
     */
    public function getDelistTime()
    {
        return $this->delistTime;
    }

    /**
     * Set stuffStatus
     *
     * @param string $stuffStatus
     * @return TbItem
     */
    public function setStuffStatus($stuffStatus)
    {
        $this->stuffStatus = $stuffStatus;

        return $this;
    }

    /**
     * Get stuffStatus
     *
     * @return string 
     */
    public function getStuffStatus()
    {
        return $this->stuffStatus;
    }

    /**
     * Set property
     *
     * @param string $property
     * @return TbItem
     */
    public function setProperty($property)
    {
        $this->property = $property;

        return $this;
    }

    /**
     * Get property
     *
     * @return string 
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * Set propertyAlias
     *
     * @param string $propertyAlias
     * @return TbItem
     */
    public function setPropertyAlias($propertyAlias)
    {
        $this->propertyAlias = $propertyAlias;

        return $this;
    }

    /**
     * Get propertyAlias
     *
     * @return string 
     */
    public function getPropertyAlias()
    {
        return $this->propertyAlias;
    }

    /**
     * Set inputPids
     *
     * @param string $inputPids
     * @return TbItem
     */
    public function setInputPids($inputPids)
    {
        $this->inputPids = $inputPids;

        return $this;
    }

    /**
     * Get inputPids
     *
     * @return string 
     */
    public function getInputPids()
    {
        return $this->inputPids;
    }

    /**
     * Set inputStr
     *
     * @param string $inputStr
     * @return TbItem
     */
    public function setInputStr($inputStr)
    {
        $this->inputStr = $inputStr;

        return $this;
    }

    /**
     * Get inputStr
     *
     * @return string 
     */
    public function getInputStr()
    {
        return $this->inputStr;
    }

    /**
     * Set auctionPoint
     *
     * @param integer $auctionPoint
     * @return TbItem
     */
    public function setAuctionPoint($auctionPoint)
    {
        $this->auctionPoint = $auctionPoint;

        return $this;
    }

    /**
     * Get auctionPoint
     *
     * @return integer 
     */
    public function getAuctionPoint()
    {
        return $this->auctionPoint;
    }

    /**
     * Set templateId
     *
     * @param integer $templateId
     * @return TbItem
     */
    public function setTemplateId($templateId)
    {
        $this->templateId = $templateId;

        return $this;
    }

    /**
     * Get templateId
     *
     * @return integer 
     */
    public function getTemplateId()
    {
        return $this->templateId;
    }

    /**
     * Set postageId
     *
     * @param integer $postageId
     * @return TbItem
     */
    public function setPostageId($postageId)
    {
        $this->postageId = $postageId;

        return $this;
    }

    /**
     * Get postageId
     *
     * @return integer 
     */
    public function getPostageId()
    {
        return $this->postageId;
    }

    /**
     * Set subStock
     *
     * @param integer $subStock
     * @return TbItem
     */
    public function setSubStock($subStock)
    {
        $this->subStock = $subStock;

        return $this;
    }

    /**
     * Get subStock
     *
     * @return integer 
     */
    public function getSubStock()
    {
        return $this->subStock;
    }

    /**
     * Set num
     *
     * @param integer $num
     * @return TbItem
     */
    public function setNum($num)
    {
        $this->num = $num;

        return $this;
    }

    /**
     * Get num
     *
     * @return integer 
     */
    public function getNum()
    {
        return $this->num;
    }

    /**
     * Set price
     *
     * @param integer $price
     * @return TbItem
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return integer 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set validThru
     *
     * @param integer $validThru
     * @return TbItem
     */
    public function setValidThru($validThru)
    {
        $this->validThru = $validThru;

        return $this;
    }

    /**
     * Get validThru
     *
     * @return integer 
     */
    public function getValidThru()
    {
        return $this->validThru;
    }

    /**
     * Set expressFee
     *
     * @param integer $expressFee
     * @return TbItem
     */
    public function setExpressFee($expressFee)
    {
        $this->expressFee = $expressFee;

        return $this;
    }

    /**
     * Get expressFee
     *
     * @return integer 
     */
    public function getExpressFee()
    {
        return $this->expressFee;
    }

    /**
     * Set emsFee
     *
     * @param integer $emsFee
     * @return TbItem
     */
    public function setEmsFee($emsFee)
    {
        $this->emsFee = $emsFee;

        return $this;
    }

    /**
     * Get emsFee
     *
     * @return integer 
     */
    public function getEmsFee()
    {
        return $this->emsFee;
    }

    /**
     * Set postFee
     *
     * @param integer $postFee
     * @return TbItem
     */
    public function setPostFee($postFee)
    {
        $this->postFee = $postFee;

        return $this;
    }

    /**
     * Get postFee
     *
     * @return integer 
     */
    public function getPostFee()
    {
        return $this->postFee;
    }

    /**
     * Set productId
     *
     * @param integer $productId
     * @return TbItem
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * Get productId
     *
     * @return integer 
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * Set freightPayer
     *
     * @param string $freightPayer
     * @return TbItem
     */
    public function setFreightPayer($freightPayer)
    {
        $this->freightPayer = $freightPayer;

        return $this;
    }

    /**
     * Get freightPayer
     *
     * @return string 
     */
    public function getFreightPayer()
    {
        return $this->freightPayer;
    }

    /**
     * Set increment
     *
     * @param string $increment
     * @return TbItem
     */
    public function setIncrement($increment)
    {
        $this->increment = $increment;

        return $this;
    }

    /**
     * Get increment
     *
     * @return string 
     */
    public function getIncrement()
    {
        return $this->increment;
    }

    /**
     * Set hasDiscount
     *
     * @param boolean $hasDiscount
     * @return TbItem
     */
    public function setHasDiscount($hasDiscount)
    {
        $this->hasDiscount = $hasDiscount;

        return $this;
    }

    /**
     * Get hasDiscount
     *
     * @return boolean 
     */
    public function getHasDiscount()
    {
        return $this->hasDiscount;
    }

    /**
     * Set hasInvoice
     *
     * @param boolean $hasInvoice
     * @return TbItem
     */
    public function setHasInvoice($hasInvoice)
    {
        $this->hasInvoice = $hasInvoice;

        return $this;
    }

    /**
     * Get hasInvoice
     *
     * @return boolean 
     */
    public function getHasInvoice()
    {
        return $this->hasInvoice;
    }

    /**
     * Set hasWarranty
     *
     * @param boolean $hasWarranty
     * @return TbItem
     */
    public function setHasWarranty($hasWarranty)
    {
        $this->hasWarranty = $hasWarranty;

        return $this;
    }

    /**
     * Get hasWarranty
     *
     * @return boolean 
     */
    public function getHasWarranty()
    {
        return $this->hasWarranty;
    }

    /**
     * Set hasShowcase
     *
     * @param boolean $hasShowcase
     * @return TbItem
     */
    public function setHasShowcase($hasShowcase)
    {
        $this->hasShowcase = $hasShowcase;

        return $this;
    }

    /**
     * Get hasShowcase
     *
     * @return boolean 
     */
    public function getHasShowcase()
    {
        return $this->hasShowcase;
    }

    /**
     * Set isLightningConsignment
     *
     * @param integer $isLightningConsignment
     * @return TbItem
     */
    public function setIsLightningConsignment($isLightningConsignment)
    {
        $this->isLightningConsignment = $isLightningConsignment;

        return $this;
    }

    /**
     * Get isLightningConsignment
     *
     * @return integer 
     */
    public function getIsLightningConsignment()
    {
        return $this->isLightningConsignment;
    }

    /**
     * Set isXinpin
     *
     * @param boolean $isXinpin
     * @return TbItem
     */
    public function setIsXinpin($isXinpin)
    {
        $this->isXinpin = $isXinpin;

        return $this;
    }

    /**
     * Get isXinpin
     *
     * @return boolean 
     */
    public function getIsXinpin()
    {
        return $this->isXinpin;
    }

    /**
     * Set isVirtual
     *
     * @param boolean $isVirtual
     * @return TbItem
     */
    public function setIsVirtual($isVirtual)
    {
        $this->isVirtual = $isVirtual;

        return $this;
    }

    /**
     * Get isVirtual
     *
     * @return boolean 
     */
    public function getIsVirtual()
    {
        return $this->isVirtual;
    }

    /**
     * Set isTaobao
     *
     * @param boolean $isTaobao
     * @return TbItem
     */
    public function setIsTaobao($isTaobao)
    {
        $this->isTaobao = $isTaobao;

        return $this;
    }

    /**
     * Get isTaobao
     *
     * @return boolean 
     */
    public function getIsTaobao()
    {
        return $this->isTaobao;
    }

    /**
     * Set isEx
     *
     * @param boolean $isEx
     * @return TbItem
     */
    public function setIsEx($isEx)
    {
        $this->isEx = $isEx;

        return $this;
    }

    /**
     * Get isEx
     *
     * @return boolean 
     */
    public function getIsEx()
    {
        return $this->isEx;
    }

    /**
     * Set isTiming
     *
     * @param boolean $isTiming
     * @return TbItem
     */
    public function setIsTiming($isTiming)
    {
        $this->isTiming = $isTiming;

        return $this;
    }

    /**
     * Get isTiming
     *
     * @return boolean 
     */
    public function getIsTiming()
    {
        return $this->isTiming;
    }

    /**
     * Set is3d
     *
     * @param boolean $is3d
     * @return TbItem
     */
    public function setIs3d($is3d)
    {
        $this->is3d = $is3d;

        return $this;
    }

    /**
     * Get is3d
     *
     * @return boolean 
     */
    public function getIs3d()
    {
        return $this->is3d;
    }

    /**
     * Set oneStation
     *
     * @param boolean $oneStation
     * @return TbItem
     */
    public function setOneStation($oneStation)
    {
        $this->oneStation = $oneStation;

        return $this;
    }

    /**
     * Get oneStation
     *
     * @return boolean 
     */
    public function getOneStation()
    {
        return $this->oneStation;
    }

    /**
     * Set violation
     *
     * @param boolean $violation
     * @return TbItem
     */
    public function setViolation($violation)
    {
        $this->violation = $violation;

        return $this;
    }

    /**
     * Get violation
     *
     * @return boolean 
     */
    public function getViolation()
    {
        return $this->violation;
    }

    /**
     * Set codPostageId
     *
     * @param integer $codPostageId
     * @return TbItem
     */
    public function setCodPostageId($codPostageId)
    {
        $this->codPostageId = $codPostageId;

        return $this;
    }

    /**
     * Get codPostageId
     *
     * @return integer 
     */
    public function getCodPostageId()
    {
        return $this->codPostageId;
    }

    /**
     * Set sellPromise
     *
     * @param integer $sellPromise
     * @return TbItem
     */
    public function setSellPromise($sellPromise)
    {
        $this->sellPromise = $sellPromise;

        return $this;
    }

    /**
     * Get sellPromise
     *
     * @return integer 
     */
    public function getSellPromise()
    {
        return $this->sellPromise;
    }

    /**
     * Set discount
     *
     * @param integer $discount
     * @return TbItem
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return integer 
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return TbItem
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set elited
     *
     * @param boolean $elited
     * @return TbItem
     */
    public function setElited($elited)
    {
        $this->elited = $elited;

        return $this;
    }

    /**
     * Get elited
     *
     * @return boolean 
     */
    public function getElited()
    {
        return $this->elited;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return TbItem
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param integer $created
     * @return TbItem
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return integer 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param integer $modified
     * @return TbItem
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return integer 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set user
     *
     * @param \AccountBundle\Entity\User $user
     * @return TbItem
     */
    public function setUser(\AccountBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AccountBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set category
     *
     * @param \TaobaoBundle\Entity\TbCategory $category
     * @return TbItem
     */
    public function setCategory(\TaobaoBundle\Entity\TbCategory $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \TaobaoBundle\Entity\TbCategory 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Add skus
     *
     * @param \TaobaoBundle\Entity\TbSku $skus
     * @return TbItem
     */
    public function addSkus(\TaobaoBundle\Entity\TbSku $skus)
    {
        $this->skus[] = $skus;

        return $this;
    }

    /**
     * Remove skus
     *
     * @param \TaobaoBundle\Entity\TbSku $skus
     */
    public function removeSkus(\TaobaoBundle\Entity\TbSku $skus)
    {
        $this->skus->removeElement($skus);
    }

    /**
     * Get skus
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSkus()
    {
        return $this->skus;
    }
}
