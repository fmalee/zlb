<?php

namespace TaobaoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Taobao Order
 *
 * @ORM\Table(name="tb_order", indexes={@ORM\Index(name="id", columns={"id", "trade_id", "id", "sku_id"})})
 * @ORM\Entity
 */
class TbOrder
{
     /**
     * @var integer
     * Reference num_iid
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     * Reference tid
     *
     * @ORM\Column(name="trade_id", type="bigint")
     */
    private $tradeId;

    /**
     * @var integer
     * Reference num_iid
     *
     * @ORM\Column(name="item_id", type="bigint")
     */
    private $itemId;

    /**
     * @var integer
     *
     * @ORM\Column(name="sku_id", type="bigint")
     */
    private $skuId;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=40)
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\Column(name="refund_id", type="bigint")
     */
    private $refundId;

    /**
     * @var string
     *
     * @ORM\Column(name="refund_status", type="string", length=40)
     */
    private $refundStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="item_meal_id", type="bigint")
     */
    private $itemMealId;

    /**
     * @var string
     *
     * @ORM\Column(name="seller_type", type="string", length=5)
     */
    private $sellerType;

    /**
     * @var integer
     *
     * @ORM\Column(name="cid", type="integer")
     */
    private $cid;

    /**
     * @var integer
     *
     * @ORM\Column(name="num", type="integer")
     */
    private $num;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", precision=10, scale=0)
     */
    private $price;

    /**
     * @var float
     *
     * @ORM\Column(name="payment", type="float", precision=10, scale=0)
     */
    private $payment;

    /**
     * @var float
     *
     * @ORM\Column(name="adjust_fee", type="float", precision=10, scale=0)
     */
    private $adjustFee;

    /**
     * @var float
     *
     * @ORM\Column(name="discount_fee", type="float", precision=10, scale=0)
     */
    private $discountFee;

    /**
     * @var float
     *
     * @ORM\Column(name="total_fee", type="float", precision=10, scale=0)
     */
    private $totalFee;

    /**
     * @var string
     *
     * @ORM\Column(name="order_from", type="string", length=20)
     */
    private $orderFrom;

    /**
     * @var string
     *
     * @ORM\Column(name="pic_path", type="string", length=255)
     */
    private $picPath;

    /**
     * @var string
     *
     * @ORM\Column(name="snapshot_url", type="string", length=50)
     */
    private $snapshotUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="outer_iid", type="string", length=40)
     */
    private $outerIid;

    /**
     * @var string
     *
     * @ORM\Column(name="outer_sku_id", type="string", length=40)
     */
    private $outerSkuId;

    /**
     * @var string
     *
     * @ORM\Column(name="sku_properties_name", type="string", length=255)
     */
    private $skuPropertiesName;

    /**
     * @var boolean
     *
     * @ORM\Column(name="buyer_rate", type="boolean")
     */
    private $buyerRate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="seller_rate", type="boolean")
     */
    private $sellerRate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_oversold", type="boolean")
     */
    private $isOversold;

    /**
     * @var string
     *
     * @ORM\Column(name="timeout_action_time", type="string", length=30)
     */
    private $timeoutActionTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="end_time", type="integer")
     */
    private $endTime;

    /**
     * @ORM\ManyToOne(targetEntity="TaobaoBundle\Entity\TbTrade", inversedBy="orders")
     * @ORM\JoinColumn(name="trade_id", referencedColumnName="id")
     */
    private $trade;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tradeId
     *
     * @param integer $tradeId
     * @return TbOrder
     */
    public function setTradeId($tradeId)
    {
        $this->tradeId = $tradeId;

        return $this;
    }

    /**
     * Get tradeId
     *
     * @return integer 
     */
    public function getTradeId()
    {
        return $this->tradeId;
    }

    /**
     * Set itemId
     *
     * @param integer $itemId
     * @return TbOrder
     */
    public function setItemId($itemId)
    {
        $this->itemId = $itemId;

        return $this;
    }

    /**
     * Get itemId
     *
     * @return integer 
     */
    public function getItemId()
    {
        return $this->itemId;
    }

    /**
     * Set skuId
     *
     * @param integer $skuId
     * @return TbOrder
     */
    public function setSkuId($skuId)
    {
        $this->skuId = $skuId;

        return $this;
    }

    /**
     * Get skuId
     *
     * @return integer 
     */
    public function getSkuId()
    {
        return $this->skuId;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return TbOrder
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return TbOrder
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set refundId
     *
     * @param integer $refundId
     * @return TbOrder
     */
    public function setRefundId($refundId)
    {
        $this->refundId = $refundId;

        return $this;
    }

    /**
     * Get refundId
     *
     * @return integer 
     */
    public function getRefundId()
    {
        return $this->refundId;
    }

    /**
     * Set refundStatus
     *
     * @param string $refundStatus
     * @return TbOrder
     */
    public function setRefundStatus($refundStatus)
    {
        $this->refundStatus = $refundStatus;

        return $this;
    }

    /**
     * Get refundStatus
     *
     * @return string 
     */
    public function getRefundStatus()
    {
        return $this->refundStatus;
    }

    /**
     * Set itemMealId
     *
     * @param integer $itemMealId
     * @return TbOrder
     */
    public function setItemMealId($itemMealId)
    {
        $this->itemMealId = $itemMealId;

        return $this;
    }

    /**
     * Get itemMealId
     *
     * @return integer 
     */
    public function getItemMealId()
    {
        return $this->itemMealId;
    }

    /**
     * Set sellerType
     *
     * @param string $sellerType
     * @return TbOrder
     */
    public function setSellerType($sellerType)
    {
        $this->sellerType = $sellerType;

        return $this;
    }

    /**
     * Get sellerType
     *
     * @return string 
     */
    public function getSellerType()
    {
        return $this->sellerType;
    }

    /**
     * Set cid
     *
     * @param integer $cid
     * @return TbOrder
     */
    public function setCid($cid)
    {
        $this->cid = $cid;

        return $this;
    }

    /**
     * Get cid
     *
     * @return integer 
     */
    public function getCid()
    {
        return $this->cid;
    }

    /**
     * Set num
     *
     * @param integer $num
     * @return TbOrder
     */
    public function setNum($num)
    {
        $this->num = $num;

        return $this;
    }

    /**
     * Get num
     *
     * @return integer 
     */
    public function getNum()
    {
        return $this->num;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return TbOrder
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set payment
     *
     * @param float $payment
     * @return TbOrder
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * Get payment
     *
     * @return float 
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * Set adjustFee
     *
     * @param float $adjustFee
     * @return TbOrder
     */
    public function setAdjustFee($adjustFee)
    {
        $this->adjustFee = $adjustFee;

        return $this;
    }

    /**
     * Get adjustFee
     *
     * @return float 
     */
    public function getAdjustFee()
    {
        return $this->adjustFee;
    }

    /**
     * Set discountFee
     *
     * @param float $discountFee
     * @return TbOrder
     */
    public function setDiscountFee($discountFee)
    {
        $this->discountFee = $discountFee;

        return $this;
    }

    /**
     * Get discountFee
     *
     * @return float 
     */
    public function getDiscountFee()
    {
        return $this->discountFee;
    }

    /**
     * Set totalFee
     *
     * @param float $totalFee
     * @return TbOrder
     */
    public function setTotalFee($totalFee)
    {
        $this->totalFee = $totalFee;

        return $this;
    }

    /**
     * Get totalFee
     *
     * @return float 
     */
    public function getTotalFee()
    {
        return $this->totalFee;
    }

    /**
     * Set orderFrom
     *
     * @param string $orderFrom
     * @return TbOrder
     */
    public function setOrderFrom($orderFrom)
    {
        $this->orderFrom = $orderFrom;

        return $this;
    }

    /**
     * Get orderFrom
     *
     * @return string 
     */
    public function getOrderFrom()
    {
        return $this->orderFrom;
    }

    /**
     * Set picPath
     *
     * @param string $picPath
     * @return TbOrder
     */
    public function setPicPath($picPath)
    {
        $this->picPath = $picPath;

        return $this;
    }

    /**
     * Get picPath
     *
     * @return string 
     */
    public function getPicPath()
    {
        return $this->picPath;
    }

    /**
     * Set snapshotUrl
     *
     * @param string $snapshotUrl
     * @return TbOrder
     */
    public function setSnapshotUrl($snapshotUrl)
    {
        $this->snapshotUrl = $snapshotUrl;

        return $this;
    }

    /**
     * Get snapshotUrl
     *
     * @return string 
     */
    public function getSnapshotUrl()
    {
        return $this->snapshotUrl;
    }

    /**
     * Set outerIid
     *
     * @param string $outerIid
     * @return TbOrder
     */
    public function setOuterIid($outerIid)
    {
        $this->outerIid = $outerIid;

        return $this;
    }

    /**
     * Get outerIid
     *
     * @return string 
     */
    public function getOuterIid()
    {
        return $this->outerIid;
    }

    /**
     * Set outerSkuId
     *
     * @param string $outerSkuId
     * @return TbOrder
     */
    public function setOuterSkuId($outerSkuId)
    {
        $this->outerSkuId = $outerSkuId;

        return $this;
    }

    /**
     * Get outerSkuId
     *
     * @return string 
     */
    public function getOuterSkuId()
    {
        return $this->outerSkuId;
    }

    /**
     * Set skuPropertiesName
     *
     * @param string $skuPropertiesName
     * @return TbOrder
     */
    public function setSkuPropertiesName($skuPropertiesName)
    {
        $this->skuPropertiesName = $skuPropertiesName;

        return $this;
    }

    /**
     * Get skuPropertiesName
     *
     * @return string 
     */
    public function getSkuPropertiesName()
    {
        return $this->skuPropertiesName;
    }

    /**
     * Set buyerRate
     *
     * @param boolean $buyerRate
     * @return TbOrder
     */
    public function setBuyerRate($buyerRate)
    {
        $this->buyerRate = $buyerRate;

        return $this;
    }

    /**
     * Get buyerRate
     *
     * @return boolean 
     */
    public function getBuyerRate()
    {
        return $this->buyerRate;
    }

    /**
     * Set sellerRate
     *
     * @param boolean $sellerRate
     * @return TbOrder
     */
    public function setSellerRate($sellerRate)
    {
        $this->sellerRate = $sellerRate;

        return $this;
    }

    /**
     * Get sellerRate
     *
     * @return boolean 
     */
    public function getSellerRate()
    {
        return $this->sellerRate;
    }

    /**
     * Set isOversold
     *
     * @param boolean $isOversold
     * @return TbOrder
     */
    public function setIsOversold($isOversold)
    {
        $this->isOversold = $isOversold;

        return $this;
    }

    /**
     * Get isOversold
     *
     * @return boolean 
     */
    public function getIsOversold()
    {
        return $this->isOversold;
    }

    /**
     * Set timeoutActionTime
     *
     * @param string $timeoutActionTime
     * @return TbOrder
     */
    public function setTimeoutActionTime($timeoutActionTime)
    {
        $this->timeoutActionTime = $timeoutActionTime;

        return $this;
    }

    /**
     * Get timeoutActionTime
     *
     * @return string 
     */
    public function getTimeoutActionTime()
    {
        return $this->timeoutActionTime;
    }

    /**
     * Set endTime
     *
     * @param integer $endTime
     * @return TbOrder
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;

        return $this;
    }

    /**
     * Get endTime
     *
     * @return integer 
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * Set trade
     *
     * @param \TaobaoBundle\Entity\TbTrade $trade
     * @return TbOrder
     */
    public function setTrade(\TaobaoBundle\Entity\TbTrade $trade = null)
    {
        $this->trade = $trade;

        return $this;
    }

    /**
     * Get trade
     *
     * @return \TaobaoBundle\Entity\TbTrade 
     */
    public function getTrade()
    {
        return $this->trade;
    }
}
