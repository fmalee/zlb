<?php

namespace TaobaoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Taobao Trade
 *
 * @ORM\Table(name="tb_trade", indexes={@ORM\Index(name="status", columns={"status", "id"})})
 * @ORM\Entity
 */
class TbTrade
{
    /**
     * @var integer
     * Reference tid
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="seller_nick", type="string", length=50)
     */
    private $sellerNick;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=50)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="cod_status", type="string", length=40)
     */
    private $codStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="buyer_message", type="string", length=255)
     */
    private $buyerMessage;

    /**
     * @var string
     *
     * @ORM\Column(name="seller_memo", type="string", length=255)
     */
    private $sellerMemo;

    /**
     * @var string
     *
     * @ORM\Column(name="trade_memo", type="string", length=255)
     */
    private $tradeMemo;

    /**
     * @var integer
     *
     * @ORM\Column(name="created", type="integer")
     */
    private $created;

    /**
     * @var integer
     *
     * @ORM\Column(name="modified", type="integer")
     */
    private $modified;

    /**
     * @var integer
     *
     * @ORM\Column(name="end_time", type="integer")
     */
    private $endTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="pay_time", type="integer")
     */
    private $payTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="consign_time", type="integer")
     */
    private $consignTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="alipay_id", type="integer")
     */
    private $alipayId;

    /**
     * @var integer
     *
     * @ORM\Column(name="alipay_no", type="integer")
     */
    private $alipayNo;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="shipping_type", type="string", length=40)
     */
    private $shippingType;

    /**
     * @var boolean
     *
     * @ORM\Column(name="seller_flag", type="boolean")
     */
    private $sellerFlag;

    /**
     * @var float
     *
     * @ORM\Column(name="commission_fee", type="float", precision=10, scale=0)
     */
    private $commissionFee;

    /**
     * @var float
     *
     * @ORM\Column(name="received_payment", type="float", precision=10, scale=0)
     */
    private $receivedPayment;

    /**
     * @var float
     *
     * @ORM\Column(name="available_confirm_fee", type="float", precision=10, scale=0)
     */
    private $availableConfirmFee;

    /**
     * @var float
     *
     * @ORM\Column(name="payment", type="float", precision=10, scale=0)
     */
    private $payment;

    /**
     * @var float
     *
     * @ORM\Column(name="cod_fee", type="float", precision=10, scale=0)
     */
    private $codFee;

    /**
     * @var float
     *
     * @ORM\Column(name="point_fee", type="float", precision=10, scale=0)
     */
    private $pointFee;

    /**
     * @var float
     *
     * @ORM\Column(name="total_fee", type="float", precision=10, scale=0)
     */
    private $totalFee;

    /**
     * @var float
     *
     * @ORM\Column(name="post_fee", type="float", precision=10, scale=0)
     */
    private $postFee;

    /**
     * @var float
     *
     * @ORM\Column(name="express_agency_fee", type="float", precision=10, scale=0)
     */
    private $expressAgencyFee;

    /**
     * @var integer
     *
     * @ORM\Column(name="buyer_obtain_point_fee", type="integer")
     */
    private $buyerObtainPointFee;

    /**
     * @var boolean
     *
     * @ORM\Column(name="has_post_fee", type="boolean")
     */
    private $hasPostFee;

    /**
     * @var string
     *
     * @ORM\Column(name="buyer_nick", type="string", length=50)
     */
    private $buyerNick;

    /**
     * @var string
     *
     * @ORM\Column(name="buyer_alipay_no", type="string", length=100)
     */
    private $buyerAlipayNo;

    /**
     * @var string
     *
     * @ORM\Column(name="buyer_email", type="string", length=100)
     */
    private $buyerEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="receiver_name", type="string", length=40)
     */
    private $receiverName;

    /**
     * @var string
     *
     * @ORM\Column(name="receiver_state", type="string", length=40)
     */
    private $receiverState;

    /**
     * @var string
     *
     * @ORM\Column(name="receiver_city", type="string", length=40)
     */
    private $receiverCity;

    /**
     * @var string
     *
     * @ORM\Column(name="receiver_district", type="string", length=40)
     */
    private $receiverDistrict;

    /**
     * @var string
     *
     * @ORM\Column(name="receiver_address", type="string", length=255)
     */
    private $receiverAddress;

    /**
     * @var integer
     *
     * @ORM\Column(name="receiver_zip", type="integer")
     */
    private $receiverZip;

    /**
     * @var integer
     *
     * @ORM\Column(name="receiver_mobile", type="bigint")
     */
    private $receiverMobile;

    /**
     * @var string
     *
     * @ORM\Column(name="receiver_phone", type="string", length=100)
     */
    private $receiverPhone;

    /**
     * @var string
     *
     * @ORM\Column(name="invoice_name", type="string", length=100)
     */
    private $invoiceName;

    /**
     * @var integer
     *
     * @ORM\Column(name="real_point_fee", type="integer")
     */
    private $realPointFee;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_3D", type="boolean")
     */
    private $is3d;

    /**
     * @var boolean
     *
     * @ORM\Column(name="can_rate", type="boolean")
     */
    private $canRate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="buyer_rate", type="boolean")
     */
    private $buyerRate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="seller_rate", type="boolean")
     */
    private $sellerRate;

    /**
     * @var string
     *
     * @ORM\Column(name="trade_from", type="string", length=100)
     */
    private $tradeFrom;

    /**
     * @var string
     *
     * @ORM\Column(name="delivery_company", type="string", length=20)
     */
    private $deliveryCompany;

    /**
     * @var string
     *
     * @ORM\Column(name="out_sid", type="string", length=15)
     */
    private $outSid;

    /**
     * @var string
     *
     * @ORM\Column(name="delivery_status", type="string", length=30)
     */
    private $deliveryStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="delivery_start", type="integer")
     */
    private $deliveryStart;

    /**
     * @var integer
     *
     * @ORM\Column(name="delivery_end", type="integer")
     */
    private $deliveryEnd;

    /**
     * @var integer
     *
     * @ORM\Column(name="delivery_created", type="integer")
     */
    private $deliveryCreated;

    /**
     * @var integer
     *
     * @ORM\Column(name="delivery_modified", type="integer")
     */
    private $deliveryModified;

    /**
     * @var integer
     *
     * @ORM\Column(name="invoice", type="integer")
     */
    private $invoice;

    /**
     * @var integer
     *
     * @ORM\Column(name="service_time", type="integer")
     */
    private $serviceTime;

    /**
     * @var string
     *
     * @ORM\Column(name="promotion_details", type="text")
     */
    private $promotionDetails;

    /**
     * @var boolean
     *
     * @ORM\Column(name="credit", type="boolean")
     */
    private $credit;

    /**
     * @ORM\OneToMany(targetEntity="TaobaoBundle\Entity\TbOrder", mappedBy="trade")
     */
    private $orders;

    /**
     * @ORM\OneToMany(targetEntity="TaobaoBundle\Entity\TbRefund", mappedBy="trade")
     */
    private $refunds;

    public function __construct()
    {
        $this->orders    = new ArrayCollection();
        $this->refunds    = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sellerNick
     *
     * @param string $sellerNick
     * @return TbTrade
     */
    public function setSellerNick($sellerNick)
    {
        $this->sellerNick = $sellerNick;

        return $this;
    }

    /**
     * Get sellerNick
     *
     * @return string 
     */
    public function getSellerNick()
    {
        return $this->sellerNick;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return TbTrade
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set codStatus
     *
     * @param string $codStatus
     * @return TbTrade
     */
    public function setCodStatus($codStatus)
    {
        $this->codStatus = $codStatus;

        return $this;
    }

    /**
     * Get codStatus
     *
     * @return string 
     */
    public function getCodStatus()
    {
        return $this->codStatus;
    }

    /**
     * Set buyerMessage
     *
     * @param string $buyerMessage
     * @return TbTrade
     */
    public function setBuyerMessage($buyerMessage)
    {
        $this->buyerMessage = $buyerMessage;

        return $this;
    }

    /**
     * Get buyerMessage
     *
     * @return string 
     */
    public function getBuyerMessage()
    {
        return $this->buyerMessage;
    }

    /**
     * Set sellerMemo
     *
     * @param string $sellerMemo
     * @return TbTrade
     */
    public function setSellerMemo($sellerMemo)
    {
        $this->sellerMemo = $sellerMemo;

        return $this;
    }

    /**
     * Get sellerMemo
     *
     * @return string 
     */
    public function getSellerMemo()
    {
        return $this->sellerMemo;
    }

    /**
     * Set tradeMemo
     *
     * @param string $tradeMemo
     * @return TbTrade
     */
    public function setTradeMemo($tradeMemo)
    {
        $this->tradeMemo = $tradeMemo;

        return $this;
    }

    /**
     * Get tradeMemo
     *
     * @return string 
     */
    public function getTradeMemo()
    {
        return $this->tradeMemo;
    }

    /**
     * Set created
     *
     * @param integer $created
     * @return TbTrade
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return integer 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param integer $modified
     * @return TbTrade
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return integer 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set endTime
     *
     * @param integer $endTime
     * @return TbTrade
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;

        return $this;
    }

    /**
     * Get endTime
     *
     * @return integer 
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * Set payTime
     *
     * @param integer $payTime
     * @return TbTrade
     */
    public function setPayTime($payTime)
    {
        $this->payTime = $payTime;

        return $this;
    }

    /**
     * Get payTime
     *
     * @return integer 
     */
    public function getPayTime()
    {
        return $this->payTime;
    }

    /**
     * Set consignTime
     *
     * @param integer $consignTime
     * @return TbTrade
     */
    public function setConsignTime($consignTime)
    {
        $this->consignTime = $consignTime;

        return $this;
    }

    /**
     * Get consignTime
     *
     * @return integer 
     */
    public function getConsignTime()
    {
        return $this->consignTime;
    }

    /**
     * Set alipayId
     *
     * @param integer $alipayId
     * @return TbTrade
     */
    public function setAlipayId($alipayId)
    {
        $this->alipayId = $alipayId;

        return $this;
    }

    /**
     * Get alipayId
     *
     * @return integer 
     */
    public function getAlipayId()
    {
        return $this->alipayId;
    }

    /**
     * Set alipayNo
     *
     * @param integer $alipayNo
     * @return TbTrade
     */
    public function setAlipayNo($alipayNo)
    {
        $this->alipayNo = $alipayNo;

        return $this;
    }

    /**
     * Get alipayNo
     *
     * @return integer 
     */
    public function getAlipayNo()
    {
        return $this->alipayNo;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return TbTrade
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set shippingType
     *
     * @param string $shippingType
     * @return TbTrade
     */
    public function setShippingType($shippingType)
    {
        $this->shippingType = $shippingType;

        return $this;
    }

    /**
     * Get shippingType
     *
     * @return string 
     */
    public function getShippingType()
    {
        return $this->shippingType;
    }

    /**
     * Set sellerFlag
     *
     * @param boolean $sellerFlag
     * @return TbTrade
     */
    public function setSellerFlag($sellerFlag)
    {
        $this->sellerFlag = $sellerFlag;

        return $this;
    }

    /**
     * Get sellerFlag
     *
     * @return boolean 
     */
    public function getSellerFlag()
    {
        return $this->sellerFlag;
    }

    /**
     * Set commissionFee
     *
     * @param float $commissionFee
     * @return TbTrade
     */
    public function setCommissionFee($commissionFee)
    {
        $this->commissionFee = $commissionFee;

        return $this;
    }

    /**
     * Get commissionFee
     *
     * @return float 
     */
    public function getCommissionFee()
    {
        return $this->commissionFee;
    }

    /**
     * Set receivedPayment
     *
     * @param float $receivedPayment
     * @return TbTrade
     */
    public function setReceivedPayment($receivedPayment)
    {
        $this->receivedPayment = $receivedPayment;

        return $this;
    }

    /**
     * Get receivedPayment
     *
     * @return float 
     */
    public function getReceivedPayment()
    {
        return $this->receivedPayment;
    }

    /**
     * Set availableConfirmFee
     *
     * @param float $availableConfirmFee
     * @return TbTrade
     */
    public function setAvailableConfirmFee($availableConfirmFee)
    {
        $this->availableConfirmFee = $availableConfirmFee;

        return $this;
    }

    /**
     * Get availableConfirmFee
     *
     * @return float 
     */
    public function getAvailableConfirmFee()
    {
        return $this->availableConfirmFee;
    }

    /**
     * Set payment
     *
     * @param float $payment
     * @return TbTrade
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * Get payment
     *
     * @return float 
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * Set codFee
     *
     * @param float $codFee
     * @return TbTrade
     */
    public function setCodFee($codFee)
    {
        $this->codFee = $codFee;

        return $this;
    }

    /**
     * Get codFee
     *
     * @return float 
     */
    public function getCodFee()
    {
        return $this->codFee;
    }

    /**
     * Set pointFee
     *
     * @param float $pointFee
     * @return TbTrade
     */
    public function setPointFee($pointFee)
    {
        $this->pointFee = $pointFee;

        return $this;
    }

    /**
     * Get pointFee
     *
     * @return float 
     */
    public function getPointFee()
    {
        return $this->pointFee;
    }

    /**
     * Set totalFee
     *
     * @param float $totalFee
     * @return TbTrade
     */
    public function setTotalFee($totalFee)
    {
        $this->totalFee = $totalFee;

        return $this;
    }

    /**
     * Get totalFee
     *
     * @return float 
     */
    public function getTotalFee()
    {
        return $this->totalFee;
    }

    /**
     * Set postFee
     *
     * @param float $postFee
     * @return TbTrade
     */
    public function setPostFee($postFee)
    {
        $this->postFee = $postFee;

        return $this;
    }

    /**
     * Get postFee
     *
     * @return float 
     */
    public function getPostFee()
    {
        return $this->postFee;
    }

    /**
     * Set expressAgencyFee
     *
     * @param float $expressAgencyFee
     * @return TbTrade
     */
    public function setExpressAgencyFee($expressAgencyFee)
    {
        $this->expressAgencyFee = $expressAgencyFee;

        return $this;
    }

    /**
     * Get expressAgencyFee
     *
     * @return float 
     */
    public function getExpressAgencyFee()
    {
        return $this->expressAgencyFee;
    }

    /**
     * Set buyerObtainPointFee
     *
     * @param integer $buyerObtainPointFee
     * @return TbTrade
     */
    public function setBuyerObtainPointFee($buyerObtainPointFee)
    {
        $this->buyerObtainPointFee = $buyerObtainPointFee;

        return $this;
    }

    /**
     * Get buyerObtainPointFee
     *
     * @return integer 
     */
    public function getBuyerObtainPointFee()
    {
        return $this->buyerObtainPointFee;
    }

    /**
     * Set hasPostFee
     *
     * @param boolean $hasPostFee
     * @return TbTrade
     */
    public function setHasPostFee($hasPostFee)
    {
        $this->hasPostFee = $hasPostFee;

        return $this;
    }

    /**
     * Get hasPostFee
     *
     * @return boolean 
     */
    public function getHasPostFee()
    {
        return $this->hasPostFee;
    }

    /**
     * Set buyerNick
     *
     * @param string $buyerNick
     * @return TbTrade
     */
    public function setBuyerNick($buyerNick)
    {
        $this->buyerNick = $buyerNick;

        return $this;
    }

    /**
     * Get buyerNick
     *
     * @return string 
     */
    public function getBuyerNick()
    {
        return $this->buyerNick;
    }

    /**
     * Set buyerAlipayNo
     *
     * @param string $buyerAlipayNo
     * @return TbTrade
     */
    public function setBuyerAlipayNo($buyerAlipayNo)
    {
        $this->buyerAlipayNo = $buyerAlipayNo;

        return $this;
    }

    /**
     * Get buyerAlipayNo
     *
     * @return string 
     */
    public function getBuyerAlipayNo()
    {
        return $this->buyerAlipayNo;
    }

    /**
     * Set buyerEmail
     *
     * @param string $buyerEmail
     * @return TbTrade
     */
    public function setBuyerEmail($buyerEmail)
    {
        $this->buyerEmail = $buyerEmail;

        return $this;
    }

    /**
     * Get buyerEmail
     *
     * @return string 
     */
    public function getBuyerEmail()
    {
        return $this->buyerEmail;
    }

    /**
     * Set receiverName
     *
     * @param string $receiverName
     * @return TbTrade
     */
    public function setReceiverName($receiverName)
    {
        $this->receiverName = $receiverName;

        return $this;
    }

    /**
     * Get receiverName
     *
     * @return string 
     */
    public function getReceiverName()
    {
        return $this->receiverName;
    }

    /**
     * Set receiverState
     *
     * @param string $receiverState
     * @return TbTrade
     */
    public function setReceiverState($receiverState)
    {
        $this->receiverState = $receiverState;

        return $this;
    }

    /**
     * Get receiverState
     *
     * @return string 
     */
    public function getReceiverState()
    {
        return $this->receiverState;
    }

    /**
     * Set receiverCity
     *
     * @param string $receiverCity
     * @return TbTrade
     */
    public function setReceiverCity($receiverCity)
    {
        $this->receiverCity = $receiverCity;

        return $this;
    }

    /**
     * Get receiverCity
     *
     * @return string 
     */
    public function getReceiverCity()
    {
        return $this->receiverCity;
    }

    /**
     * Set receiverDistrict
     *
     * @param string $receiverDistrict
     * @return TbTrade
     */
    public function setReceiverDistrict($receiverDistrict)
    {
        $this->receiverDistrict = $receiverDistrict;

        return $this;
    }

    /**
     * Get receiverDistrict
     *
     * @return string 
     */
    public function getReceiverDistrict()
    {
        return $this->receiverDistrict;
    }

    /**
     * Set receiverAddress
     *
     * @param string $receiverAddress
     * @return TbTrade
     */
    public function setReceiverAddress($receiverAddress)
    {
        $this->receiverAddress = $receiverAddress;

        return $this;
    }

    /**
     * Get receiverAddress
     *
     * @return string 
     */
    public function getReceiverAddress()
    {
        return $this->receiverAddress;
    }

    /**
     * Set receiverZip
     *
     * @param integer $receiverZip
     * @return TbTrade
     */
    public function setReceiverZip($receiverZip)
    {
        $this->receiverZip = $receiverZip;

        return $this;
    }

    /**
     * Get receiverZip
     *
     * @return integer 
     */
    public function getReceiverZip()
    {
        return $this->receiverZip;
    }

    /**
     * Set receiverMobile
     *
     * @param integer $receiverMobile
     * @return TbTrade
     */
    public function setReceiverMobile($receiverMobile)
    {
        $this->receiverMobile = $receiverMobile;

        return $this;
    }

    /**
     * Get receiverMobile
     *
     * @return integer 
     */
    public function getReceiverMobile()
    {
        return $this->receiverMobile;
    }

    /**
     * Set receiverPhone
     *
     * @param string $receiverPhone
     * @return TbTrade
     */
    public function setReceiverPhone($receiverPhone)
    {
        $this->receiverPhone = $receiverPhone;

        return $this;
    }

    /**
     * Get receiverPhone
     *
     * @return string 
     */
    public function getReceiverPhone()
    {
        return $this->receiverPhone;
    }

    /**
     * Set invoiceName
     *
     * @param string $invoiceName
     * @return TbTrade
     */
    public function setInvoiceName($invoiceName)
    {
        $this->invoiceName = $invoiceName;

        return $this;
    }

    /**
     * Get invoiceName
     *
     * @return string 
     */
    public function getInvoiceName()
    {
        return $this->invoiceName;
    }

    /**
     * Set realPointFee
     *
     * @param integer $realPointFee
     * @return TbTrade
     */
    public function setRealPointFee($realPointFee)
    {
        $this->realPointFee = $realPointFee;

        return $this;
    }

    /**
     * Get realPointFee
     *
     * @return integer 
     */
    public function getRealPointFee()
    {
        return $this->realPointFee;
    }

    /**
     * Set is3d
     *
     * @param boolean $is3d
     * @return TbTrade
     */
    public function setIs3d($is3d)
    {
        $this->is3d = $is3d;

        return $this;
    }

    /**
     * Get is3d
     *
     * @return boolean 
     */
    public function getIs3d()
    {
        return $this->is3d;
    }

    /**
     * Set canRate
     *
     * @param boolean $canRate
     * @return TbTrade
     */
    public function setCanRate($canRate)
    {
        $this->canRate = $canRate;

        return $this;
    }

    /**
     * Get canRate
     *
     * @return boolean 
     */
    public function getCanRate()
    {
        return $this->canRate;
    }

    /**
     * Set buyerRate
     *
     * @param boolean $buyerRate
     * @return TbTrade
     */
    public function setBuyerRate($buyerRate)
    {
        $this->buyerRate = $buyerRate;

        return $this;
    }

    /**
     * Get buyerRate
     *
     * @return boolean 
     */
    public function getBuyerRate()
    {
        return $this->buyerRate;
    }

    /**
     * Set sellerRate
     *
     * @param boolean $sellerRate
     * @return TbTrade
     */
    public function setSellerRate($sellerRate)
    {
        $this->sellerRate = $sellerRate;

        return $this;
    }

    /**
     * Get sellerRate
     *
     * @return boolean 
     */
    public function getSellerRate()
    {
        return $this->sellerRate;
    }

    /**
     * Set tradeFrom
     *
     * @param string $tradeFrom
     * @return TbTrade
     */
    public function setTradeFrom($tradeFrom)
    {
        $this->tradeFrom = $tradeFrom;

        return $this;
    }

    /**
     * Get tradeFrom
     *
     * @return string 
     */
    public function getTradeFrom()
    {
        return $this->tradeFrom;
    }

    /**
     * Set deliveryCompany
     *
     * @param string $deliveryCompany
     * @return TbTrade
     */
    public function setDeliveryCompany($deliveryCompany)
    {
        $this->deliveryCompany = $deliveryCompany;

        return $this;
    }

    /**
     * Get deliveryCompany
     *
     * @return string 
     */
    public function getDeliveryCompany()
    {
        return $this->deliveryCompany;
    }

    /**
     * Set outSid
     *
     * @param string $outSid
     * @return TbTrade
     */
    public function setOutSid($outSid)
    {
        $this->outSid = $outSid;

        return $this;
    }

    /**
     * Get outSid
     *
     * @return string 
     */
    public function getOutSid()
    {
        return $this->outSid;
    }

    /**
     * Set deliveryStatus
     *
     * @param string $deliveryStatus
     * @return TbTrade
     */
    public function setDeliveryStatus($deliveryStatus)
    {
        $this->deliveryStatus = $deliveryStatus;

        return $this;
    }

    /**
     * Get deliveryStatus
     *
     * @return string 
     */
    public function getDeliveryStatus()
    {
        return $this->deliveryStatus;
    }

    /**
     * Set deliveryStart
     *
     * @param integer $deliveryStart
     * @return TbTrade
     */
    public function setDeliveryStart($deliveryStart)
    {
        $this->deliveryStart = $deliveryStart;

        return $this;
    }

    /**
     * Get deliveryStart
     *
     * @return integer 
     */
    public function getDeliveryStart()
    {
        return $this->deliveryStart;
    }

    /**
     * Set deliveryEnd
     *
     * @param integer $deliveryEnd
     * @return TbTrade
     */
    public function setDeliveryEnd($deliveryEnd)
    {
        $this->deliveryEnd = $deliveryEnd;

        return $this;
    }

    /**
     * Get deliveryEnd
     *
     * @return integer 
     */
    public function getDeliveryEnd()
    {
        return $this->deliveryEnd;
    }

    /**
     * Set deliveryCreated
     *
     * @param integer $deliveryCreated
     * @return TbTrade
     */
    public function setDeliveryCreated($deliveryCreated)
    {
        $this->deliveryCreated = $deliveryCreated;

        return $this;
    }

    /**
     * Get deliveryCreated
     *
     * @return integer 
     */
    public function getDeliveryCreated()
    {
        return $this->deliveryCreated;
    }

    /**
     * Set deliveryModified
     *
     * @param integer $deliveryModified
     * @return TbTrade
     */
    public function setDeliveryModified($deliveryModified)
    {
        $this->deliveryModified = $deliveryModified;

        return $this;
    }

    /**
     * Get deliveryModified
     *
     * @return integer 
     */
    public function getDeliveryModified()
    {
        return $this->deliveryModified;
    }

    /**
     * Set invoice
     *
     * @param integer $invoice
     * @return TbTrade
     */
    public function setInvoice($invoice)
    {
        $this->invoice = $invoice;

        return $this;
    }

    /**
     * Get invoice
     *
     * @return integer 
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * Set serviceTime
     *
     * @param integer $serviceTime
     * @return TbTrade
     */
    public function setServiceTime($serviceTime)
    {
        $this->serviceTime = $serviceTime;

        return $this;
    }

    /**
     * Get serviceTime
     *
     * @return integer 
     */
    public function getServiceTime()
    {
        return $this->serviceTime;
    }

    /**
     * Set promotionDetails
     *
     * @param string $promotionDetails
     * @return TbTrade
     */
    public function setPromotionDetails($promotionDetails)
    {
        $this->promotionDetails = $promotionDetails;

        return $this;
    }

    /**
     * Get promotionDetails
     *
     * @return string 
     */
    public function getPromotionDetails()
    {
        return $this->promotionDetails;
    }

    /**
     * Set credit
     *
     * @param boolean $credit
     * @return TbTrade
     */
    public function setCredit($credit)
    {
        $this->credit = $credit;

        return $this;
    }

    /**
     * Get credit
     *
     * @return boolean 
     */
    public function getCredit()
    {
        return $this->credit;
    }

    /**
     * Add orders
     *
     * @param \TaobaoBundle\Entity\TbOrder $orders
     * @return TbTrade
     */
    public function addOrder(\TaobaoBundle\Entity\TbOrder $orders)
    {
        $this->orders[] = $orders;

        return $this;
    }

    /**
     * Remove orders
     *
     * @param \TaobaoBundle\Entity\TbOrder $orders
     */
    public function removeOrder(\TaobaoBundle\Entity\TbOrder $orders)
    {
        $this->orders->removeElement($orders);
    }

    /**
     * Get orders
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Add refunds
     *
     * @param \TaobaoBundle\Entity\TbRefund $refunds
     * @return TbTrade
     */
    public function addRefund(\TaobaoBundle\Entity\TbRefund $refunds)
    {
        $this->refunds[] = $refunds;

        return $this;
    }

    /**
     * Remove refunds
     *
     * @param \TaobaoBundle\Entity\TbRefund $refunds
     */
    public function removeRefund(\TaobaoBundle\Entity\TbRefund $refunds)
    {
        $this->refunds->removeElement($refunds);
    }

    /**
     * Get refunds
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRefunds()
    {
        return $this->refunds;
    }
}
