<?php

namespace TaobaoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Taobao Shop
 *
 * @ORM\Table(name="tb_shop", indexes={@ORM\Index(name="sort_order", columns={"sort_order", "sid"}), @ORM\Index(name="status", columns={"status"})})
 * @ORM\Entity
 */
class TbShop
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="sid", type="bigint")
     */
    private $sid;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=60)
     */
    private $title;

    /**
     * @var string
     * Reference nick
     *
     * @ORM\Column(name="nick_name", type="string", length=50)
     */
    private $nickName;

    /**
     * @var boolean
     *
     * @ORM\Column(name="source", type="boolean")
     */
    private $source;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=25)
     */
    private $location;

    /**
     * @var string
     *
     * @ORM\Column(name="business", type="string", length=20)
     */
    private $business;

    /**
     * @var string
     *
     * @ORM\Column(name="contact", type="string", length=30)
     */
    private $contact;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=40)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="qq", type="string", length=30)
     */
    private $qq;

    /**
     * @var string
     *
     * @ORM\Column(name="qq_group", type="string", length=30)
     */
    private $qqGroup;

    /**
     * @var string
     *
     * @ORM\Column(name="csv_path", type="string", length=255)
     */
    private $csvPath;

    /**
     * @var string
     *
     * @ORM\Column(name="memo", type="string", length=255)
     */
    private $memo;

    /**
     * @var string
     *
     * @ORM\Column(name="shop_title", type="string", length=80)
     */
    private $shopTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="pic_path", type="string", length=255)
     */
    private $picPath;

    /**
     * @var string
     *
     * @ORM\Column(name="shop_score", type="text")
     */
    private $shopScore;

    /**
     * @var boolean
     *
     * @ORM\Column(name="sort_order", type="integer")
     */
    private $sort_order;

    /**
     * @var boolean
     *
     * @ORM\Column(name="elited", type="boolean")
     */
    private $elited;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean")
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\Column(name="created", type="integer")
     */
    private $created;

    /**
     * @var integer
     *
     * @ORM\Column(name="modified", type="integer")
     */
    private $modified;

    /**
     * @ORM\ManyToOne(targetEntity="AccountBundle\Entity\User", inversedBy="shops")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="TaobaoBundle\Entity\TbShopCategory", inversedBy="shops")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    public function __construct()
    {
        $this->created     = new \Datetime();
        $this->modified    = new \Datetime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sid
     *
     * @param integer $sid
     * @return TbShop
     */
    public function setSid($sid)
    {
        $this->sid = $sid;

        return $this;
    }

    /**
     * Get sid
     *
     * @return integer 
     */
    public function getSid()
    {
        return $this->sid;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return TbShop
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set nickName
     *
     * @param string $nickName
     * @return TbShop
     */
    public function setNickName($nickName)
    {
        $this->nickName = $nickName;

        return $this;
    }

    /**
     * Get nickName
     *
     * @return string 
     */
    public function getNickName()
    {
        return $this->nickName;
    }

    /**
     * Set source
     *
     * @param boolean $source
     * @return TbShop
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source
     *
     * @return boolean 
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return TbShop
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set business
     *
     * @param string $business
     * @return TbShop
     */
    public function setBusiness($business)
    {
        $this->business = $business;

        return $this;
    }

    /**
     * Get business
     *
     * @return string 
     */
    public function getBusiness()
    {
        return $this->business;
    }

    /**
     * Set contact
     *
     * @param string $contact
     * @return TbShop
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return string 
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return TbShop
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set qq
     *
     * @param string $qq
     * @return TbShop
     */
    public function setQq($qq)
    {
        $this->qq = $qq;

        return $this;
    }

    /**
     * Get qq
     *
     * @return string 
     */
    public function getQq()
    {
        return $this->qq;
    }

    /**
     * Set qqGroup
     *
     * @param string $qqGroup
     * @return TbShop
     */
    public function setQqGroup($qqGroup)
    {
        $this->qqGroup = $qqGroup;

        return $this;
    }

    /**
     * Get qqGroup
     *
     * @return string 
     */
    public function getQqGroup()
    {
        return $this->qqGroup;
    }

    /**
     * Set csvPath
     *
     * @param string $csvPath
     * @return TbShop
     */
    public function setCsvPath($csvPath)
    {
        $this->csvPath = $csvPath;

        return $this;
    }

    /**
     * Get csvPath
     *
     * @return string 
     */
    public function getCsvPath()
    {
        return $this->csvPath;
    }

    /**
     * Set memo
     *
     * @param string $memo
     * @return TbShop
     */
    public function setMemo($memo)
    {
        $this->memo = $memo;

        return $this;
    }

    /**
     * Get memo
     *
     * @return string 
     */
    public function getMemo()
    {
        return $this->memo;
    }

    /**
     * Set shopTitle
     *
     * @param string $shopTitle
     * @return TbShop
     */
    public function setShopTitle($shopTitle)
    {
        $this->shopTitle = $shopTitle;

        return $this;
    }

    /**
     * Get shopTitle
     *
     * @return string 
     */
    public function getShopTitle()
    {
        return $this->shopTitle;
    }

    /**
     * Set picPath
     *
     * @param string $picPath
     * @return TbShop
     */
    public function setPicPath($picPath)
    {
        $this->picPath = $picPath;

        return $this;
    }

    /**
     * Get picPath
     *
     * @return string 
     */
    public function getPicPath()
    {
        return $this->picPath;
    }

    /**
     * Set shopScore
     *
     * @param string $shopScore
     * @return TbShop
     */
    public function setShopScore($shopScore)
    {
        $this->shopScore = $shopScore;

        return $this;
    }

    /**
     * Get shopScore
     *
     * @return string 
     */
    public function getShopScore()
    {
        return $this->shopScore;
    }

    /**
     * Set sort_order
     *
     * @param integer $sortOrder
     * @return TbShop
     */
    public function setSortOrder($sortOrder)
    {
        $this->sort_order = $sortOrder;

        return $this;
    }

    /**
     * Get sort_order
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sort_order;
    }

    /**
     * Set elited
     *
     * @param boolean $elited
     * @return TbShop
     */
    public function setElited($elited)
    {
        $this->elited = $elited;

        return $this;
    }

    /**
     * Get elited
     *
     * @return boolean 
     */
    public function getElited()
    {
        return $this->elited;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return TbShop
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param integer $created
     * @return TbShop
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return integer 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param integer $modified
     * @return TbShop
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return integer 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set user
     *
     * @param \AccountBundle\Entity\User $user
     * @return TbShop
     */
    public function setUser(\AccountBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AccountBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set category
     *
     * @param \TaobaoBundle\Entity\TbShopCategory $category
     * @return TbShop
     */
    public function setCategory(\TaobaoBundle\Entity\TbShopCategory $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \TaobaoBundle\Entity\TbShopCategory 
     */
    public function getCategory()
    {
        return $this->category;
    }
}
