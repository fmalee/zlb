<?php

namespace CommonBundle;

use CoreBundle\AppBundle\Bundle;
use CommonBundle\Compiler;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class CommonBundle extends Bundle
{
    public function build(ContainerBuilder $builder)
    {
        parent::build($builder);

        /* 给CategoryRepository添加Cache服务*/
        $builder->addCompilerPass(new Compiler\AddCategoryRepositoryPass());
    }
}
