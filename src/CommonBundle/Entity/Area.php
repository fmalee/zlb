<?php

namespace CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Area
 *
 * @ORM\Entity(repositoryClass="CommonBundle\Entity\AreaRepository")
 * @ORM\Table(name="mc_area", indexes={@ORM\Index(name="parent_id", columns={"parent_id", "id"})})
 */
class Area
{
     /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=80)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="type", type="boolean")
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="zip", type="integer", nullable=true)
     */
    private $zip;

    /**
     * @var integer
     *
     * @ORM\Column(name="parent_id", type="integer")
     */
    private $parentId;

    /**
     * @var string
     *
     * @ORM\Column(name="arr_parent", type="string", length=255, nullable=true)
     */
    private $arrParent;

    /**
     * @var string
     *
     * @ORM\Column(name="arr_child", type="string", length=255, nullable=true)
     */
    private $arrChild;

    /**
     * @var boolean
     *
     * @ORM\Column(name="parented", type="boolean")
     */
    private $parented;

    /**
     * @ORM\OneToMany(targetEntity="ProductBundle\Entity\Mall", mappedBy="area")
     */
    private $malls;

    /**
     * @ORM\OneToMany(targetEntity="AccountBundle\Entity\Location", mappedBy="area")
     */
    private $locations;

    /**
     * @ORM\OneToMany(targetEntity="TradeBundle\Entity\Trade", mappedBy="area")
     */
    private $trades;

    public function __construct()
    {
        $this->parentId     = 0;
        $this->parented     = false;
        $this->malls        = new ArrayCollection();
        $this->locations    = new ArrayCollection();
        $this->trades       = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * Set id
     *
     * @param string $id
     * @return Area
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Area
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param boolean $type
     * @return Area
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return boolean 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set zip
     *
     * @param integer $zip
     * @return Area
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Get zip
     *
     * @return integer 
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set parentId
     *
     * @param integer $parentId
     * @return Area
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * Get parentId
     *
     * @return integer 
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * Set arrParent
     *
     * @param string $arrParent
     * @return Area
     */
    public function setArrParent($arrParent)
    {
        $this->arrParent = $arrParent;

        return $this;
    }

    /**
     * Get arrParent
     *
     * @return string 
     */
    public function getArrParent()
    {
        return $this->arrParent;
    }

    /**
     * Set arrChild
     *
     * @param string $arrChild
     * @return Area
     */
    public function setArrChild($arrChild)
    {
        $this->arrChild = $arrChild;

        return $this;
    }

    /**
     * Get arrChild
     *
     * @return string 
     */
    public function getArrChild()
    {
        return $this->arrChild;
    }

    /**
     * Set parented
     *
     * @param boolean $parented
     * @return Area
     */
    public function setParented($parented)
    {
        $this->parented = $parented;

        return $this;
    }

    /**
     * Get parented
     *
     * @return boolean 
     */
    public function getParented()
    {
        return $this->parented;
    }

    /**
     * Add malls
     *
     * @param \ProductBundle\Entity\Mall $malls
     * @return Area
     */
    public function addMall(\ProductBundle\Entity\Mall $malls)
    {
        $this->malls[] = $malls;

        return $this;
    }

    /**
     * Remove malls
     *
     * @param \ProductBundle\Entity\Mall $malls
     */
    public function removeMall(\ProductBundle\Entity\Mall $malls)
    {
        $this->malls->removeElement($malls);
    }

    /**
     * Get malls
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMalls()
    {
        return $this->malls;
    }

    /**
     * Add locations
     *
     * @param \AccountBundle\Entity\Location $locations
     * @return Area
     */
    public function addLocation(\AccountBundle\Entity\Location $locations)
    {
        $this->locations[] = $locations;

        return $this;
    }

    /**
     * Remove locations
     *
     * @param \AccountBundle\Entity\Location $locations
     */
    public function removeLocation(\AccountBundle\Entity\Location $locations)
    {
        $this->locations->removeElement($locations);
    }

    /**
     * Get locations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLocations()
    {
        return $this->locations;
    }

    /**
     * Add trades
     *
     * @param \TradeBundle\Entity\Trade $trades
     * @return Area
     */
    public function addTrade(\TradeBundle\Entity\Trade $trades)
    {
        $this->trades[] = $trades;

        return $this;
    }

    /**
     * Remove trades
     *
     * @param \TradeBundle\Entity\Trade $trades
     */
    public function removeTrade(\TradeBundle\Entity\Trade $trades)
    {
        $this->trades->removeElement($trades);
    }

    /**
     * Get trades
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTrades()
    {
        return $this->trades;
    }
}
