<?php

namespace CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use ArticleBundle\Entity\Article;
use ProductBundle\Entity\Mall;

/**
 * Category
 *
 * @ORM\Entity(repositoryClass="CommonBundle\Entity\CategoryRepository")
 * @ORM\Table(name="mc_category", indexes={@ORM\Index(name="pid", columns={"parent_id"}), @ORM\Index(name="name", columns={"name"})})
 * @ORM\HasLifecycleCallbacks()
 */
class Category
{
    const MODULE_DEFAULT = 'article';
    const MODULE_ARTICLE = 'article';
    const MODULE_PRODUCT = 2;
    const MODULE_Mall = 3;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=30)
     *
     * @Assert\Length(
     *      max = "30",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     * @Assert\Regex(
     *      pattern="/^[^-_]+[a-z0-9-_]+[^-_]$/",
     *      message="标签名只能包含字母、数字、_或减号，不能以_或减号开头或结尾。",
     *      groups={"Registration"}
     * )
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=50)
     *
     * @Assert\NotBlank(message = "标题不能为空", groups={"Edit"})
     * @Assert\Length(
     *      max = "50",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     */
    private $title;

    /**
     * @var integer
     *
     * @ORM\Column(name="parent_id", type="integer")
     */
    private $parentId;

    /**
     * @var string
     *
     * @ORM\Column(name="arr_parent", type="string", length=255, nullable=true)
     *
     * @Assert\Length(
     *      max = "255",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     */
    private $arrParent;

    /**
     * @var string
     *
     * @ORM\Column(name="arr_child", type="string", length=255, nullable=true)
     *
     * @Assert\Length(
     *      max = "255",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     */
    private $arrChild;

    /**
     * @var boolean
     *
     * @ORM\Column(name="parented", type="boolean", nullable=true)
     */
    private $parented;

    /**
     * @var integer
     *
     * @ORM\Column(name="icon", type="integer", nullable=true)
     */
    private $icon;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean")
     */
    private $enabled;

    /**
     * @var string
     *
     * @ORM\Column(name="module", type="string", length=20)
     */
    private $module;

    /**
     * @var boolean
     *
     * @ORM\Column(name="page_size", type="integer", nullable=true)
     */
    private $pageSize;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_title", type="string", length=50, nullable=true)
     *
     * @Assert\Length(
     *      max = "50",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     */
    private $metaTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="keywords", type="string", length=255, nullable=true)
     *
     * @Assert\Length(
     *      max = "255",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     */
    private $keywords;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     *
     * @Assert\Length(
     *      max = "255",
     *      maxMessage = "长度不能超过{{ limit }}个字符",
     *      groups={"Edit"}
     * )
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="template_index", type="string", length=100, nullable=true)
     */
    private $templateIndex;

    /**
     * @var string
     *
     * @ORM\Column(name="template_list", type="string", length=100, nullable=true)
     */
    private $templateList;

    /**
     * @var string
     *
     * @ORM\Column(name="template_detail", type="string", length=100, nullable=true)
     */
    private $templateDetail;

    /**
     * @var boolean
     *
     * @ORM\Column(name="display", type="boolean", nullable=true)
     */
    private $display;

    /**
     * @var boolean
     *
     * @ORM\Column(name="reply", type="boolean", nullable=true)
     */
    private $reply;

    /**
     * @var integer
     *
     * @ORM\Column(name="sort_order", type="integer")
     */
    private $sortOrder;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="modified", type="datetime")
     */
    private $modified;
    
    /**
     * @ORM\OneToMany(targetEntity="ArticleBundle\Entity\Article", mappedBy="category")
     */
    private $articles;

    /**
     * @ORM\OneToMany(targetEntity="ProductBundle\Entity\Mall", mappedBy="category")
     */
    private $malls;

    /**
     * @ORM\OneToMany(targetEntity="ProductBundle\Entity\Supply", mappedBy="category")
     */
    private $supplies;

    public function __construct()
    {
        $this->pageSize    = 20;
        $this->parentId    = 0;
        $this->sortOrder   = 0;
        $this->enabled     = true;
        $this->display     = true;
        $this->reply       = true;
        $this->created     = new \Datetime();
        $this->modified    = new \Datetime();
        $this->articles    = new ArrayCollection();
        $this->malls       = new ArrayCollection();
        $this->supplies       = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * @ORM\PreUpdate
     */
    public function setPerUpdate()
    {
        $this->modified = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Category
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set parentId
     *
     * @param integer $parentId
     * @return Category
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * Get parentId
     *
     * @return integer 
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * Set arrParent
     *
     * @param string $arrParent
     * @return Category
     */
    public function setArrParent($arrParent)
    {
        $this->arrParent = $arrParent;

        return $this;
    }

    /**
     * Get parents
     *
     * @return string 
     */
    public function getArrParent()
    {
        return $this->arrParent;
    }

    /**
     * Set arrChild
     *
     * @param string $arrChild
     * @return Category
     */
    public function setArrChild($arrChild)
    {
        $this->arrChild = $arrChild;

        return $this;
    }

    /**
     * Get arrChlid
     *
     * @return string
     */
    public function getArrChild()
    {
        return $this->arrChild;
    }

    /**
     * Set parented
     *
     * @param boolean $boolean
     * @return Category
     */
    public function setParented($boolean)
    {
        $this->parented = (Boolean)$boolean;

        return $this;
    }

    /**
     * Get parented
     *
     * @return boolean 
     */
    public function getParented()
    {
        return $this->parented;
    }

    /**
     * Set icon
     *
     * @param integer $icon
     * @return Category
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return integer 
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set enabled
     *
     * @param boolean $boolean
     * @return Category
     */
    public function setEnabled($boolean)
    {
        $this->enabled = (Boolean)$boolean;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set module
     *
     * @param boolean $module
     * @return Category
     */
    public function setModule($module)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module
     *
     * @return boolean 
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * Set pageSize
     *
     * @param integer $pageSize
     * @return Category
     */
    public function setPageSize($pageSize)
    {
        $this->pageSize = $pageSize;

        return $this;
    }

    /**
     * Get pageSize
     *
     * @return integer 
     */
    public function getPageSize()
    {
        return $this->pageSize;
    }

    /**
     * Set metaTitle
     *
     * @param string $metaTitle
     * @return Category
     */
    public function setMetaTitle($metaTitle)
    {
        $this->metaTitle = $metaTitle;

        return $this;
    }

    /**
     * Get metaTitle
     *
     * @return string 
     */
    public function getMetaTitle()
    {
        return $this->metaTitle;
    }

    /**
     * Set keywords
     *
     * @param string $keywords
     * @return Category
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get keywords
     *
     * @return string 
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Category
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set templateIndex
     *
     * @param string $templateIndex
     * @return Category
     */
    public function setTemplateIndex($templateIndex)
    {
        $this->templateIndex = $templateIndex;

        return $this;
    }

    /**
     * Get templateIndex
     *
     * @return string 
     */
    public function getTemplateIndex()
    {
        return $this->templateIndex;
    }

    /**
     * Set templateList
     *
     * @param string $templateList
     * @return Category
     */
    public function setTemplateList($templateList)
    {
        $this->templateList = $templateList;

        return $this;
    }

    /**
     * Get templateList
     *
     * @return string 
     */
    public function getTemplateList()
    {
        return $this->templateList;
    }

    /**
     * Set templateDetail
     *
     * @param string $templateDetail
     * @return Category
     */
    public function setTemplateDetail($templateDetail)
    {
        $this->templateDetail = $templateDetail;

        return $this;
    }

    /**
     * Get templateDetail
     *
     * @return string 
     */
    public function getTemplateDetail()
    {
        return $this->templateDetail;
    }

    /**
     * Set display
     *
     * @param boolean $display
     * @return Category
     */
    public function setDisplay($display)
    {
        $this->display = $display;

        return $this;
    }

    /**
     * Get display
     *
     * @return boolean 
     */
    public function getDisplay()
    {
        return $this->display;
    }

    /**
     * Set reply
     *
     * @param boolean $reply
     * @return Category
     */
    public function setReply($reply)
    {
        $this->reply = $reply;

        return $this;
    }

    /**
     * Get reply
     *
     * @return boolean 
     */
    public function getReply()
    {
        return $this->reply;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return Category
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set created
     *
     * @param integer $created
     * @return Category
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return integer 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param integer $modified
     * @return Category
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return integer 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Add articles
     *
     * @param \ArticleBundle\Entity\Article $articles
     * @return Category
     */
    public function addArticle(\ArticleBundle\Entity\Article $articles)
    {
        $this->articles[] = $articles;

        return $this;
    }

    /**
     * Remove articles
     *
     * @param \ArticleBundle\Entity\Article $articles
     */
    public function removeArticle(\ArticleBundle\Entity\Article $articles)
    {
        $this->articles->removeElement($articles);
    }

    /**
     * Get articles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * Add malls
     *
     * @param \ProductBundle\Entity\Mall $malls
     * @return Category
     */
    public function addMall(\ProductBundle\Entity\Mall $malls)
    {
        $this->malls[] = $malls;

        return $this;
    }

    /**
     * Remove malls
     *
     * @param \ProductBundle\Entity\Mall $malls
     */
    public function removeMall(\ProductBundle\Entity\Mall $malls)
    {
        $this->malls->removeElement($malls);
    }

    /**
     * Get malls
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMalls()
    {
        return $this->malls;
    }

    /**
     * Add supplies
     *
     * @param \ProductBundle\Entity\Supply $supplies
     * @return Category
     */
    public function addSupply(\ProductBundle\Entity\Supply $supplies)
    {
        $this->supplies[] = $supplies;

        return $this;
    }

    /**
     * Remove supplies
     *
     * @param \ProductBundle\Entity\Supply $supplies
     */
    public function removeSupply(\ProductBundle\Entity\Supply $supplies)
    {
        $this->supplies->removeElement($supplies);
    }

    /**
     * Get supplies
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSupplies()
    {
        return $this->supplies;
    }
}
