<?php

namespace CommonBundle\Entity;

use CoreBundle\Doctrine\EntityRepository;
use CoreBundle\Cache\Cache;

/**
 * Area Repository
 */
class AreaRepository extends EntityRepository
{
    /* cache service*/
    private $cache;
    /*cache file name*/
    private $fileName = 'area.list';
    private $treeName = 'area.tree';

    public function setCacheService(Cache $cache)
    {
        $this->cache = $cache;
    }

    /**
     * {@inheritDoc}
     */
    public function createArea1()
    {
        $class = $this->getClassName();
        $area = new $class();

        return $area;
    }

    /**
     * {@inheritdoc}
     */
    public function findCategoryByName($name)
    {
        $q = $this->createQueryBuilder('c')
            ->Where('c.enabled = true')
            ->andWhere('c.name = :name')
            ->setParameter('name', $name)
            ->getQuery();

        return $q->getSingleResult();
    }

    /**
     * {@inheritdoc}
     */
    public function findCategoryBy(array $criteria)
    {
        return $this->findOneBy($criteria);
    }

    /**
     * {@inheritdoc}
     * 返回目录树
     *
     * @param string $name
     * @return static|null
     */
    public function findCategoryIdByParent($categoryId)
    {
        $qb = $this->createQueryBuilder('c');
        $qb->Where('c.enabled = true')
            ->select('c.id')
            ->andWhere($qb->expr()->like('c.arrParent', ':arrParent'))
            ->andWhere('c.parented = false')
            ->setParameter('arrParent', "%,$categoryId,%");
        $rows = $qb->getQuery()->getArrayResult();

        $category_ids = array();
        if ($rows) {
            foreach ($rows as $row) {
                $category_ids[] = $row['id'];
            }
        }


        return $category_ids;
    }

    /**
     * {@inheritdoc}
     * 返回目录树
     *
     * @param string $name
     * @return static|null
     */
    public function getCategoryTree($categoryId)
    {
        $qb = $this->createQueryBuilder('c');
        $qb->andWhere('c.enabled = true')
            ->andWhere($qb->expr()->like('c.arrParent', ':arrParent'))
            ->addOrderBy('c.created', 'DESC')
            ->setParameter('arrParent', "%,$categoryId,%");
        $rows = $qb->getQuery()->getArrayResult();

        $categoryies = [];
        foreach ($rows as $category) {
            $categoryies[$category['id']] = $category;
        }

        return $this->generateTree($categoryies);
    }

    /**
     * {@inheritdoc}
     * 返回目录
     *
     * @param string $name
     * @return static|null
     */
    public function getCategories($isTree = false)
    {
        if ($isTree) {
            $categories = $this->cache->get($this->treeName);
        } else {
            $categories = $this->cache->get($this->fileName);
        }

        if (!$categories) {
            $categories = $this->cacheCategories($isTree);
        }

        return $categories ?: false;
    }

    function cacheCategories($isTree)
    {
        $qb = $this->createQueryBuilder('c')
            ->andWhere('c.enabled = true')
            ->addOrderBy('c.created', 'DESC');
        $categories = $qb->getQuery()->getArrayResult();

        if (!$categories) {
            return false;
        }

        $list = [];
        foreach ($categories as $category) {
            $list[$category['id']] = $category;
        }
        $this->cache->set($this->fileName, $list);

        $tree = $this->generateTree($list);
        $this->cache->set($this->treeName, $this->generateTree($list));

        return $isTree ? $tree : $list;
    }

    function generateTree($categoryies)
    {
        $t = [];
        foreach ($categoryies as $id => $category) {
            if ($category['parentId']) {
                $categoryies[$category['parentId']]['son'][$category['id']] = &$categoryies[$category['id']];
                $t[] = $id;
            }
        }

        foreach($t as $u) {
            unset($categoryies[$u]);
        }
        return $categoryies;
    }
}
