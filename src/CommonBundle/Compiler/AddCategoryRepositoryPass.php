<?php

namespace CommonBundle\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Add core.cache service
 *
 */
class AddCategoryRepositoryPass implements CompilerPassInterface {

    public function process(ContainerBuilder $container)
    {
        $def= $container->findDefinition('common.entity.category_repository');
        $reference = new Reference('core.cache');
        $def->addMethodCall('setCacheService', array($reference));
    }
}
