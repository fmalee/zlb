<?php

namespace CommonBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EditCategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('parentId', 'hidden', array(
                'required' => true
            ))
            ->add('module', 'hidden', array(
                'required' => true
            ))
            ->add('title', 'text', array(
                'label'       => '名称',
                'attr' => array(
                    'placeholder' => '分类名称, 如 申通快递',
                ),
            ))
            ->add('name', 'text', array(
                'label'       => '代号',
                'attr' => array(
                    'placeholder' => '分类代号，如 sto',
                ),
            ))
            // ->add('icon', 'url', array(
            //     'label'       => '栏目图标',
            //     'required' => false,
            //     'attr' => array(
            //         'placeholder' => '栏目图标',
            //     ),
            // ))
            ->add('enabled', 'choice', array(
                'label'       => '是否启用',
                'choices' => array(0 => '禁用', 1 => '启用'),
                'expanded'    => true
            ))
            ->add('pageSize', 'number', array(
                'label'       => '默认分页',
                'invalid_message'       => '只能输入数字',
            ))
            ->add('templateIndex', 'text', array(
                'label'       => '首页模版',
                'required' => false,
                'attr' => array(
                    'placeholder' => '如 ProductBundle:Product:index.html.twig',
                )
            ))
            ->add('templateList', 'text', array(
                'label'       => '列表页模版',
                'required' => false,
                'attr' => array(
                    'placeholder' => '如 ProductBundle:Product:list.html.twig',
                )
            ))
            ->add('templateDetail', 'text', array(
                'label'       => '详情页模版',
                'required' => false,
                'attr' => array(
                    'placeholder' => '如 ProductBundle:Product:detail.html.twig',
                )
            ))
            ->add('metaTitle', 'text', array(
                'label'       => '网页标题',
                'required' => false,
                'attr' => array(
                    'placeholder' => '用于SEO优化',
                    'class' => 'col-xs-4'
                )
            ))
            ->add('keywords', 'textarea', array(
                'label'       => '网页关键字',
                'required' => false,
                'attr'     => array(
                    'rows'  => 1,
                    'cols'  => 50,
                    'placeholder' => '用于SEO优化',
                )
            ))
            ->add('description', 'textarea', array(
                'label'    => '栏目说明',
                'required' => false,
                'attr'     => array(
                    'rows'  => 3,
                    'cols'  => 50,
                    'placeholder' => '用于SEO优化',
                )
            ))
            ->add('display', 'choice', array(
                'label'       => '前台显示',
                'choices' => array(0 => '不显示', 1 => '显示'),
                'expanded'    => true
            ))
            ->add('reply', 'choice', array(
                'label'       => '允许回复',
                'choices' => array(0 => '不允许', 1 => '允许'),
                'expanded'    => true,
            ))
            ->add('sortOrder', 'text', array(
                'label'       => '排序',
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'        => 'CommonBundle\Entity\Category',
            'validation_groups' => array('Edit')
        ));
    }

    public function getName()
    {
        return 'category';
    }
}