<?php

namespace CommonBundle\Controller;

use CoreBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class CategoryController extends Controller
{
    /**
     * @Route("/category", name="category_home")
     * @Template()
     */
    public function indexAction()
    {
        $categorys = $this->getCategoryRepository()->getCategories(1);
        $this->dump($categorys);

        $categorys = $this->getCategoryRepository()->findByModule(1);
        //$this->dump($categorys);

        return array();
    }

}
