<?php

namespace CommonBundle\Controller;

use CoreBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class WidgetController extends Controller
{
    /**
     * get simple category list
     */
    public function CategoryListAction($route = 'article_list', $categoryId, $categories = null, $template = 'box')
    {
        if (!$categories) {
            $categories = $this->getCategories();
        }

        $arrChild = trim($categories[$categoryId]['arrChild']);
        $arrChild = array_filter(explode(',', $arrChild));
        if (!count($arrChild)) {
            return false;
        }

        $current = array();
        foreach($arrChild as $catId) {
            $current[$catId] = $categories[$catId];
        }
        $categories = $this->getCategoryRepository()->generateTree($current);

        $template = 'CommonBundle:Widget:CategoryList-' . $template. '.html.twig';

        return $this->render($template, array(
            'categories' => array_shift($categories),
            'route' => $route
        ));
    }

    /**
     * get chosen Product list
     */
    public function ChosenProductAction($limit = 5, $template = 'big')
    {
        $products = $this->getProductRepository()->findProductByChosen($limit);
        //$this->dump($products);

        $template = 'CommonBundle:Widget:ChosenProduct-' . $template. '.html.twig';

        return $this->render($template, array(
            'products' => $products,
        ));
    }

    /**
     * get chosen Product list
     */
    public function ProductCateogryAction($categoryId, $limit = 5, $template = 'list')
    {
        $products = $this->getProductRepository()->findProductByCategoryId($categoryId, $limit);
        //$this->dump($products);

        $template = 'CommonBundle:Widget:ProductCateogry-' . $template. '.html.twig';

        return $this->render($template, array(
            'products' => $products,
        ));
    }

    /**
     * @return ArticleRepository
     */
    private function getArticleRepository()
    {
        return $this->get('article.entity.article_repository');
    }

    /**
     * @return ArticleRepository
     */
    private function getProductRepository()
    {
        return $this->get('product.entity.product_repository');
    }

}
