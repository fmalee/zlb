<?php

namespace AdminBundle\Controller;

use CoreBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use CoreBundle\Util\SingleTree;
use CoreBundle\Segment\Segment;
use CoreBundle\Iconv\Iconv;
/**
 * @Route("/admin/cache")
 */
class CacheController extends Controller
{
    /**
     * @Route("/", name="cache_admin")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction()
    {
        $this->setAdminBreadcrumb()->add('管理列表');

        return array();
    }

    /**
     * {@inheritdoc}
     * 返回后台导航
     *
     * @param string $name
     * @return static|null
     */
    protected function setAdminBreadcrumb()
    {
        $this->getBreadcrumb()
            ->add('后台管理', $this->generateUrl('admin_home'))
            ->add('缓存管理', $this->generateUrl('cache_admin'));

        return $this->getBreadcrumb();
    }
}
