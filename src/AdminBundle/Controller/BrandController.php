<?php

namespace AdminBundle\Controller;

use CoreBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use CoreBundle\Util\SingleTree;
use CoreBundle\Util\Help;
use ProductBundle\Entity\Brand;
use CoreBundle\Segment\Segment;
use CoreBundle\Iconv\Iconv;
/**
 * @Route("/admin/brand")
 */
class BrandController extends Controller
{
    /**
     * @Route("/{status}/{page}", requirements={"status" = "publish|draft|trash", "page" = "\d+"}, defaults={"status" = "publish", "page" = 1}, name="brand_admin")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction(Request $request, $status, $page)
    {
        switch ($status) {
            case 'draft' :
                $type = Brand::STATUS_DRAFT;
                $name = '草稿箱';
                break;

            case 'trash' :
                $type = Brand::STATUS_TRASH;
                $name = '回收站';
                break;

            default:
                $type = Brand::STATUS_PUBLISH;
                $name = '已发布';
                break;
        }
        $title = $request->query->get('title');

        $this->setAdminBreadcrumb()->add($name);

        $qb = $this->getBrandRepository()->createQueryByStatus($type);

        $categories = $this->getCategories();
        if ($title) {
            $qb = $this->getBrandRepository()->addQueryByTitle($qb, $title);
        }

        $pagination = $this->paginate($qb, $page, 3);

        return array(
            'pagination' => $pagination,
            'name' => $name,
            'status' => $status
        );
    }

    /**
     * @Route("/add/{mallId}", name="brand_add")
     * @Route("/edit/{id}", name="brand_edit")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function editAction(Request $request, $id = null, $mallId = null)
    {
        $arrParent = array();
        if ($id) {
            $brand = $this->getBrandRepository()->find($id);
            if (!$brand) {
                throw new NotFoundHttpException;
            }
            $mall = $brand->getMall();
        } else {
            $brand = $this->getBrandRepository()->createNew();
            if ($mallId) {
                $mall = $this->getMallRepository()->find($mallId);
            }
        }
        if (!empty($mall)) {
            $area = $mall->getArea();
            $arrParent = trim($area->getArrparent() . $mall->getAreaId() .','. $mall->getId(), ',');
        } else {
            $arrParent = 0;
        }

        $form = $this->createBoundObjectForm($brand, 'edit');

            $title = $form->get('title')->getData();
            $description = $form->get('description')->getData();
            $content = $form->get('content')->getData();
            //自动提取栏目标识
            if (!$form->get('name')->getData()) {
                $name = Iconv::gbk_to_pinyin($title);
                $form->get('name')->setData(implode($name));
            }
            //自动提取SEO标题
            if (!$form->get('metaTitle')->getData()) {
                $form->get('metaTitle')->setData($title);
            }
            //自动提取摘要
            if (!$description) {
                $description = str_replace(array("'","\r\n","\t",'&ldquo;','&rdquo;','&nbsp;'), '', strip_tags($content));
                $description = Help::str_cut($description, 255);
                $form->get('description')->setData($description);
            }
            //自动提取SEO关键字
            if (!$form->get('keywords')->getData()) {
                $keywords = Segment::phpAnalysis($description);
                $form->get('keywords')->setData($keywords);
            }


        if ($form->isBound() && $form->isValid()) {
            if (!$id) {
                $categoryId = $brand->getCategoryId();
                $category = $this->getCategoryRepository()->find($categoryId);
                $brand->setCategory($category);

                $areaId = $brand->getAreaId();
                $area = $this->getAreaRepository()->find($areaId);
                $brand->setArea($area);
            }

            $this->persist($brand, true);

            $this->addFlash('编辑品牌信息成功');

            return $this->redirectToRoute('brand_admin');
        }

        $this->setAdminBreadcrumb()->add('编辑品牌信息');

        return array(
            'form' => $form->createView(),
            'id' => $id,
            'arrParent' => $arrParent
        );
    }

    /**
     * @Route("/ajaxedit", name="brand_ajaxedit")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function ajaxEditAction()
    {
        return $this->ajaxEdit($this->getBrandRepository());
    }

    /**
     * @Route("/republish/{id}", name="brand_publish")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function publishAction($id)
    {
        $brand = $this->getBrandRepository()->find($id);
        if (!$brand) {
            throw $this->createNotFoundException('不存在的记录');
        }

        $status = $brand->getStatus();
        if ($status != Brand::STATUS_PUBLISH) {
            $brand->setStatus(Brand::STATUS_PUBLISH);
            $this->persist($brand, true);
        }

        return $this->ajaxReturn('成功发布操作' . $brand->getTitle());
    }

    /**
     * @Route("/delete/{id}", name="brand_delete")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deleteAction($id)
    {
        $brand = $this->getBrandRepository()->find($id);
        if (!$brand) {
            throw $this->createNotFoundException('不存在的记录');
        }

        $status = $brand->getStatus();
        switch ($status) {
            case Brand::STATUS_TRASH :
                //TODO FMA 关联表删除的判断
                $this->remove($brand, true);
                break;

            default:
                $brand->setStatus(Brand::STATUS_TRASH);
                $this->persist($brand, true);
                break;
        }

        return $this->ajaxReturn('成功删除' . $brand->getTitle());
    }

    /**
     * @Route("/json", name="brand_json")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function jsonAction(Request $request)
    {
        $name = $request->query->get('name');
        $id = $request->query->get('id');
        if ($id) {
            $brand = $this->getBrandRepository()->find($id);
            if ($brand) {
                return $this->ajaxReturn(array(
                    'id' => $brand->getId(),
                    'name' => $brand->getTitle()
                ));
            }
        } elseif ($name) {
            $brands = $this->getBrandRepository()->findBrandByTitle($name);

            $childs = array();
            if($brands)
            {
                foreach ($brands as $brand) {
                    $childs[] = array('id'=>$brand->getId(), 'name'=>$brand->getTitle());
                }

                return $this->ajaxReturn($childs);
            }
        }

        return $this->ajaxReturn(array(), 0);
    }

    /**
     * @return BrandRepository
     */
    private function getBrandRepository()
    {
        return $this->get('product.entity.brand_repository');
    }

    /**
     * @return MallRepository
     */
    private function getMallRepository()
    {
        return $this->get('product.entity.mall_repository');
    }

    /**
     * @return MallRepository
     */
    private function getAreaRepository()
    {
        return $this->get('common.entity.area_repository');
    }

    /**
     * {@inheritdoc}
     * 返回后台导航
     *
     * @param string $name
     * @return static|null
     */
    protected function setAdminBreadcrumb()
    {
        $this->getBreadcrumb()
            ->add('后台管理', $this->generateUrl('admin_home'))
            ->add('品牌管理', $this->generateUrl('brand_admin'));

        return $this->getBreadcrumb();
    }
}
