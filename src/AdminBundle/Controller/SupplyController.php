<?php

namespace AdminBundle\Controller;

use CoreBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use CoreBundle\Util\SingleTree;
use CoreBundle\Util\Help;
use CoreBundle\Segment\Segment;
use CoreBundle\Iconv\Iconv;
use ProductBundle\Entity\Product;
use ProductBundle\Entity\Supply;

/**
 * @Route("/admin/supply")
 */
class SupplyController extends Controller
{
    /**
     * @Route("/{status}/{page}", requirements={"status" = "publish|draft|trash", "page" = "\d+"}, defaults={"status" = "publish", "page" = 1}, name="supply_admin")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction(Request $request, $status, $page)
    {
        switch ($status) {
            case 'draft' :
                $type = Mall::STATUS_DRAFT;
                $name = '草稿箱';
                break;

            case 'trash' :
                $type = Mall::STATUS_TRASH;
                $name = '回收站';
                break;

            default:
                $type = Mall::STATUS_PUBLISH;
                $name = '已发布';
                break;
        }
        $title = $request->query->get('title');
        $categoryId = $request->query->get('cid');

        $this->setAdminBreadcrumb()->add($name);

        $qb = $this->getSupplyRepository()->createQueryByStatus($type);

        $categories = $this->getCategories();
        if ($categoryId) {
            $category = $categories[$categoryId];
            if (!$category) {
                throw $this->createNotFoundException('没有找到对应的栏目');
            }
            if ($category['parented']) {
                $categoryIds = explode(',', trim($category['arrChild'], ','));

                $qb = $this->getSupplyRepository()->addQueryByCategories($qb, $categoryIds);
            } else {
                $qb = $this->getSupplyRepository()->addQueryByCategory($qb, $categoryId);
            }
        }
        if ($title) {
            $qb = $this->getSupplyRepository()->addQueryByTitle($qb, $title);
        }

        $pagination = $this->paginate($qb, $page, 3);

        $categories = $this->getcategoryRepository()->findCategoryByModule('mall');

        return array(
            'pagination' => $pagination,
            'categories' => SingleTree::tree($categories, 0),
            'categoryId' => $categoryId,
            'status' => $status,
            'name' => $name
        );
    }

    /**
     * @Route("/add", name="supply_add")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function addAction(Request $request)
    {
        $product = $this->getSupplyRepository()->createNew();

        $actionName = '发布商品';
        $actionUrl = $this->generateUrl('supply_add');
        $brandId = $request->query->get('brandId');
        $categoryId = $request->query->get('categoryId');

        if ($brandId) {
            $brand = $this->getBrandRepository()->find($brandId);
            $product->setBrandId($brand->getId());
        }

        if ($categoryId) {
            $category = $this->getCategoryRepository()->find($categoryId);
            $product->setCategoryId($category->getId());
            $arrParent = $category->getArrparent();
        } else {
            $arrParent = 0;
        }

        if (!$product->getName()) {
            $name = $this->randomName();
            $product->setName($name);
        }

        $form = $this->createBoundObjectForm($product, 'edit');

        if ($form->isBound() && $form->isValid()) {
            $title = $product->getTitle();
            $content = $product->getContent();
            //自动提取SEO标题
            if (!$product->getMetaTitle()) {
                $product->setMetaTitle($title);
            }
            //自动提取SEO关键字
            if (!!$product->getKeywords()) {
                $product->setKeywords(Segment::phpAnalysis($title));
            }
            
            $product->setUser($this->getUser());

            $categoryId = $product->getCategoryId();
            $category = $this->getCategoryRepository()->find($categoryId);
            $product->setCategory($category);

            $brandId = $product->getBrandId();
            $brand = $this->getBrandRepository()->find($brandId);
            $product->setBrand($brand);

            $this->persist($product, true);

            $supply = $this->getSupplyRepository()->createNew();
            $supply->setName($form->get('model')->getData());
            $supply->setResource($form->get('resource')->getData());
            $supply->setStatus($product->getStatus());
            $supply->setPrice($product->getBuiingPrice());
            $supply->setStock($product->getStock());
            $supply->setMemo($product->getDescription());
            $supply->setProduct($product);
            $supply->setBrand($brand);
            $supply->setCategory($category);

            $this->persist($supply, true);

            $product->setSupplyId($supply->getId());
            $this->persist($product, true);

            $this->addFlash($name . '成功');

            return $this->redirectToRoute('supplyadmin');
        }

        $this->setAdminBreadcrumb()->add($actionName);

        return array(
            'form' => $form->createView(),
            'arrParent' => $arrParent,
            'actionName' => $actionName,
            'actionUrl' => $actionUrl
        );
    }

    /**
     * @Route("/edit/{id}", name="supply_edit")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function editAction(Request $request, $id)
    {
        $product = $this->getSupplyRepository()->find($id);
        if (!$product) {
            throw new NotFoundHttpException;
        }

        $actionName = '编辑商品';
        $actionUrl = $this->generateUrl('supply_edit', array('id' => $id));

        $oldBrandId = $product->getBrandId();
        $oldCategoryId = $product->getCategoryId();
        $arrParent = $product->getCategory()->getArrparent();
        $supplyId = $product->getSupplyId();

        $supply = $this->getSupplyRepository()->find($supplyId);

        $form = $this->createBoundObjectForm($product, 'edit');

        if (!$form->get('model')->getData()) {
            $form->get('model')->setData($supply->getName());
        }
        if (!$form->get('resource')->getData()) {
            $form->get('resource')->setData($supply->getResource());
        }

        if ($form->isBound() && $form->isValid()) { 
            $brandId = $product->getBrandId();
            if ($brandId != $oldBrandId) {
                $this->remove($supply, true);
                //$product->removeSupply($supply);
                $supply = $this->getSupplyRepository()->createNew();

                $brand = $this->getBrandRepository()->find($brandId);
                $product->setBrand($brand);
                $supply->setBrand($brand);
                $supply->setCategory($product->getCategory());
            }

            $categoryId = $product->getCategoryId();
            if ($categoryId != $oldCategoryId) {
                $category = $this->getCategoryRepository()->find($categoryId);
                $product->setCategory($category);
                $supply->setCategory($category);
            }

            $this->persist($product, true);

            $supply->setName($form->get('model')->getData());
            $supply->setResource($form->get('resource')->getData());
            $supply->setStatus($product->getStatus());
            $supply->setPrice($product->getBuiingPrice());
            $supply->setStock($product->getStock());
            $supply->setProduct($product);

            $this->persist($supply, true);

            if (!$supplyId != $supply->getId()) {
                $product->setSupplyId($supply->getId());
                $this->persist($product, true);
            }

            $this->addFlash($name . '成功');

            return $this->redirectToRoute('supplyadmin');
        }

        $this->setAdminBreadcrumb()->add($actionName);

        return $this->render('AdminBundle:Product:add.html.twig', array(
            'form' => $form->createView(),
            'arrParent' => $arrParent,
            'actionName' => $actionName,
            'actionUrl' => $actionUrl
        ));
    }

    /**
     * @Route("/ajaxedit", name="supply_ajaxedit")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function ajaxEditAction()
    {
        return $this->ajaxEdit($this->getSupplyRepository());
    }

    /**
     * @Route("/republish/{id}", name="supply_publish")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function publishAction($id)
    {
        $mall = $this->getSupplyRepository()->find($id);
        if (!$mall) {
            throw $this->createNotFoundException('不存在的记录');
        }

        $status = $mall->getStatus();
        if ($status !== Mall::STATUS_PUBLISH) {
            $mall->setStatus(Mall::STATUS_PUBLISH);
            $this->persist($mall, true);
        }

        return $this->ajaxReturn('成功发布' . $mall->getTitle());
    }

    /**
     * @Route("/delete/{id}", name="supply_delete")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deleteAction($id)
    {
        $mall = $this->getSupplyRepository()->find($id);
        if (!$mall) {
            throw $this->createNotFoundException('不存在的记录');
        }

        $status = $mall->getStatus();
        switch ($status) {
            case Mall::STATUS_TRASH :
                //TODO FMA 关联表删除的判断
                $this->remove($mall, true);
                break;

            default:
                $mall->setStatus(Mall::STATUS_TRASH);
                $this->persist($mall, true);
                break;
        }

        return $this->ajaxReturn('成功删除' . $mall->getTitle());
    }

    /**
     * @return SupplyRepository
     */
    private function getSupplyRepository()
    {
        return $this->get('product.entity.supply_repository');
    }

    /**
     * @return ProductRepository
     */
    private function getProductRepository()
    {
        return $this->get('product.entity.supplyrepository');
    }

    /**
     * @return BrandRepository
     */
    private function getBrandRepository()
    {
        return $this->get('product.entity.brand_repository');
    }

    /**
     * @return MallRepository
     */
    private function getMallRepository()
    {
        return $this->get('product.entity.supplyrepository');
    }

    /**
     * {@inheritdoc}
     * 返回后台导航
     *
     * @param string $name
     * @return static|null
     */
    protected function setAdminBreadcrumb()
    {
        $this->getBreadcrumb()
            ->add('后台管理', $this->generateUrl('admin_home'))
            ->add('供应管理', $this->generateUrl('supplyadmin'));

        return $this->getBreadcrumb();
    }
}
