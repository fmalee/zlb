<?php

namespace AdminBundle\Controller;

use CoreBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use CoreBundle\Util\SingleTree;
use CoreBundle\Util\Help;
use CoreBundle\Segment\Segment;
use CoreBundle\Iconv\Iconv;
use ProductBundle\Entity\Product;
use ProductBundle\Entity\Supply;

/**
 * @Route("/admin/product")
 */
class ProductController extends Controller
{
    /**
     * @Route("/{status}/{page}", requirements={"status" = "publish|draft|trash", "page" = "\d+"}, defaults={"status" = "publish", "page" = 1}, name="product_admin")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction(Request $request, $status, $page)
    {
        switch ($status) {
            case 'draft' :
                $type = Product::STATUS_DRAFT;
                $name = '草稿箱';
                break;

            case 'trash' :
                $type = Product::STATUS_TRASH;
                $name = '回收站';
                break;

            default:
                $type = Product::STATUS_PUBLISH;
                $name = '已发布';
                break;
        }
        $title = $request->query->get('title');
        $categoryId = $request->query->get('cid');

        $this->setAdminBreadcrumb()->add($name);

        $qb = $this->getProductRepository()->createQueryByStatus($type);

        $categories = $this->getCategories();
        if ($categoryId) {
            $category = $categories[$categoryId];
            if (!$category) {
                throw $this->createNotFoundException('没有找到对应的栏目');
            }
            if ($category['parented']) {
                $categoryIds = explode(',', trim($category['arrChild'], ','));

                $qb = $this->getProductRepository()->addQueryByCategories($qb, $categoryIds);
            } else {
                $qb = $this->getProductRepository()->addQueryByCategory($qb, $categoryId);
            }
        }
        if ($title) {
            $qb = $this->getProductRepository()->addQueryByTitle($qb, $title);
        }

        $pagination = $this->paginate($qb, $page, 3);

        $categories = $this->getcategoryRepository()->findCategoryByModule('product');

        return array(
            'pagination' => $pagination,
            'categories' => SingleTree::tree($categories, 0),
            'categoryId' => $categoryId,
            'status' => $status,
            'name' => $name
        );
    }

    /**
     * @Route("/detail/{id}",  name="product_manage")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function detailAction(Request $request, $id)
    {
        $product = $this->getProductRepository()->find($id);
        if (!$product) {
            throw new NotFoundHttpException;
        }

        $category = $product->getCategory();
        $brand = $product->getBrand();
        $skus = $product->getSkus();
        $supplies = $product->getSupplies();
        $supplyId = $product->getSupplyId();

        $supply = $this->getSupplyRepository()->find($supplyId);

        $this->setAdminBreadcrumb()->add($product->getTitle());

        //$this->dump($skus);

        return array(
            'product' => $product,
            'category' => $category,
            'brand' => $brand,
            'skus' => $skus,
            'supplies' => $supplies,
            'supply' => $supply,
        );
    }

    /**
     * @Route("/category", name="product_category")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function categoryAction()
    {
        $this->setAdminBreadcrumb()->add('栏目管理');

        $response = $this->forward('AdminBundle:Category:list', array(
            'module'  => 'product',
            'moduleName' => '商品',
        ));

        return $response;
    }

    /**
     * @Route("/add", name="product_add")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function addAction(Request $request)
    {
        $product = $this->getProductRepository()->createNew();

        $actionName = '发布商品';
        $actionUrl = $this->generateUrl('product_add');
        $brandId = $request->query->get('brandId');
        $categoryId = $request->query->get('categoryId');

        if ($brandId) {
            $brand = $this->getBrandRepository()->find($brandId);
            $product->setBrandId($brand->getId());
        }

        if ($categoryId) {
            $category = $this->getCategoryRepository()->find($categoryId);
            $product->setCategoryId($category->getId());
            $arrParent = $category->getArrparent();
        } else {
            $arrParent = 0;
        }

        if (!$product->getName()) {
            $name = $this->randomName();
            $product->setName($name);
        }

        $form = $this->createBoundObjectForm($product, 'edit');

        if ($form->isBound() && $form->isValid()) {
            $title = $product->getTitle();
            $content = $product->getContent();
            //自动提取SEO标题
            if (!$product->getMetaTitle()) {
                $product->setMetaTitle($title);
            }
            //自动提取SEO关键字
            if (!!$product->getKeywords()) {
                $product->setKeywords(Segment::phpAnalysis($title));
            }
            
            $product->setUser($this->getUser());

            $categoryId = $product->getCategoryId();
            $category = $this->getCategoryRepository()->find($categoryId);
            $product->setCategory($category);

            $brandId = $product->getBrandId();
            $brand = $this->getBrandRepository()->find($brandId);
            $product->setBrand($brand);

            $this->persist($product, true);

            $supply = $this->getSupplyRepository()->createNew();
            $supply->setName($form->get('model')->getData());
            $supply->setResource($form->get('resource')->getData());
            $supply->setStatus($product->getStatus());
            $supply->setPrice($product->getBuiingPrice());
            $supply->setStock($product->getStock());
            $supply->setMemo($product->getDescription());
            $supply->setProduct($product);
            $supply->setBrand($brand);
            $supply->setCategory($category);

            $this->persist($supply, true);

            $product->setSupplyId($supply->getId());
            $this->persist($product, true);

            $this->addFlash($actionName . '成功');

            return $this->redirectToRoute('product_admin');
        }

        $this->setAdminBreadcrumb()->add($actionName);

        return array(
            'form' => $form->createView(),
            'arrParent' => $arrParent,
            'actionName' => $actionName,
            'actionUrl' => $actionUrl
        );
    }

    /**
     * @Route("/edit/{id}", name="product_edit")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function editAction(Request $request, $id)
    {
        $product = $this->getProductRepository()->find($id);
        if (!$product) {
            throw new NotFoundHttpException;
        }

        $actionName = '编辑商品';
        $actionUrl = $this->generateUrl('product_edit', array('id' => $id));

        $oldBrandId = $product->getBrandId();
        $oldCategoryId = $product->getCategoryId();
        $arrParent = $product->getCategory()->getArrparent();
        $supplyId = $product->getSupplyId();

        $supply = $this->getSupplyRepository()->find($supplyId);

        $form = $this->createBoundObjectForm($product, 'edit');

        if (!$form->get('model')->getData()) {
            $form->get('model')->setData($supply->getName());
        }
        if (!$form->get('resource')->getData()) {
            $form->get('resource')->setData($supply->getResource());
        }

        if ($form->isBound() && $form->isValid()) { 
            $brandId = $product->getBrandId();
            if ($brandId != $oldBrandId) {
                $this->remove($supply, true);
                //$product->removeSupply($supply);
                $supply = $this->getSupplyRepository()->createNew();

                $brand = $this->getBrandRepository()->find($brandId);
                $product->setBrand($brand);
                $supply->setBrand($brand);
                $supply->setCategory($product->getCategory());
            }

            $categoryId = $product->getCategoryId();
            if ($categoryId != $oldCategoryId) {
                $category = $this->getCategoryRepository()->find($categoryId);
                $product->setCategory($category);
                $supply->setCategory($category);
            }

            $this->persist($product, true);

            $supply->setName($form->get('model')->getData());
            $supply->setResource($form->get('resource')->getData());
            $supply->setStatus($product->getStatus());
            $supply->setPrice($product->getBuiingPrice());
            $supply->setStock($product->getStock());
            $supply->setProduct($product);

            $this->persist($supply, true);

            if (!$supplyId != $supply->getId()) {
                $product->setSupplyId($supply->getId());
                $this->persist($product, true);
            }

            $this->addFlash($actionName . '成功');

            return $this->redirectToRoute('product_admin');
        }

        $this->setAdminBreadcrumb()->add($actionName);

        return $this->render('AdminBundle:Product:add.html.twig', array(
            'form' => $form->createView(),
            'arrParent' => $arrParent,
            'actionName' => $actionName,
            'actionUrl' => $actionUrl
        ));
    }

    /**
     * @Route("/ajaxedit", name="product_ajaxedit")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function ajaxEditAction()
    {
        return $this->ajaxEdit($this->getProductRepository());
    }

    /**
     * @Route("/republish/{id}", name="product_publish")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function publishAction($id)
    {
        $product = $this->getProductRepository()->find($id);
        if (!$product) {
            throw $this->createNotFoundException('不存在的记录');
        }

        $status = $product->getStatus();
        if ($status !== Product::STATUS_PUBLISH) {
            $product->setStatus(Product::STATUS_PUBLISH);
            $this->persist($product, true);
        }

        return $this->ajaxReturn('成功发布' . $product->getTitle());
    }

    /**
     * @Route("/delete/{id}", name="product_delete")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deleteAction($id)
    {
        $product = $this->getProductRepository()->find($id);
        if (!$product) {
            throw $this->createNotFoundException('不存在的记录');
        }

        $status = $product->getStatus();
        switch ($status) {
            case Product::STATUS_TRASH :
                //TODO FMA 关联表删除的判断
                $supplies = $this->getSupplyRepository()->finByProductId($id);
                if ($supplies) {
                    $this->remove($supplies, true);
                }
                $this->remove($product, true);
                break;

            default:
                $product->setStatus(Product::STATUS_TRASH);
                $this->persist($product, true);
                break;
        }

        return $this->ajaxReturn('成功删除' . $product->getTitle());
    }

    /**
     * 随机生成编码
     * @param $str 已经存在的编码 如A01,Z99
     */
    public function randomName($str = array())
    {
        $numbers = rand(01, 99); // 生成随机数字
        $alphabets = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'X', 'Y', 'Z');
        $arr = rand(0, 23); // 生成随机数组指标
        $serial = $alphabets[$arr] . sprintf("%02d", $numbers);
        if (!array_key_exists($serial, $str)) {
            $exist = $this->getProductRepository()->findOneByName($serial);
            if (!$exist) {
                return $serial;
            }
        }
        $str[$serial] = 1; // 记录存在编码
        $this->randomName($str); // 进行递归
    }

    /**
     * @return ProductRepository
     */
    private function getProductRepository()
    {
        return $this->get('product.entity.product_repository');
    }

    /**
     * @return BrandRepository
     */
    private function getBrandRepository()
    {
        return $this->get('product.entity.brand_repository');
    }

    /**
     * @return MallRepository
     */
    private function getMallRepository()
    {
        return $this->get('product.entity.product_repository');
    }

    /**
     * @return SupplyRepository
     */
    private function getSupplyRepository()
    {
        return $this->get('product.entity.supply_repository');
    }

    /**
     * {@inheritdoc}
     * 返回后台导航
     *
     * @param string $name
     * @return static|null
     */
    protected function setAdminBreadcrumb()
    {
        $this->getBreadcrumb()
            ->add('后台管理', $this->generateUrl('admin_home'))
            ->add('商品管理', $this->generateUrl('product_admin'));

        return $this->getBreadcrumb();
    }
}
