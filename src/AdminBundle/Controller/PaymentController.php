<?php

namespace AdminBundle\Controller;

use CoreBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/admin/payment")
 */
class PaymentController extends Controller
{
    /**
     * @Route("/", name="payment_admin")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction()
    {
        $this->getBreadcrumb()->add('后台管理', $this->generateUrl('admin_home'))->add('支付方式');

        $payments = $this->getPaymentRepository()->findAll();

        return array('payments' => $payments);
    }

    /**
     * @Route("/add", name="payment_add")
     * @Route("/edit/{id}", name="payment_edit")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function editAction(Request $request, $id = null)
    {
       $template = 'AdminBundle:Payment:edit.html.twig';

       return $this->edit($request, $this->getPaymentRepository(), $template, $id);
    }

    /**
     * @Route("/ajaxedit", name="payment_ajaxedit")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function ajaxEditAction()
    {
        return $this->ajaxEdit($this->getPaymentRepository());
    }

    /**
     * @return PaymentRepository
     */
    private function getPaymentRepository()
    {
        return $this->get('trade.entity.payment_repository');
    }
}
