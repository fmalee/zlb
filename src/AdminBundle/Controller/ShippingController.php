<?php

namespace AdminBundle\Controller;

use CoreBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/admin/shipping")
 */
class ShippingController extends Controller
{
    /**
     * @Route("/", name="shipping_admin")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction()
    {
        $this->getBreadcrumb()->add('后台管理', $this->generateUrl('admin_home'))->add('配送方式');

        $shippings = $this->getShippingRepository()->findAll();

        return array('shippings' => $shippings);
    }

    /**
     * @Route("/add", name="shipping_add")
     * @Route("/edit/{id}", name="shipping_edit")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function editAction(Request $request, $id = null)
    {
       $template = 'AdminBundle:Shipping:edit.html.twig';

       return $this->edit($request, $this->getShippingRepository(), $template, $id);
    }

    /**
     * @Route("/ajaxedit", name="shipping_ajaxedit")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function ajaxEditAction()
    {
        return $this->ajaxEdit($this->getShippingRepository());
    }

    /**
     * @return ShippingRepository
     */
    private function getShippingRepository()
    {
        return $this->get('trade.entity.shipping_repository');
    }
}
