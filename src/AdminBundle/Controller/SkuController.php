<?php

namespace AdminBundle\Controller;

use CoreBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use CoreBundle\Util\SingleTree;
use CoreBundle\Util\Help;
use CoreBundle\Segment\Segment;
use CoreBundle\Iconv\Iconv;
use ProductBundle\Entity\Product;
use ProductBundle\Entity\Supply;

/**
 * @Route("/admin/sku")
 */
class SkuController extends Controller
{
    /**
     * @Route("/{status}/{page}", requirements={"status" = "publish|draft|trash", "page" = "\d+"}, defaults={"status" = "publish", "page" = 1}, name="sku_admin")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction(Request $request, $status, $page)
    {
        switch ($status) {
            case 'draft' :
                $type = Product::STATUS_DRAFT;
                $name = '草稿箱';
                break;

            case 'trash' :
                $type = Product::STATUS_TRASH;
                $name = '回收站';
                break;

            default:
                $type = Product::STATUS_PUBLISH;
                $name = '已发布';
                break;
        }
        $title = $request->query->get('title');
        $categoryId = $request->query->get('cid');

        $this->setAdminBreadcrumb()->add($name);

        $qb = $this->getSkuRepository()->createQueryByStatus($type);

        $categories = $this->getCategories();
        if ($categoryId) {
            $category = $categories[$categoryId];
            if (!$category) {
                throw $this->createNotFoundException('没有找到对应的栏目');
            }
            if ($category['parented']) {
                $categoryIds = explode(',', trim($category['arrChild'], ','));

                $qb = $this->getSkuRepository()->addQueryByCategories($qb, $categoryIds);
            } else {
                $qb = $this->getSkuRepository()->addQueryByCategory($qb, $categoryId);
            }
        }
        if ($title) {
            $qb = $this->getSkuRepository()->addQueryByTitle($qb, $title);
        }

        $pagination = $this->paginate($qb, $page, 3);

        $categories = $this->getcategoryRepository()->findCategoryByModule('product');

        return array(
            'pagination' => $pagination,
            'categories' => SingleTree::tree($categories, 0),
            'categoryId' => $categoryId,
            'status' => $status,
            'name' => $name
        );
    }

    /**
     * @Route("/add/{productId}", name="sku_add")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function addAction(Request $request, $productId)
    {
        $product = $this->getProductRepository()->find($productId);
        if (!$product) {
            throw new NotFoundHttpException;
        }

        if (!count($product->getSupplies())) {
            return $this->ajaxReturn('请先给产品添加供应商');
        }

        $supplyId = $product->getSupplyId();
        $supplies = array();
        foreach ($product->getSupplies() as $supply) {
            $supplies[$supply->getId()] = $supply->getBrand()->getTitle().'-'.$supply->getName();
        }

        if ($request->getMethod() == 'POST') {
            $skus = $request->get('skus');
            if(count($skus['sku'])) {
                $stock = $product->getStock();
                foreach ($skus['sku'] as $key=>$sku) {
                    if(!$sku['name']) {
                        continue;
                    }
                    $entity = $this->getSkuRepository()->createNew();
                    $entity->setName($sku['name']);
                    $entity->setSku1($sku['sku_1']);
                    if (isset($sku['sku_2'])) {
                        $entity->setSku2($sku['sku_2']);
                    }
                    $entity->setSellingPrice($product->getSellingPrice());
                    $entity->setCurrentPrice($product->getCurrentPrice());
                    $entity->setBuiingPrice($sku['buiingPrice']);
                    $entity->setStock($sku['stock']);
                    $entity->setProduct($product);

                    $supply = $this->getSupplyRepository()->find($sku['supplyId']);
                    if ($supply) {
                        $entity->setSupply($supply);
                    } else {
                        continue;
                    }

                    $stock = $stock + $sku['stock'];

                    $this->persist($entity, true);

                    $product->setStock($stock);
                    $this->persist($product, true);

                    $this->ajaxReturn('添加成功');
                }
            } else {
                $this->ajaxReturn('请先填写相关内容', false);
            }
        }

        $content = $this->renderView('AdminBundle:Sku:add.html.twig', array(
            'supplies' => $supplies,
            'productId' => $productId,
            'supplyId' => $supplyId,
            'product' => $product
        ));
        return $this->ajaxReturn($content, null);
    }

    /**
     * @Route("/edit/{id}", name="sku_edit")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function editAction(Request $request, $id)
    {
        $sku = $this->getSkuRepository()->find($id);
        if (!$sku) {
            throw new NotFoundHttpException;
        }
        $product = $this->getProductRepository()->find($sku->getProductId());
        if (!$product) {
            throw new NotFoundHttpException;
        }

        if (!count($product->getSupplies())) {
            return $this->ajaxReturn('请先给产品添加供应商');
        }

        $supplyId = $sku->getSupplyId();
        $supplies = array();
        foreach ($product->getSupplies() as $supply) {
            $supplies[$supply->getId()] = $supply->getBrand()->getTitle().'-'.$supply->getName().'-'.$supply->getPrice();
        }

        $form = $this->createBoundObjectForm($sku, 'edit');

        if ($form->isBound() && $form->isValid()) {
            $newSupplyId = $sku->getSupplyId();
            if ($newSupplyId != $supplyId) {    
                $supply = $this->getSupplyRepository()->find($newSupplyId);

                $sku->setSupply($supply);
            }

            $this->persist($sku, true);

            $this->addFlash('编辑销售属性成功');

            return $this->ajaxReturn('编辑销售属性成功');
        }

        $content = $this->renderView('AdminBundle:Sku:edit.html.twig', array(
            'form' => $form->createView(),
            'id' => $id,
            'supplies' => $supplies,
            'supplyId' => $supplyId,
            'product' => $product
        ));
        return $this->ajaxReturn($content, null);
    }

    /**
     * @Route("/ajaxedit", name="sku_ajaxedit")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function ajaxEditAction()
    {
        return $this->ajaxEdit($this->getSkuRepository());
    }

    /**
     * @Route("/delete/{id}", name="sku_delete")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deleteAction($id)
    {
        $sku = $this->getSkuRepository()->find($id);
        if (!$sku) {
            throw $this->createNotFoundException('不存在的记录');
        }
        $this->remove($sku, true);

        return $this->ajaxReturn('成功删除' . $sku->getName());
    }

    /**
     * @return SkuRepository
     */
    private function getSkuRepository()
    {
        return $this->get('product.entity.sku_repository');
    }

    /**
     * @return ProductRepository
     */
    private function getProductRepository()
    {
        return $this->get('product.entity.product_repository');
    }

    /**
     * @return BrandRepository
     */
    private function getBrandRepository()
    {
        return $this->get('product.entity.brand_repository');
    }

    /**
     * @return MallRepository
     */
    private function getMallRepository()
    {
        return $this->get('product.entity.sku_repository');
    }

    /**
     * @return SupplyRepository
     */
    private function getSupplyRepository()
    {
        return $this->get('product.entity.supply_repository');
    }

    /**
     * {@inheritdoc}
     * 返回后台导航
     *
     * @param string $name
     * @return static|null
     */
    protected function setAdminBreadcrumb()
    {
        $this->getBreadcrumb()
            ->add('后台管理', $this->generateUrl('admin_home'))
            ->add('商品管理', $this->generateUrl('product_admin'))
            ->add('属性管理', $this->generateUrl('sku_admin'));

        return $this->getBreadcrumb();
    }
}
