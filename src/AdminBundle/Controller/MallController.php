<?php

namespace AdminBundle\Controller;

use CoreBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use CoreBundle\Util\SingleTree;
use ProductBundle\Entity\Mall;

/**
 * @Route("/admin/mall")
 */
class MallController extends Controller
{
    /**
     * @Route("/{status}/{page}", requirements={"status" = "publish|draft|trash", "page" = "\d+"}, defaults={"status" = "publish", "page" = 1}, name="mall_admin")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction(Request $request, $status, $page)
    {
        switch ($status) {
            case 'draft' :
                $type = Mall::STATUS_DRAFT;
                $name = '草稿箱';
                break;

            case 'trash' :
                $type = Mall::STATUS_TRASH;
                $name = '回收站';
                break;

            default:
                $type = Mall::STATUS_PUBLISH;
                $name = '已发布';
                break;
        }
        $title = $request->query->get('title');
        $categoryId = $request->query->get('cid');

        $this->setAdminBreadcrumb()->add($name);

        $qb = $this->getMallRepository()->createQueryByStatus($type);

        $categories = $this->getCategories();
        if ($categoryId) {
            $category = $categories[$categoryId];
            if (!$category) {
                throw $this->createNotFoundException('没有找到对应的栏目');
            }
            if ($category['parented']) {
                $categoryIds = explode(',', trim($category['arrChild'], ','));

                $qb = $this->getMallRepository()->addQueryByCategories($qb, $categoryIds);
            } else {
                $qb = $this->getMallRepository()->addQueryByCategory($qb, $categoryId);
            }
        }
        if ($title) {
            $qb = $this->getMallRepository()->addQueryByTitle($qb, $title);
        }

        $pagination = $this->paginate($qb, $page, 3);

        $categories = $this->getcategoryRepository()->findCategoryByModule('mall');

        return array(
            'pagination' => $pagination,
            'categories' => SingleTree::tree($categories, 0),
            'categoryId' => $categoryId,
            'status' => $status,
            'name' => $name
        );
    }

    /**
     * @Route("/category", name="mall_category")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function categoryAction()
    {
        $this->setAdminBreadcrumb()->add('栏目管理');

        $response = $this->forward('AdminBundle:Category:list', array(
            'module'  => 'mall',
            'moduleName' => '商城',
        ));

        return $response;
    }

    /**
     * @Route("/add/{categoryId}", name="mall_add")
     * @Route("/edit/{id}", name="mall_edit")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function editAction(Request $request, $id = null, $categoryId = null)
    {
        $arrParent = array();
        if ($id) {
            $mall = $this->getMallRepository()->find($id);
            if (!$mall) {
                throw new NotFoundHttpException;
            }
            //$category = $this->getCategories();
            $category = $mall->getCategory();
            $arrParent['category'] = $category->getArrparent();
            $area = $mall->getArea();
            $arrParent['area'] = trim($area->getArrparent() . $mall->getAreaId(), ',');
        } else {
            $mall = $this->getMallRepository()->createNew();
            $arrParent = array('category' => 0, 'area' => 0);
        }
        //$this->dump($mall);

        $form = $this->createBoundObjectForm($mall, 'edit');

        if ($form->isBound() && $form->isValid()) {
            if (!$id) {
                $categoryId = $mall->getCategoryId();
                $category = $this->getCategoryRepository()->find($categoryId);
                $mall->setCategory($category);

                $areaId = $mall->getAreaId();
                $area = $this->getAreaRepository()->find($areaId);
                $mall->setArea($area);
            }

            $this->persist($mall, true);

            $this->addFlash('编辑商城信息成功');

            return $this->redirectToRoute('mall_admin');
        }

        $this->setAdminBreadcrumb()->add('编辑商城信息');

        return array(
            'form' => $form->createView(),
            'id' => $id,
            'arrParent' => $arrParent
        );
    }

    /**
     * @Route("/ajaxedit", name="mall_ajaxedit")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function ajaxEditAction()
    {
        return $this->ajaxEdit($this->getMallRepository());
    }

    /**
     * @Route("/republish/{id}", name="mall_publish")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function publishAction($id)
    {
        $mall = $this->getMallRepository()->find($id);
        if (!$mall) {
            throw $this->createNotFoundException('不存在的记录');
        }

        $status = $mall->getStatus();
        if ($status !== Mall::STATUS_PUBLISH) {
            $mall->setStatus(Mall::STATUS_PUBLISH);
            $this->persist($mall, true);
        }

        return $this->ajaxReturn('成功发布' . $mall->getTitle());
    }

    /**
     * @Route("/delete/{id}", name="mall_delete")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deleteAction($id)
    {
        $mall = $this->getMallRepository()->find($id);
        if (!$mall) {
            throw $this->createNotFoundException('不存在的记录');
        }

        $status = $mall->getStatus();
        switch ($status) {
            case Mall::STATUS_TRASH :
                //TODO FMA 关联表删除的判断
                $this->remove($mall, true);
                break;

            default:
                $mall->setStatus(Mall::STATUS_TRASH);
                $this->persist($mall, true);
                break;
        }

        return $this->ajaxReturn('成功删除' . $mall->getTitle());
    }

    /**
     * @return MallRepository
     */
    private function getMallRepository()
    {
        return $this->get('product.entity.mall_repository');
    }

    /**
     * @return MallRepository
     */
    private function getAreaRepository()
    {
        return $this->get('common.entity.area_repository');
    }

    /**
     * {@inheritdoc}
     * 返回后台导航
     *
     * @param string $name
     * @return static|null
     */
    protected function setAdminBreadcrumb()
    {
        $this->getBreadcrumb()
            ->add('后台管理', $this->generateUrl('admin_home'))
            ->add('商城管理', $this->generateUrl('mall_admin'));

        return $this->getBreadcrumb();
    }
}
