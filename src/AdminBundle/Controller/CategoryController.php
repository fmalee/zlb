<?php

namespace AdminBundle\Controller;

use CoreBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use CoreBundle\Util\SingleTree;
use CoreBundle\Segment\Segment;
use CoreBundle\Iconv\Iconv;

/**
 * @Route("/admin/category")
 */
class CategoryController extends Controller
{
    /**
     * @Route("/", name="category_admin")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }

    /**
     * 通用栏目管理
     */
    public function listAction($module, $moduleName, $template = null)
    {
        $categories = $this->getCategoryRepository()->findCategoryByModule($module);

        $categories = SingleTree::tree($categories, 0);
        //$this->dump($categories);

        $template = $template ?: 'AdminBundle:Category:list.html.twig'; 

        return $this->render($template, array(
            'categories' => $categories,
            'module' => $module,
            'moduleName' => $moduleName
        ));
    }

    /**
     * @Route("/add/{module}/{parentId}", name="category_add")
     * @Route("/edit/{module}/{id}", name="category_edit")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function editAction(Request $request, $module, $parentId = null, $id = null)
    {
         $categories = $this->getCategories();

        if ($id) {
            if (!array_key_exists($id, $categories) || $categories[$id]['module'] != $module) {
                throw $this->createNotFoundException('模型不对');
            }

            $category = $this->getCategoryRepository()->find($id);
        } else {
            $category = $this->getcategoryRepository()->createNew();
        }

        if (is_null($parentId)) {
            $parentId = $category->getParentId();
        }
        if ($parentId != 0 && $categories[$parentId]['module'] != $module) {
            throw new NotFoundHttpException;
        }

        $arrParent = $parentId ? $categories[$parentId]['arrParent'] : 0;
        //$this->dump($categories[$parentId]['arrParent']);

        if (!$id) {
            $category->setParentId($parentId);
            $category->setModule($module);
            if ($parentId <> 0) {
                $parent = $categories[$parentId];

                $category->setPageSize($parent['pageSize']);
                $category->setReply($parent['reply']);
                $category->setTemplateIndex($parent['templateIndex']);
                $category->setTemplateList($parent['templateList']);
                $category->setTemplateDetail($parent['templateDetail']);
            }
        }

        $form = $this->createBoundObjectForm($category, 'edit');
        if ($request->getMethod() != 'POST') {
            $title = $form->get('title')->getData();
            $description = $form->get('description')->getData();
            //自动提取SEO标题
            if (!$form->get('metaTitle')->getData()) {
                $form->get('metaTitle')->setData($title);
            }
            //自动提取SEO关键字
            if (!$form->get('keywords')->getData()) {
                $keywords = $description ?: $title;
                $keywords = Segment::phpAnalysis($title);
                $form->get('keywords')->setData($keywords);
            }
            //自动提取栏目标识
            if (!$form->get('name')->getData()) {
                $name = Iconv::gbk_to_pinyin($title);
                $form->get('name')->setData(implode($name));
            }
        }

        if ($form->isBound() && $form->isValid()) {

            $this->persist($category, true);

            if ($id && $parentId != $categories[$id]['parentId']) {
                /*修复层级关系*/
                $repair = $this->forward('AdminBundle:Category:repair');
            }
            /*重新缓存修复后的栏目*/
            $this->getcategoryRepository()->cacheCategories();

            return $this->ajaxReturn('编辑成功');
        }

        $content = $this->renderView('AdminBundle:Category:edit.html.twig', array(
            'form' => $form->createView(),
            'module' => $module,
            'arrParent' => $arrParent,
            'id' => $id
        ));
        return $this->ajaxReturn($content, null);
    }

    /**
     * @Route("/ajaxedit", name="category_ajaxedit")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function ajaxEditAction()
    {
        return $this->ajaxEdit($this->getCategoryRepository());
    }

    /**
     * @Route("/childs/{module}", name="category_childs")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function childsAction(Request $request, $module)
    {
        $id = $request->query->get('id') ?: 0;
        $categories = $this->getCategoryRepository()->findChildrenByModule($module, $id, true);

        if ($categories) {
            $childs = array();
            foreach ($categories as $key=>$value) {
                $childs[$key]['id'] = $value['id'];
                $childs[$key]['name'] = $value['title'];
            }

            return $this->ajaxReturn($childs);
        }

        return $this->ajaxReturn('没有记录', false);
    }

    /**
     * @Route("/delete/{id}", name="category_delete")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deleteAction($id)
    {
        $category = $this->getCategoryRepository()->find($id);
        if (!$category) {
            throw $this->createNotFoundException('栏目不存在');
        }
        if ($category->getParented()) {
            return $this->ajaxReturn('栏目包含下级栏目，不能删除', false);
        }
        $module = 'get'. ucfirst($category->getModule()) . 's()';
        //TODO FMA 判断是否有文章
        //$modules = $category->$module;
        //$modules = $category->getMalls();

        //$this->remove($category, true);

        return $this->ajaxReturn('成功删除' . $category->getTitle());
    }

    /**
     * @Route("/repair", name="category_repair")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function repairAction()
    {
        $categories = $this->getCategoryRepository()->findAll();
        /*修复层级关系*/
        $this->getcategoryRepository()->repair($categories);
        /*重新缓存修复后的栏目*/
        $this->getcategoryRepository()->cacheCategories();

        $this->addFlash('完成栏目缓存更新');

        return $this->ajaxReturn('修复成功');
    }
}
