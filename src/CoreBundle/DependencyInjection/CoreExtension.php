<?php

namespace CoreBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\Kernel;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class CoreExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));

        $loader->load('bundle.yml');
        $loader->load('controller_helper.yml');
        $loader->load('util.yml');
        $loader->load('twig.yml');

        foreach ($config['detect'] as $type => $isActivated) {
            $container->setParameter('core.detect.'.$type, $isActivated);
        }

        if ($config['breadcrumb']) {
            $loader->load('breadcrumb.yml');
        }
        if ($config['mailer']['logger']) {
            $loader->load('mailer_logger.yml');
        }
        if ($config['mailer']['message_factory']) {
            $loader->load('mailer_message_factory.yml');
        }
        if ($config['listener']['orm_user']) {
            $loader->load('orm_user_listener.yml');
        }
        if ($config['form_manager']) {
            $loader->load('form_manager.yml');
        }
        if ($config['security_voter']) {
            $loader->load('security_voter.yml');
        }
        if ($config['datatable']) {
            $loader->load('datatable.yml');
        }
        $container->setParameter('core.flashes.trans_catalog', $config['flashes']['trans_catalog']);
        if ($this->isConfigEnabled($container, $config['flashes'])) {
            $loader->load('flashes.yml');
        }

        $container->setParameter('core.decision_manager.id', $config['security']['decision_manager']);
        if ($config['cache']) {
            $loader->load('cache.yml');
        }
        $container->setParameter('core.cache.client', $config['cache']['client']);
        $container->setParameter('core.cache.ttl', $config['cache']['ttl']);
        $container->setParameter('core.cache.debug', $config['cache']['debug']);
        $container->setParameter('core.cache.memcache.ip', $config['cache']['parameters']['memcache']['ip']);
        $container->setParameter('core.cache.memcache.port', $config['cache']['parameters']['memcache']['port']);
        $container->setParameter('core.cache.filecache.path', $config['cache']['parameters']['filecache']['path']);

    }

    public function getNamespace()
    {
        return 'http://knplabs.com/schema/dic/rad';
    }
}
