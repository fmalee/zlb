<?php

namespace CoreBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Cache
 *
 * @package
 * @version $id$
 * @author Yaroslav Nechaev <mail@remper.ru>
 * @license See LICENSE.md
 */
class RegisterCacheClientPass implements CompilerPassInterface {

    public function process(ContainerBuilder $container)
    {
        $def = $container->findDefinition('core.cache');
        $reference = new Reference('core.cache.client.' . $container->getParameter('core.cache.client'));
        $def->addMethodCall('setClient', array($reference));
    }
}
