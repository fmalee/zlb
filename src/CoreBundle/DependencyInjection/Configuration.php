<?php

namespace CoreBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('core');

        $rootNode
            ->children()
                ->arrayNode('detect')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->booleanNode('entity')->defaultTrue()->end()
                        ->booleanNode('form_creator')->defaultTrue()->end()
                        ->booleanNode('form_extension')->defaultTrue()->end()
                        ->booleanNode('form_type')->defaultTrue()->end()
                        ->booleanNode('twig')->defaultTrue()->end()
                        ->booleanNode('security_voter')->defaultTrue()->end()
                        ->booleanNode('validator_constraint')->defaultTrue()->end()
                    ->end()
                ->end()
                ->arrayNode('cache')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('client')->defaultValue('filecache')->end()
                        ->scalarNode('ttl')->defaultValue(300)->end()
                        ->arrayNode('parameters')
                            ->addDefaultsIfNotSet()
                            ->children()
                                ->arrayNode('memcache')
                                    ->addDefaultsIfNotSet()
                                    ->children()
                                        ->scalarNode('ip')->defaultValue('127.0.0.1')->end()
                                        ->scalarNode('port')->defaultValue(11211)->end()
                                    ->end()
                                ->end()
                                ->arrayNode('filecache')
                                    ->addDefaultsIfNotSet()
                                    ->children()
                                        ->scalarNode('path')->defaultValue('%kernel.cache_dir%/apc')->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                        ->booleanNode('debug')->defaultValue('%kernel.debug%')->end()
                    ->end()
                ->end()
                ->arrayNode('mailer')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->booleanNode('logger')->defaultFalse()->end()
                        ->booleanNode('message_factory')->defaultTrue()->end()
                    ->end()
                ->end()
                ->arrayNode('listener')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->booleanNode('orm_user')->defaultTrue()->end()
                    ->end()
                ->end()
                ->booleanNode('form_manager')->defaultTrue()->end()
                ->booleanNode('breadcrumb')->defaultTrue()->end()
                ->booleanNode('security_voter')->defaultTrue()->end()
                ->booleanNode('datatable')->defaultTrue()->end()
                ->arrayNode('flashes')
                    ->addDefaultsIfNotSet()
                    ->canBeDisabled()
                    ->children()
                        ->scalarNode('trans_catalog')->defaultValue('messages')->end()
                    ->end()
                ->end()
                ->arrayNode('security')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('decision_manager')->defaultValue('security.access.decision_manager')->end()
                    ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
