<?php

namespace CoreBundle\Security;

interface RecoverableUserInterface extends UserInterface
{
    public function erasePasswordRecoveryKey();
}
