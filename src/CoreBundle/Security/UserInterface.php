<?php

namespace CoreBundle\Security;

use Symfony\Component\Security\Core\User\UserInterface as BaseUserInterface;

interface UserInterface extends BaseUserInterface
{
    public function setPassword($password);

    public function getPlainPassword();
}
