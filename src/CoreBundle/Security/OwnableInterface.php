<?php

namespace CoreBundle\Security;

interface OwnableInterface
{
    public function getOwner();
}
