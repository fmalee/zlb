<?php

namespace CoreBundle\Mailer;

use Swift_Mailer;
use Swift_Mime_Message;
use Twig_Environment;
use CoreBundle\AppBundle\BundleGuesser;

class MessageFactory
{
    private $mailer;
    private $twig;
    private $bundleGuesser;
    private $from;

    public function __construct(Swift_Mailer $mailer, Twig_Environment $twig, BundleGuesser $bundleGuesser, $from)
    {
        $this->mailer     = $mailer;
        $this->twig       = $twig;
        $this->bundleGuesser = $bundleGuesser;
        $this->from       = $from;
    }

    public function createMessage($class, $name, $parameters)
    {
        $subject = $txtBody = $htmlBody = null;
        $bundle  = $this->bundleGuesser->getBundleForClass($class);
        $txtTpl  = sprintf('%s:Mails:%s.txt.twig', $bundle->getName(), $name);
        $htmlTpl = sprintf('%s:Mails:%s.html.twig', $bundle->getName(), $name);

        if (true === $this->twig->getLoader()->exists($txtTpl)) {
            $template = $this->twig->loadTemplate($txtTpl);
            $subject  = $template->renderBlock('subject', $parameters);
            $txtBody  = $template->renderBlock('body', $parameters);
        }

        if (true === $this->twig->getLoader()->exists($htmlTpl)) {
            $template = $this->twig->loadTemplate($htmlTpl);
            $subject  = $subject ?: $template->renderBlock('subject', $parameters);
            $htmlBody = $template->renderBlock('body', $parameters);
        }

        if (!$subject) {
            throw new \Twig_Error_Loader(sprintf(
                'Can not find mail subject in "%s" or "%s".', $txtTpl, $htmlTpl
            ));
        }

        if (!$txtBody && !$htmlBody) {
            throw new \Twig_Error_Loader(sprintf(
                'Can not find mail body in "%s" or "%s".', $txtTpl, $htmlTpl
            ));
        }

        $message = $this->mailer->createMessage();
        $message->setSubject($subject);

        if ($txtBody) {
            $message->setBody($txtBody, 'text/plain');
        } else {
            $message->setBody($htmlBody, 'text/html');
        }

        if ($txtBody && $htmlBody) {
            $message->addPart($htmlBody, 'text/html');
        }

        if ($this->from) {
            $message->setFrom($this->from);
        }

        return $message;
    }

    public function send(Swift_Mime_Message $message, $to = null , $from = null)
    {
        if (!$from) {
            $from = $this->from;
        }
        $message->setFrom($from);

        if ($to) {
            $message->setTo($to);
        }

        $this->mailer->send($message);
    }

    public function getMailer()
    {
        return $this->mailer;
    }
}
