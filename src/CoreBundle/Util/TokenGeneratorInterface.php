<?php

namespace CoreBundle\Util;

/**
 * @author Wenming Tang <tang@babyfamily.com>
 */
interface TokenGeneratorInterface
{
    public function generateToken();
}