<?php

namespace CoreBundle\Util;

/**
 * 简单的无限树型类，只是简单排序，不生成任何树型结构
 * 排序前请先按照 ParentId ASC 排序，不然会显示不完全
 */
class SingleTree
{
    static public $positionTree = array();
    static public $treeList = array(); //存放无限分类结果

    /**
     * 无限级分类
     * 获取当前当前分类的子栏目
     * @access public 
     * @param Array $categorys  //数据库里获取的结果集 
     * @param Int $pid  //当前分类ID
     * @param Int $count       //第几级分类
     * @return Array $treeList   
     */
    static public function tree(&$categorys, $parentId, $count = 0)
    {
        foreach ($categorys as $key => $value) {
            if ($value['parentId'] == $parentId) {
                $value['count'] = $count;
                self::$treeList[] = $value;

                unset($categorys[$key]);

                self::tree($categorys,$value['id'], $count+1);
            } 
        }

        return self::$treeList;
    }

    /**
     * 无限级分类变形
     * 获取当前位置数组
     * @access public 
     * @param Array $data  //数据库里获取的结果集 
     * @param Int $pid  //当前分类ID
     * @param Int $count       //第几级分类
     * @return Array $treeList   
     */
    /* 获取当前位置数组 */
    static public function currentTree(&$categorys, $currentId, $count = 0)
    {
        foreach ($categorys as $key => $value) {
            if ($value['id']==$currentId) {
                $value['count'] = $count;
                self::$positionTree[]=$value;

                unset($categorys[$key]);

                self::currentTree($categorys,$value['parentId'],$count+1);
            }
        }

        return self::$positionTree;
    }

    /* 清空当前缓存 */
    static public function clear() {
        self::$treeList = array();
        self::$positionTree = array();
    }
}