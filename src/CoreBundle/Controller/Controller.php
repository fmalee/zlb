<?php

namespace CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller as BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Doctrine\ORM\EntityRepository;
use CoreBundle\Flash;
use CoreBundle\Util\VarDumper;

class Controller extends BaseController
{
    protected function redirectToRoute($route, array $parameters = array(), $status = 302)
    {
        return $this->get('core.controller.helper.response')->redirectToRoute($route, $parameters, $status);
    }

    public function createAccessDeniedException($message = 'Access Denied', \Exception $previous = null)
    {
        return $this->get('core.controller.helper.security')->createAccessDeniedException($message, $previous);
    }

    protected function getRepository($object)
    {
        return $this->get('core.controller.helper.doctrine')->getRepository($object);
    }

    protected function isGranted($attributes, $object = null)
    {
        return $this->get('core.controller.helper.security')->isGranted($attributes, $object);
    }

    protected function isGrantedOr403($object, $criteria = array())
    {
        return $this->get('core.controller.helper.security')->isGrantedOr403($object, $criteria);
    }

    protected function createMessage($name, array $parameters = array(), $to = null, $from = null)
    {
        return $this->get('core.controller.helper.mail')->createMessage($name, $parameters, $from, $to);
    }

    protected function send(\Swift_Mime_Message $message)
    {
        $this->get('core.controller.helper.mail')->send($message);
    }

    protected function persist($object, $flush = false)
    {
        return $this->get('core.controller.helper.doctrine')->persist($object, $flush);
    }

    protected function remove($object, $flush = false)
    {
        return $this->get('core.controller.helper.doctrine')->remove($object, $flush);
    }

    protected function flush($object = null)
    {
        return $this->get('core.controller.helper.doctrine')->flush($object);
    }

    protected function findBy($object, $criteria = array())
    {
        return $this->get('core.controller.helper.doctrine')->findBy($object, $criteria);
    }

    protected function findOr404($object, $criteria = array())
    {
        return $this->get('core.controller.helper.doctrine')->findOr404($object, $criteria);
    }

    protected function addFlash($type, $message = null, array $parameters = array(), $pluralization = null)
    {
        return $this->get('core.controller.helper.session')->addFlash($type, $message, $parameters, $pluralization);
    }

    protected function createObjectForm($object, $purpose = null, array $options = array())
    {
        return $this->get('core.controller.helper.form')->createObjectForm($object, $purpose, $options);
    }

    protected function createBoundObjectForm($object, $purpose = null, array $options = array())
    {
        return $this->get('core.controller.helper.form')->createBoundObjectForm($object, $purpose, $options);
    }

    protected function getSession()
    {
        return $this->get('session');
    }

    protected function getMailer()
    {
        return $this->get('mailer');
    }

    protected function getSecurity()
    {
        return $this->get('security.context');
    }

    protected function getManager()
    {
        return $this->getDoctrine()->getManager();
    }

    protected function getFlashBag()
    {
        return $this->getSession()->getFlashBag();
    }

    protected function getParameter($name)
    {
        return $this->container->getParameter($name);
    }

    protected function getCache()
    {
        return $this->get('core.cache');
    }

    protected function getBreadcrumb()
    {
        return $this->get('core.breadcrumb');
    }

    protected function paginator()
    {
        return $this->get('knp_paginator');
    }

    protected function paginate($query, $page = 1, $limit = 20)
    {
        return $this->get('knp_paginator')->paginate($query, $page, $limit);
    }

    protected function dump($var, $exit = true, $depth = 10, $highlight = true)
    {
        VarDumper::dump($var, $exit = true, $depth = 10, $highlight = true);
    }

    /**
     * {@inheritdoc}
     * Ajax方式返回数据到客户端
     * 如果想直接赋值，用$this->ajaxRetun(array(key=>value),null);
     * @param String $success 要返回的状态
     * @param String $message 要返回的标题
     * @param mixed $data 要返回的数据
     * @param String $type AJAX返回数据格式
     * @return void
     */
    protected function ajaxReturn($message, $success = true, $data = '', $type='JSON')
    {
        if (is_null($success)) {
            $ajax = $message;
        } else {
            $ajax = array();
            $ajax['success'] = $success;
            $ajax['message'] = $message;
            if(!empty($data)) $ajax['data'] = $data;
        }

        $response = new Response();
        $response->setStatusCode(200);
        switch (strtoupper($type))
        {
            /* 返回xml格式数据 */
            case 'XML' :
                $response->headers->set('content-type','text/xml');
                $response->setContent(xml_encode($ajax));
            /* 返回JSONP数据格式到客户端 包含状态信息 */
            case 'JSONP':
                $response->headers->set('content-type','application/json');
                $response->setContent(json_encode($ajax));
            /*  返回可执行的js脚本 */
            case 'EVAL' :
                $response->headers->set('content-type','text/html');
                $response->setContent($ajax);
            /*  返回JSON数据格式到客户端 包含状态信息 */
            default :
                $response->headers->set('content-type','application/json');
                $response->setContent(json_encode($ajax));
        }

        return $response;
    }

    /**
     * {@inheritdoc}
     * 返回目录式导航
     *
     * @param string $name
     * @return static|null
     */
    protected function setCategoryBreadcrumb($categoryId, $route)
    {
        $categories = $this->getCategories();
        if (!isset($categories[$categoryId])) {
            return '';
        }

        $arrpParent = trim($categories[$categoryId]['arrParent']) . ',' . $categoryId;
        $arrpParent = array_filter(explode(',', $arrpParent));
        foreach($arrpParent as $parentId) {
            $url = $this->generateUrl($route, array('cate' => $categories[$parentId]['name']));
            $this->getBreadcrumb()->add($categories[$parentId]['title'], $url);
        }

        return $this->getBreadcrumb();
    }

    /**
     * {@inheritdoc}
     * 统一处理模块更新
     *
     * @param string $name
     * @return static|null
     */
    public function edit(Request $request, $entityRepository, $template, $id = null)
    {
        if ($id) {
            $entity = $entityRepository->find($id);
            if (!$entity) {
                throw new NotFoundHttpException;
            }
        } else {
            $entity = $entityRepository->createNew();
        }

        $form = $this->createBoundObjectForm($entity, 'edit');

        if ($form->isBound() && $form->isValid()) {
            $this->persist($entity, true);

            $this->addFlash('编辑成功');

            return $this->ajaxReturn('编辑成功');
        }

        $content = $this->renderView($template, array(
            'form' => $form->createView(),
            'id' => $id
        ));
        return $this->ajaxReturn($content, null);
    }

    /**
     * {@inheritdoc}
     * 处理AJAX单字段更新
     *
     * @param string $name
     * @return static|null
     */
    public function ajaxEdit($entityRepository)
    {
        $query = $this->getRequest()->query;
        $id = $query->get('id');
        if ($id) {
            $entity = $entityRepository->find($id);
            if ($entity) {
                $field = ucfirst($query->get('field'));
                $value = $query->get('val');
                $setFun = 'set' . $field;
                $getFun = 'get' . $field;

                if (is_bool($entity->$getFun())) {
                    $value = $value ? true : false;
                }

                $entity->$setFun($value);
                $this->persist($entity, true);

                return $this->ajaxReturn('动态更新数据完成');
            }
        }

        return $this->ajaxReturn('动态更新数据失败', false);
    }

    /**
     * 获取CategoryRepository
     * @return CategoryRepository
     */
    protected function getCategoryRepository()
    {
        return $this->get('common.entity.category_repository');
    }

    /**
     * {@inheritdoc}
     * 返回目录集合
     *
     * @param string $isTree
     * @return array Categories
     */
    protected function getCategories($isTree = false)
    {
        return $this->getCategoryRepository()->getCategories($isTree);
    }
}
