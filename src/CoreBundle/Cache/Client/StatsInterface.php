<?php

namespace CoreBundle\Cache\Client;

use CoreBundle\Cache\Statistics;

/**
 * Interface for generating statistics
 *
 * @package
 * @version $id$
 * @author Yaroslav Nechaev <mail@remper.ru>
 * @license See LICENSE.md
 */
interface StatsInterface
{
    /**
     * @return Statistics[]
     */
    public function getStats();
}