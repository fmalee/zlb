<?php

namespace CoreBundle\Breadcrumb;

class SingleBreadcrumb
{
    public $text;
    public $url;

    public function __construct($text, $url = '')
    {
        $this->text = $text;
        $this->url  = $url;
    }
}