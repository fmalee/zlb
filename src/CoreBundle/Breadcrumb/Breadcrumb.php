<?php

namespace CoreBundle\Breadcrumb;

use Symfony\Component\Routing\RouterInterface;

class Breadcrumb
{
    protected $breadcrumbs = array();

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
        $this->breadcrumbs[] = new SingleBreadcrumb('英靡卡', '/');
    }

    public function add($text, $url = '')
    {
        $this->breadcrumbs[] = new SingleBreadcrumb($text, $url);

        return $this;
    }

    public function rest()
    {
        $this->breadcrumbs = array();

        return $this;
    }

    public function getBreadcrumbs()
    {
        return $this->breadcrumbs;
    }
}
