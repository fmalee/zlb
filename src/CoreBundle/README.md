Copy at 2014.04.20



CoreBundle
============

develop: [![Build Status](https://travis-ci.org/KnpLabs/CoreBundle.png?branch=develop)](https://travis-ci.org/KnpLabs/CoreBundle)

2.3: [![Build Status](https://travis-ci.org/KnpLabs/CoreBundle.png?branch=2.3)](https://travis-ci.org/KnpLabs/CoreBundle)

2.2: [![Build Status](https://travis-ci.org/KnpLabs/CoreBundle.png?branch=2.2)](https://travis-ci.org/KnpLabs/CoreBundle)

Rapid Application Development bundle for Symfony2

[http://rad.knplabs.com](http://rad.knplabs.com)

Changelog generation:

    npm install github-changelog
    gh-changelog -t gh-changelog-template.tpl -o KnpLabs -r CoreBundle -s 2013-04-16 -f CHANGELOG.md
